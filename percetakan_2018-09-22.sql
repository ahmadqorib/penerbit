# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.61-0ubuntu0.14.04.1)
# Database: percetakan
# Generation Time: 2018-09-22 05:47:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table api_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api_log`;

CREATE TABLE `api_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request` text,
  `response` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `headercode` varchar(30) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table bahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bahan`;

CREATE TABLE `bahan` (
  `id_bahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bahan` varchar(45) DEFAULT NULL,
  `panjang` float DEFAULT NULL,
  `lebar` float DEFAULT NULL,
  PRIMARY KEY (`id_bahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bahan` WRITE;
/*!40000 ALTER TABLE `bahan` DISABLE KEYS */;

INSERT INTO `bahan` (`id_bahan`, `nama_bahan`, `panjang`, `lebar`)
VALUES
	(5,'HVS1',30,20),
	(6,'Bp 57 gr',109,79);

/*!40000 ALTER TABLE `bahan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cetak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cetak`;

CREATE TABLE `cetak` (
  `id_cetak` int(11) NOT NULL AUTO_INCREMENT,
  `id_pra_cetak` int(11) DEFAULT NULL,
  `model_cetak` varchar(45) DEFAULT NULL,
  `hal` varchar(45) DEFAULT NULL,
  `id_tinta` int(11) DEFAULT NULL,
  `jml_set` int(11) DEFAULT NULL,
  `insheet_tiap_set` varchar(45) DEFAULT NULL,
  `tot_druck_per_set` int(11) DEFAULT NULL,
  `id_detail_order` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id_cetak`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table detail_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detail_order`;

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `produk` varchar(200) DEFAULT NULL,
  `oplah` int(11) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `jenis_order` varchar(45) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `jml_hlm_isi` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `finish_pracetak` int(1) DEFAULT '0',
  `finish_bahan` int(1) DEFAULT '0',
  `finish_cetak` int(1) DEFAULT '0',
  `finish_finishing` int(1) DEFAULT '0',
  `file` text,
  PRIMARY KEY (`id_detail_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table finish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finish`;

CREATE TABLE `finish` (
  `id_finish` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) DEFAULT NULL,
  `id_finishing` int(11) DEFAULT NULL,
  `ket` text,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id_finish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table finishing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finishing`;

CREATE TABLE `finishing` (
  `id_finishing` int(11) NOT NULL AUTO_INCREMENT,
  `nama_finishing` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_finishing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `finishing` WRITE;
/*!40000 ALTER TABLE `finishing` DISABLE KEYS */;

INSERT INTO `finishing` (`id_finishing`, `nama_finishing`)
VALUES
	(1,'Lipat'),
	(3,'Packing');

/*!40000 ALTER TABLE `finishing` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table karyawan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_karyawan` varchar(200) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(12) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `apikey` text,
  `level` int(2) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `alamat`, `no_telp`, `username`, `password`, `apikey`, `level`, `remember_token`)
VALUES
	(1,'superadmin','Daerah Istimewa Yogyakarta','081234567443','superadmin','25d55ad283aa400af464c76d713c07ad','5243af8256922b7edc0bd5ee0809cbfd7106da1e',1,'jWnTyPL1v6k450EzXnn7YOuzlSorbVWyT3kJcl6zMvYJqIwsShfIWqvnc3K7'),
	(3,'pcic','Daerah Istimewa Yogyakarta','082111122332','pcic','25d55ad283aa400af464c76d713c07ad',NULL,2,'zZOABuwn4Tw2ecBT16PdbJ1pR2bFEWVIqtgu6wnJVM0y7ftRuvfIb2zaC8le'),
	(10,'pracetak','Daerah Istimewa Yogyakarta','082111122332','pracetak','25d55ad283aa400af464c76d713c07ad',NULL,3,'BiuG4lycNAh9Sm7q3bTibco1mVfl6nx8DydX8L778cfkj0CNKhSTCnL8DViZ'),
	(11,'kebbahan','Daerah Istimewa Yogyakarta','083782818721','kebbahan','25d55ad283aa400af464c76d713c07ad',NULL,4,'Lv8hOk12DQ8LOn7xr0kQ6d2HjGKX4DY1Z85fs02lMmrNYjbERGsybv7hL0IG'),
	(12,'cetak','Daerah Istimewa Yogyakarta','081234567443','cetak','25d55ad283aa400af464c76d713c07ad',NULL,5,'LzoUdrPIsGNz2zTxh9Bz2LYX5OBYxBKtTksucXARtHdjE0xkNgQYGSXz1NkZ'),
	(13,'finishing','Daerah Istimewa Yogyakarta','082111122332','finishing','25d55ad283aa400af464c76d713c07ad',NULL,6,'lS35m201icDS8YBrdIkRNZu7LGVMOCF6Yv217PB2EwqvWv7RazxXB35jRpzI'),
	(14,'sales','Daerah Istimewa Yogyakarta','082111122332','sales','25d55ad283aa400af464c76d713c07ad',NULL,8,'x5fxhookqAEFladUVzheliLh15bO8IKBzOtLBulEmyyUIQ8TcJkrHpjOKsqc');

/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table keb_bahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `keb_bahan`;

CREATE TABLE `keb_bahan` (
  `id_keb_bahan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pra_cetak` int(11) DEFAULT NULL,
  `id_bahan` int(11) DEFAULT NULL,
  `panjang` float DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `pl_jadi` int(11) DEFAULT NULL,
  `jml_lbr_p` float DEFAULT NULL,
  `ket` varchar(45) DEFAULT NULL,
  `id_detail_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id_keb_bahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table komponen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `id_komponen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_komponen` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_komponen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `komponen` WRITE;
/*!40000 ALTER TABLE `komponen` DISABLE KEYS */;

INSERT INTO `komponen` (`id_komponen`, `nama_komponen`)
VALUES
	(1,'Kertas Pembatas'),
	(2,'Cover'),
	(3,'Isi');

/*!40000 ALTER TABLE `komponen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mesin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin`;

CREATE TABLE `mesin` (
  `id_mesin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mesin` varchar(45) DEFAULT NULL,
  `no_mesin` varchar(45) DEFAULT NULL,
  `tipe_mesin` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_mesin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mesin` WRITE;
/*!40000 ALTER TABLE `mesin` DISABLE KEYS */;

INSERT INTO `mesin` (`id_mesin`, `nama_mesin`, `no_mesin`, `tipe_mesin`)
VALUES
	(4,'Mesin 1','M00001','0911');

/*!40000 ALTER TABLE `mesin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `no_soki` varchar(45) DEFAULT NULL,
  `no_sp` varchar(45) DEFAULT NULL,
  `nama_order` varchar(200) DEFAULT NULL,
  `id_penerima` int(11) DEFAULT NULL,
  `nama_pemesan` varchar(200) DEFAULT NULL,
  `alamat_pemesan` text,
  `no_telp_pemesan` varchar(13) DEFAULT NULL,
  `tgl_order` date DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `ket` text,
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `no_soki_UNIQUE` (`no_soki`),
  UNIQUE KEY `no_sp_UNIQUE` (`no_sp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pra_cetak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pra_cetak`;

CREATE TABLE `pra_cetak` (
  `id_pra_cetak` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  `id_mesin` int(11) DEFAULT NULL,
  `up` int(11) DEFAULT NULL,
  `model_montage` varchar(45) DEFAULT NULL,
  `master` varchar(45) DEFAULT NULL,
  `ket` text,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id_pra_cetak`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tinta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tinta`;

CREATE TABLE `tinta` (
  `id_tinta` int(11) NOT NULL AUTO_INCREMENT,
  `warna` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_tinta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tinta` WRITE;
/*!40000 ALTER TABLE `tinta` DISABLE KEYS */;

INSERT INTO `tinta` (`id_tinta`, `warna`)
VALUES
	(3,'CMYK'),
	(4,'RGB');

/*!40000 ALTER TABLE `tinta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `apikey` text,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id_user`, `username`, `password`, `level`, `apikey`)
VALUES
	(1,'superadmin','25d55ad283aa400af464c76d713c07ad',1,'5243af8256922b7edc0bd5ee0809cbfd7106da1e');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
