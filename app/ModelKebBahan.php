<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelKebBahan extends Model
{
    protected $table = 'keb_bahan';
//    public $timestamps = false;
//    protected $guarded = ['id_keb_bahan'];
    protected $primaryKey = 'id_keb_bahan';
    protected $fillable = [        
        'id_pra_cetak',
        'id_bahan',
        'id_detail_order',
        'panjang',
        'lebar',
        'pl_jadi',
        'jml_lbr_p',
        'ket',
        'status',
        'id_komponen',
    ];

    public function order() 
    {
        return $this->belongsTo('App\ModelPraCetak', 'id_pra_cetak', 'id_pra_cetak');
    }
    
    public function bahan()
    {
        return $this->belongsTo('App\ModelBahan', 'id_bahan', 'id_bahan');
    }
    
    public function pracetak()
    {
        return $this->belongsTo('App\ModelPraCetak', 'id_pra_cetak', 'id_pra_cetak');
    }
    
    public function detail_order()
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
    public function komponen()
    {
        return $this->belongsTo('App\ModelKomponen', 'id_komponen', 'id_komponen');
    }
}
