<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTinta extends Model
{
    protected $table = 'tinta';
//    public $timestamps = false;
    public $primaryKey = 'id_tinta';
//    protected $guarded = ['id_tinta'];
    protected $fillable = [        
        'warna',
        
    ];
    public static function dropdown(){
        $list = array();
        $model = ModelTinta::get();
        foreach ($model as $item){
            $list[$item->id_tinta] = $item->warna;
        }
        return $list;
    }
    
    public function mesin() {
        return $this->hasMany('App\ModelMesinTinta', 'id_tinta', 'id_tinta');
    }
    
}
