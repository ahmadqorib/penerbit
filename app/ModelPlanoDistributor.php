<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPlanoDistributor extends Model
{
    protected $table = 'plano_distributor';
//    public $timestamps = false;
    protected $primaryKey = 'id_plano_distributor';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'id_plano',
        'id_distributor',
        
    ];
    
    public function plano() 
    {
        return $this->belongsTo('App\ModelPlano', 'id_plano', 'id_plano');
    }
    
    public function distributor() 
    {
        return $this->belongsTo('App\ModelDistributor', 'id_distributor', 'id_distributor');
    }
    
}
