<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ModelDetailPemesanan;
use Validator;
use Auth;

class DetailPemesananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'produk' => 'required',
                // 'oplah' => 'required|numeric',
                // 'satuan' => 'required',
                // 'jenis_order' => 'required',
                // 'panjang' => 'required|numeric',
                // 'lebar' => 'required|numeric',
                // 'jml_hal' => 'required|numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_order = $request->id_order;
            $produk = $request->produk;
            // $oplah = $request->oplah;
            // $satuan = $request->satuan;
            // $jenis_order = $request->jenis_order;
            // $panjang = $request->panjang;
            // $lebar = $request->lebar;
            // $hal_isi = $request->jml_hal;
            $status = 0;
            
            $data = [
                        'id_order' => $id_order,
                        'produk' => $produk,
                        // 'oplah' => $oplah,
                        // 'satuan' => $satuan,
                        // 'jenis_order' => $jenis_order,
                        // 'panjang' => $panjang,
                        // 'lebar' => $lebar,
                        // 'jml_hlm_isi' => $hal_isi,
                        'status' => $status
                    ];

            ModelDetailPemesanan::create($data);
            $getId = $id_order;

            $user = Auth::user();
            if(($user->level == 1) or ($user->level == 2)){
                return redirect("pemesanan/detail_nofile/$getId");
            }else{
                return redirect("data_pemesanan/detail/$getId");
            }
            
        }
    }
    
    public function showDataEdit($id)
    {
        $data = ModelDetailPemesanan::where(['id_detail_order' => $id])->first();
        return response()->json($data);
    }
    
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'produk' => 'required',
                'oplah' => 'required|numeric',
                'satuan' => 'required',
                'jenis_order' => 'required',
                'panjang' => 'required|numeric',
                'lebar' => 'required|numeric',
                'jml_hal' => 'required|numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_detail_order = $request->id_detail_order;
            $id_order = $request->id_order;
            $produk = $request->produk;
            $oplah = $request->oplah;
            $satuan = $request->satuan;
            $jenis_order = $request->jenis_order;
            $panjang = $request->panjang;
            $lebar = $request->lebar;
            $hal_isi = $request->jml_hal;
            
            $data = [
                        'produk' => $produk,
                        'oplah' => $oplah,
                        'satuan' => $satuan,
                        'jenis_order' => $jenis_order,
                        'panjang' => $panjang,
                        'lebar' => $lebar,
                        'jml_hlm_isi' => $hal_isi,
                    ];

            ModelDetailPemesanan::where(['id_detail_order' => $id_detail_order])->update($data);
            return redirect("pemesanan/detail/$id_order");
        }
    }
    
    public function delete($id)
    {
        $hapus = ModelDetailPemesanan::where(['id_detail_order' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }

        return redirect()->back();
    }
    
    public function KonfirmasiPra($id)
    {
        $cek = ModelDetailPemesanan::where(['id_detail_order' => $id])->first();
        if(!empty($cek)){
            $cek->update(['status'=>'2']);
//            $getId = $cek->id_order;
        }
        return redirect("praCetak/detail/$id");
    }

    public function do_print($id)
    {
        // $record = \App\ModelPemesanan::where([ 'id_order' => $id ])->first();
        $record_detail = ModelDetailPemesanan::where([ 'id_detail_order' => $id])->first();
        $data = [
            'title' => 'Print Pemesanan',
            'detail' => $record_detail,
            'row' => $record_detail->order
      ];
    return view('page.order.print', $data);
    }

}
