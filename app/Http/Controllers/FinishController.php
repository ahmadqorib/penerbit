<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelCetak;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Validator;

class FinishController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data = [
                    'title' => 'Finish',
            ];
        return view('page.finish.finish', $data);
    }

    public function showData()
    {
        // $data = \App\ModelDetailPemesanan::where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>1,'finish_finishing'=>0,'status'=>1]);
        // $no = 1;
        DB::statement(DB::raw('set @rownum=0'));
        $data = \App\ModelDetailPemesanan::with(['order'])
                ->where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>1,'finish_finishing'=>0,'status'=>2])
                ->select(['detail_order.*',DB::raw('@rownum  := @rownum  + 1 AS no')]);

        return Datatables::of($data)
            ->addColumn('no_soki', function ($data){
                return $data->order->no_soki;
            })
            ->addColumn('aksi', function($data) {
                    return '
                            <a href="finish/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>';
//                            <a href="praCetak/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-primary">
//                                <i class="glyphicon glyphicon-edit"></i> edit
//                            </a>
//                            <a href="praCetak/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-danger" style="margin-top: 3px">
//                                <i class="glyphicon glyphicon-trash"></i> hapus
//                            </a>
//                            ';
                })
            ->make(true);
    }

    public function detailFinish($id)
    {
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_detail_order' => $id ])->first();
        $data = [
                'title' => 'Detail Finish',
                'det' => $record_detail,
                'detFinish' => $record_detail->finish()->get()
            ];

        return view('page.finish.detailFinish', $data);
    }


    public function completed($id)
    {
        $data = \App\ModelFinish::where(['id_finish' => $id])->first();
        if(!empty($data)) {
            $data->status = 1;
            $data->save();

            $data->detail_order->cek_tahap();
        }
        return redirect()->back();
    }
}
