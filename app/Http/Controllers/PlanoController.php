<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PlanoController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelPlano::query();
        $default = true;
        $nama = Input::get('nama');
        
        if (!empty($nama)) {
            $q->where('nama', 'like', '%' . $nama . '%');
        }

        $data = $q->paginate(10);
        
        
        return view('page.plano.index', compact('data','nama'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
            'nama' => 'required|unique:plano',
            'panjang' => 'required|numeric|min:1',
            'lebar' => 'required|numeric|min:1',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('plano');
        }
            
            $model = new \App\ModelPlano();
            $model->nama = $request->nama;
            $model->panjang = $request->panjang;
            $model->lebar = $request->lebar;
            
            if ($model->save()) {
                
                Session::flash('success', 'ModelPlano has been saved.');
    //            return redirect('bus');
            } else {
                Session::flash('error', 'ModelPlano could not be saved. Please, try again.');
    //            return redirect('categories/create')->withInput($request->except('_token'));
            }
        
        
        
        return redirect('plano');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $valid = Validator::make($request->all(), [
            'id_plano'=>'required',
            'nama' => 'required',
            'panjang' => 'required|numeric|min:1',
            'lebar' => 'required|numeric|min:1',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('plano');
        }
        
        $id = $request->id_plano;
        
        $exist_other = \App\ModelPlano::where('id_plano','!=', $id)->where('nama',$request->nama)->first();
        if(!empty($exist_other)){
            Session::flash('error', 'ModelPlano Same Data found. Please, try again.');
            return redirect('plano');
        }
        
        $model = \App\ModelPlano::where('id_plano', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelPlano not found. Please, try again.');
            return redirect('plano');
        }
        
        $model->nama = $request->nama;
        $model->panjang = $request->panjang;
        $model->lebar = $request->lebar;
        $model->update();
                
        return redirect('plano');
    }

    public function delete($id = null) {
        $model = \App\ModelPlano::where('id_plano', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelPlano not found. Please, try again.');
            return redirect('plano');
        } 
        
        //do cek relation
        
        $model->delete();
        return redirect('plano');
    }

}
