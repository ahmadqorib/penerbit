<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelUser;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $title = 'Welcome';
        $content = 'Selamat Datang';
 
        return view('page.home', ['title' => $title, 'content' => $content]);
    }
    
//    public function adminpanel(){
//        return view('login');
//    }
//    
//    public function login(Request $request)
//    {
//        $data = [
//                    'username' => $request->username,
//                    'password' => $request->password
//            ];
//        $cek = Modeluser::where($data)->first();
//        if($cek){
//            return redirect('dashboard');
//        }else{
//            return redirect('adminpanel');
//        }
//    }
//    
//    public function logout()
//    {
//        return redirect('adminpanel');
//    }
}
