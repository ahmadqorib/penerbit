<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class JenisPercetakanController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelJenisPercetakan::query();
        $default = true;
        $nama = Input::get('nama');
        
        if (!empty($nama)) {
            $q->where('nama', 'like', '%' . $nama . '%');
        }

        $data = $q->paginate(10);
        
        
        return view('page.jenis_percetakan.index', compact('data','nama'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
            'nama' => 'required|unique:jenis_percetakan',
            
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('jenis_percetakan');
        }
            
            $model = new \App\ModelJenisPercetakan();
            $model->nama = $request->nama;
            
            if ($model->save()) {
                
                Session::flash('success', 'ModelJenisPercetakan has been saved.');
    //            return redirect('bus');
            } else {
                Session::flash('error', 'ModelJenisPercetakan could not be saved. Please, try again.');
    //            return redirect('categories/create')->withInput($request->except('_token'));
            }
        
        
        
        return redirect('jenis_percetakan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $valid = Validator::make($request->all(), [
            'id_jenis_percetakan'=>'required',
            'nama' => 'required',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('jenis_percetakan');
        }
        
        $id = $request->id_jenis_percetakan;
        
        $exist_other = \App\ModelJenisPercetakan::where('id_jenis_percetakan','!=', $id)->where('nama',$request->nama)->first();
        if(!empty($exist_other)){
            Session::flash('error', 'ModelJenisPercetakan Same Data found. Please, try again.');
            return redirect('plano');
        }
        
        $model = \App\ModelJenisPercetakan::where('id_jenis_percetakan', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelJenisPercetakan not found. Please, try again.');
            return redirect('jenis_percetakan');
        }
        
        $model->nama = $request->nama;
        $model->update();
                
        return redirect('jenis_percetakan');
    }

    public function delete($id = null) {
        $model = \App\ModelJenisPercetakan::where('id_jenis_percetakan', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelJenisPercetakan not found. Please, try again.');
            return redirect('jenis_percetakan');
        } 
        
        //do cek relation
        
        $model->delete();
        return redirect('jenis_percetakan');
    }

}
