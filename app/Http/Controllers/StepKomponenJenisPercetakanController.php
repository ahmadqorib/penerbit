<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class StepKomponenJenisPercetakanController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelStepKomponenJenisPercetakan::query();
        $nama_step_percetakan = Input::get('nama_step_percetakan');
        $nama_komponen_jenis_percetakan = Input::get('nama_komponen_jenis_percetakan');

        if (!empty($nama_step_percetakan)) {
            $q->whereHas('step_percetakan',function($query) use ($nama_step_percetakan){
                $query->where('nama', 'like', '%' . $nama_step_percetakan . '%');
            });
        }

        if (!empty($nama_komponen_jenis_percetakan)) {
            $q->whereHas('komponen_jenis_percetakan.komponen',function($query) use ($nama_komponen_jenis_percetakan){
                $query->where('nama_komponen', 'like', '%' . $nama_komponen_jenis_percetakan . '%');
            })->orWhereHas('komponen_jenis_percetakan.jenis_percetakan',function($query) use ($nama_komponen_jenis_percetakan){
                $query->where('nama', 'like', '%' . $nama_komponen_jenis_percetakan . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_step_percetakan = \App\ModelStepPercetakan::dropdown();
        $list_komponen_jenis_percetakan = \App\ModelKomponenJenisPercetakan::dropdown();

        return view('page.step_komponen_jenis_percetakan.index', compact('data', 'nama_step_percetakan','nama_komponen_jenis_percetakan', 'list_step_percetakan', 'list_komponen_jenis_percetakan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_step_percetakan' => 'required',
                    'id_komponen_jenis_percetakan' => 'required',
                    'urutan' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('step_komponen_jenis_percetakan');
        }

        $exist_other = \App\ModelStepKomponenJenisPercetakan::where('id_komponen_jenis_percetakan', $request->id_komponen_jenis_percetakan)
                        ->where('id_step_percetakan', $request->id_step_percetakan)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelStepKomponenJenisPercetakan Same Data found. Please, try again.');
            return redirect('step_komponen_jenis_percetakan');
        }

        $model = new \App\ModelStepKomponenJenisPercetakan();
        $model->id_step_percetakan = $request->id_step_percetakan;
        $model->id_komponen_jenis_percetakan = $request->id_komponen_jenis_percetakan;
        $model->urutan = $request->urutan;

        if ($model->save()) {

            Session::flash('success', 'ModelStepKomponenJenisPercetakan has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelStepKomponenJenisPercetakan could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('step_komponen_jenis_percetakan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelStepKomponenJenisPercetakan::where('id_step_komponen_jenis_percetakan', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelStepKomponenJenisPercetakan not found. Please, try again.');
            return redirect('step_komponen_jenis_percetakan');
        }

        //do cek relation

        $model->delete();
        return redirect('step_komponen_jenis_percetakan');
    }

}
