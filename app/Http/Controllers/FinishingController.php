<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelFinishing;
use Validator;

class FinishingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$field = ModelFinishing::all();
    	$data = [
    				'title' => 'Data Finishing',
    				'row' => $field
    		];
    	return view('page.finishing.finishing', $data);
    }

    public function create(Request $request)
    {
      $valid = Validator::make($request->all(), [
              'nama_finishing' => 'required'
          ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $model = new ModelFinishing();
            $model->nama_finishing = $request->nama_finishing;
            $model->save();
            
            return redirect('finishing');
        }


    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_finishing' => 'required'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            
            $model = ModelFinishing::where(['id_finishing'=>$request->id_finishing])->first();
            $model->nama_finishing = $request->nama_finishing;
            $model->save();
            return redirect('finishing');
        }
    }

    public function delete($id)
    {
        $hapus = ModelFinishing::where(['id_finishing' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('finishing');
    }
}
