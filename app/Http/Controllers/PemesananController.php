<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelPemesanan;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class PemesananController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    // private function level()
    // {
    //     $user = Auth::user();
    //     if(($user->level != 1) and ($user->level != 2)){
    //         // var_dump("fdas"); die();
    //         return Redirect::back();
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    
    public function index()
    {
        $user = auth::user();
        
        $data = [
            'title' => 'Data Pemesanan',
            'karyawan' => \App\ModelKaryawan::all(),
            'kondisi' => 1
        ];
        return view('page.order.pemesanan', $data);   
    }

    public function showData()
    {
        // $user = auth::user();
        $query = new \App\ModelDetailPemesanan();
        DB::statement(DB::raw('set @rownum=0'));
        // if(($user->level == 1) or ($user->level == 2)){
        //     $data = ModelPemesanan::select([
        //         DB::raw('@rownum  := @rownum  + 1 AS rownum'),
        //         'id_order',
        //         'no_soki', 
        //         'no_sp',
        //         'nama_order',
        //         'nama_pemesan',
        //         'tgl_order',
        //     ])->whereIn('id_order', $query->where('file', '!=', NULL)->orWhere('file', '!=', " ")->get(['id_order']));;
        // }else{
        //     $data = ModelPemesanan::select([
        //         DB::raw('@rownum  := @rownum  + 1 AS rownum'),
        //         'id_order',
        //         'no_soki', 
        //         'no_sp',
        //         'nama_order',
        //         'nama_pemesan',
        //         'tgl_order'
        //     ])->where(['id_penerima' => $user->id_karyawan]);
        // }

        $data = ModelPemesanan::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id_order',
            'no_soki', 
            'no_sp',
            'nama_order',
            'nama_pemesan',
            'tgl_order',
        ])->whereIn('id_order', $query->where('file', '!=', NULL)->orWhere('file', '!=', " ")->get(['id_order']));

        return Datatables::of($data)
            ->addColumn('aksi', function ($data) {
                    return '
                            <a href="pemesanan/detail/'.$data->id_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>
                            <a href="pemesanan/edit/'.$data->id_order.'" class="btn btn-xs btn-primary" style="margin-top: 2px">
                                <i class="glyphicon glyphicon-edit"></i> edit
                            </a>
                            <a data-href="pemesanan/delete/'.$data->id_order.'" class="btn btn-danger btn-xs"
                                data-toggle="modal" data-target="#modalHapus" style="margin-top: 2px">
                                <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            ';
                })
            ->addColumn('status', function($data){
                    $st = ModelPemesanan::where(['id_order'=>$data->id_order])->first();
                    return $st->status($data->id_order);
                })
            ->make(true);
    }

    // public function showNoFile()
    // {   
    //     $data = [
    //                 'title' => 'Data Pemesanan',
    //                 'karyawan' => \App\ModelKaryawan::all(),
    //                 'kondisi' => 0
    //         ];
    //     return view('page.pemesanan', $data);
    // }

    // public function showData_nofile()
    // {
    //     DB::statement(DB::raw('set @rownum=0'));
    //     $query = new \App\ModelDetailPemesanan();
    //     $data = ModelPemesanan::select([
    //         DB::raw('@rownum  := @rownum  + 1 AS rownum'),
    //         'id_order',
    //         'no_soki', 
    //         'no_sp',
    //         'nama_order',
    //         'nama_pemesan',
    //         'tgl_order'
    //     ])->whereDoesntHave('detail')->orWhereIn('id_order', $query->where(['file' => NULL])->get(['id_order']));

    //     return Datatables::of($data)
    //         ->addColumn('aksi', function($data) {
    //                 return '
    //                         <a href="pemesanan/detail_nofile/'.$data->id_order.'" class="btn btn-xs btn-warning">
    //                             <i class="glyphicon glyphicon-list"></i> detail
    //                         </a>
    //                         <a href="pemesanan/edit/'.$data->id_order.'" class="btn btn-xs btn-primary" style="margin-top: 2px">
    //                             <i class="glyphicon glyphicon-edit"></i> edit
    //                         </a>
    //                         <a data-href="pemesanan/delete/'.$data->id_order.'" class="btn btn-danger btn-xs"
    //                             data-toggle="modal" data-target="#modalHapus" style="margin-top: 2px">
    //                             <span class="glyphicon glyphicon-trash"></span> hapus</a>
    //                         ';
    //             })
    //         ->addColumn('status', function($data){
    //                 $st = ModelPemesanan::where(['id_order'=>$data->id_order])->first();
    //                 return $st->status($data->id_order);
    //             })
    //         ->make(true);
    // }

//    public function showform(Request $request)
//    {
//        $data = [
//              'title' => 'Tambah Data Pemesanan',
//
//          ];
//        return view('page.order.tambahPemesanan', $data);
//    }

    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'no_soki' => 'required',
                'no_sp' => 'required',
                'nama_pesanan' => 'required',
                'id_penerima' => 'required',
                'nama_pemesan' => 'required',
                'alamat_pemesan' => 'required',
                'no_telp_pemesan' => 'required',
                'ket' => 'required'
            ]);
        if($valid->fails()) {
             return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $no_soki = $request->no_soki;
            $no_sp = $request->no_sp;
            $nama_pesanan = $request->nama_pesanan;
            $id_penerima = $request->id_penerima;
            $nama_pemesan = $request->nama_pemesan;
            $tgl_buat = $request->tgl_buat;
            $alamat_pemesan = $request->alamat_pemesan;
            $no_telp_pemesan = $request->no_telp_pemesan;
            $status = 1;
            $ket = $request->ket;

            $data = [
                        'no_soki' => $no_soki,
                        'no_sp' => $no_sp,
                        'nama_order' => $nama_pesanan,
                        'id_penerima' => $id_penerima,
                        'nama_pemesan' => $nama_pemesan,
                        'alamat_pemesan' => $alamat_pemesan,
                        'no_telp_pemesan' => $no_telp_pemesan,
                        'tgl_order' => '2018-12-12',
                        'status' => $status,
                        'ket' => $ket
                  ];

            $save = ModelPemesanan::create($data);
            $getId = $save->id_order;

            return redirect("pemesanan/detail/$getId");
        }
    }

    public function showEdit($id)
    {
        $cek = ModelPemesanan::where(['id_order' => $id])->first();
        $data = [
                'title' => 'Edit Data Pemesanan',
                'record' => $cek
          ];
        return view('page.order.editPemesanan', $data);
    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_pesanan' => 'required',
                'nama_pemesan' => 'required',
                'alamat_pemesan' => 'required',
                'no_telp_pemesan' => 'required',
                'ket' => 'required'
            ]);
        if($valid->fails()) {
             return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_order = $request->id_order;
            $no_soki = $request->no_soki;
            $no_sp = $request->no_sp;
            $nama_pesanan = $request->nama_pesanan;
            $id_penerima = $request->id_penerima;
            $nama_pemesan = $request->nama_pemesan;
            $tgl_buat = $request->tgl_buat;
            $alamat_pemesan = $request->alamat_pemesan;
            $no_telp_pemesan = $request->no_telp_pemesan;
            $ket = $request->ket;

            $data = [
                        'nama_order' => $nama_pesanan,
                        'nama_pemesan' => $nama_pemesan,
                        'alamat_pemesan' => $alamat_pemesan,
                        'no_telp_pemesan' => $no_telp_pemesan,
                        'tgl_order' => '2018-12-12',
                        'ket' => $ket
                  ];

            $save = ModelPemesanan::where(['id_order' => $id_order ])->update($data);
            return redirect("pemesanan");
        }
    }

    public function detailPemesanan($id)
    {
        $record = ModelPemesanan::where([ 'id_order' => $id ])->first();
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_order' => $id, 'status' => '1'])->get();
        // if(($user->level == 1) or ($user->level == 2)){
        //     $data = [
        //                 'title' => 'Detail Pemesanan',
        //                 'row' => $record,
        //                 // 'det' => $record_detail,
        //                 'det' => $record->detail_file,
        //                 'level' => $user->level
        //         ];
        // }else{
        //     $data = [
        //         'title' => 'Detail Pemesanan',
        //         'row' => $record,
        //         // 'det' => $record_detail,
        //         'det' => $record->detail,
        //         'level' => $user->level
        //     ];
        // }
        $data = [
            'title' => 'Detail Pemesanan',
            'row' => $record,
            // 'det' => $record_detail,
            'det' => $record->detail_file,
            'status' => 1
        ];

        return view('page.order.detailPemesanan', $data);
    }

    // public function detailPemesananNoFile($id)
    // {
    //     $user = auth::user();
    //     $record = ModelPemesanan::where([ 'id_order' => $id ])->first();
    //     $record_detail = \App\ModelDetailPemesanan::where([ 'id_order' => $id, 'status' => '1'])->get();
    //     $data = [
    //                 'title' => 'Detail Pemesanan',
    //                 'row' => $record,
    //                 // 'det' => $record_detail,
    //                 'det' => $record->detail_no_file,
    //                 'status' => 0
    //         ];

    //     return view('page.order.detailPemesanan', $data);
    // }

    public function delete($id)
    {
        $cek_data = \App\ModelDetailPemesanan::where(['id_order' => $id])->first();
        $hapus = ModelPemesanan::where(['id_order' => $id]);
        if(empty($cek_data)){
            if(!empty($hapus)) {
                $hapus->delete();
            }
        }
        return redirect('pemesanan');
    }
    
    public function add(){
        $data = [
              'title' => 'Add Data Pemesanan',
              'content' => 'saat ini anda di halaman data pemesanan',
              'list_jenis_percetakan' => \App\ModelJenisPercetakan::dropdown()
          ];
        
        return view('page.order.addPemesanan', $data);
    }
    
    public function save_pemesanan(Request $request){
        
        $valid = Validator::make($request->all(), [
                'nama_pesanan' => 'required',
                'nama_pemesan' => 'required',
                'alamat_pemesan' => 'required',
                'no_telp_pemesan' => 'required',
                'ket' => 'required',
                'nama_produk' => 'required',
                'id_jenis_percetakan' => 'required',
            ]);
        if($valid->fails()) {
             return redirect()->back()->withErrors($valid)->withInput();
        }else{
            if(empty($request->input('nama_produk'))){
                return redirect()->back();
            }
            
            $user = Auth::user();
            
            $model = new ModelPemesanan();
            $model->id_penerima = $user->id_karyawan;
            $model->nama_order = $request->input('nama_pesanan');
            $model->nama_pemesan = $request->input('nama_pemesan');
            $model->alamat_pemesan = $request->input('alamat_pemesan');
            $model->no_telp_pemesan = $request->input('no_telp_pemesan');
            $model->ket = $request->input('ket');
            $model->status = 0;
            $model->tgl_order = date('Y-m-d');
            
            if($model->save()){
                $model->no_soki = $model->id_order;
                $model->no_sp = $model->id_order;
                $model->update();
                $id_jenis_percetakan = $request->input('id_jenis_percetakan');
                foreach ($request->input('nama_produk') as $key=>$item){
                    $detail = new \App\ModelDetailPemesanan();
                    $detail->id_order = $model->id_order;
                    $detail->produk = $item;
                    $detail->id_jenis_percetakan = $id_jenis_percetakan[$key];
                    $detail->status = 0;
                    $detail->save();
                }
            }
            
            $user = Auth::user();
            if(($user->level == 1) or ($user->level == 2)){
                return redirect("pemesanan/detail_nofile/$model->id_order");
            }else{
                return redirect("data_pemesanan/detail/$model->id_order");
            }
        }
    }
    
    public function show_detail($id){
        $data = \App\ModelDetailPemesanan::where(['id_detail_order'=>$id])->first();
        return view('page.order.show', compact('data'));
    }
    public function show_detail_new($id){
        $data = \App\ModelDetailPemesanan::where(['id_detail_order'=>$id])->first();
        return view('page.order.show_new', compact('data'));
    }
    
    public function save_detail(Request $request){
        
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id')])->first();
        $model->oplah = $request->input('oplah');
        $model->satuan = $request->input('satuan');
        $model->jenis_order = $request->input('jenis_pesanan');
        $model->panjang = $request->input('panjang');
        $model->lebar = $request->input('lebar');
        $model->jml_hlm_isi = $request->input('jml_hal');
        $model->save();
        
        return response()->json(array('success' => true));
    }
    
    public function save_pracetak(Request $request){
        
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        $pracetak = \App\ModelPraCetak::where(['id_pra_cetak'=>$request->input('id')])->first();
        if(empty($pracetak)){
            $pracetak = new \App\ModelPraCetak();
        }
        $pracetak->id_detail_order = $model->id_detail_order;
        $pracetak->id_komponen = $request->input('id_komponen');
        $pracetak->id_mesin = $request->input('id_mesin');
        $pracetak->up = $request->input('up');
        $pracetak->model_montage = $request->input('model_montage');
        $pracetak->master = $request->input('master');
        $pracetak->ket = $request->input('ket');
        $pracetak->save();
        
        return response()->json(array('success' => true));
    }
    
    public function save_kebbahan(Request $request){
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        $data = \App\ModelKebBahan::where(['id_keb_bahan'=>$request->input('id')])->first();
        if(empty($data)){
            $data = new \App\ModelKebBahan();
        }
        
        $data->id_detail_order = $model->id_detail_order;
        $data->id_bahan = $request->input('id_bahan');
        $data->panjang = $request->input('panjang');
        $data->lebar = $request->input('lebar');
        $data->pl_jadi = $request->input('pl_jadi');
        $data->jml_lbr_p = $request->input('jml_lembar_p');
        $data->ket = $request->input('ket');
        $data->save();
        
        return response()->json(array('success' => true));
    }
    
    public function save_cetak(Request $request){
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        $data = \App\ModelCetak::where(['id_cetak'=>$request->input('id')])->first();
        if(empty($data)){
            $data = new \App\ModelCetak();
        }
        $data->id_detail_order = $model->id_detail_order;
        $data->model_cetak = $request->input('model_cetak');
        $data->hal = $request->input('hal');
        $data->id_tinta = $request->input('id_tinta');
        $data->jml_set = $request->input('jml_set');
        $data->insheet_tiap_set = $request->input('insheet_tiap_set');
        $data->tot_druck_per_set = $request->input('total_druck_per_set');
        $data->save();
        
        
        
        
        return response()->json(array('success' => true));
    }
    
    public function save_finish(Request $request){
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        $data = \App\ModelFinish::where(['id_finish'=>$request->input('id')])->first();
        if(empty($data)){
            $data = new \App\ModelFinish();
        }
        $data->id_detail_order = $model->id_detail_order;
        $data->id_finishing = $request->input('id_finishing');
        $data->ket = $request->input('ket');
        $data->save();
        
        $model->cek_rencana();
        
        
        return response()->json(array('success' => true));
    }

    public function upload(Request $request)
    {
        
        $valid = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'file' => 'required|file|mimes:pdf'
        ]);
        if($valid->fails()) {
            \Illuminate\Support\Facades\Session::flash('error', $valid->errors()->first());
            return redirect()->back();
        }
        $nama_file = $request->file('file')->getClientOriginalName();
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->id])->first();
        $model->file = $nama_file;
        $request->file('file')->move(
            base_path() . '/public/file/', $nama_file);
        $model->save();

        $model->cek_rencana();

        $user = Auth::user();
        if(($user->level == 1) or ($user->level == 2)){
            return redirect("pemesanan/detail/$model->id_order");
        }else{
            return redirect("data_pemesanan/detail/$model->id_order");
        }
    }
    
    public function select_bahan(Request $request){
        $plano = \App\ModelPlano::where(['id_plano'=>$request->id])->first();
        
        $item = \App\ModelBahan::dropdown_by_plano($request->id);
        return response()->json(array('success' => true,'item'=>$item,'plano'=>$plano));
    }
    
    public function select_mesin(Request $request){
        
//        $mesin_komponen = \App\ModelMesinKomponen::where(['id_komponen'=>$request->id_komponen])->get();
//        $id_mesin = array();
//        foreach ($mesin_komponen as $item){
//            $id_mesin[] = $item->id_mesin;
//        }
//        
        $mesin_plano = \App\ModelMesinPlano::where(['id_plano'=>$request->id_plano])->get();
        $mesin = array();
        foreach ($mesin_plano as $value){
            $mesin[$value->id_mesin] = $value->mesin->no_mesin . '('.$value->mesin->nama_mesin.')';
        }
        
        return response()->json(array('success' => true,'item'=>$mesin));
    }
    
    public function save_pracetak_bahan(Request $request){
        
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        
        $pracetak = new \App\ModelPraCetak();
        $pracetak->id_detail_order = $model->id_detail_order;
        $pracetak->id_komponen = $request->input('id_komponen');
        $pracetak->id_mesin = $request->input('id_mesin');
        $pracetak->up = $request->input('up');
        $pracetak->model_montage = $request->input('model_montage');
        $pracetak->master = $request->input('master');
        $pracetak->ket = $request->input('keterangan');
        $pracetak->save();
        
        $keb_bahan = new \App\ModelKebBahan();
        
        $keb_bahan->id_detail_order = $model->id_detail_order;
        $keb_bahan->id_pra_cetak = $pracetak->id_pra_cetak;
        $keb_bahan->id_komponen = $request->input('id_komponen');
        $keb_bahan->id_bahan = $request->input('id_bahan');
        $keb_bahan->panjang = $request->input('panjang_nc');
        $keb_bahan->lebar = $request->input('panjang_nc');
        $keb_bahan->pl_jadi = $request->input('plano_jadi');
        $keb_bahan->jml_lbr_p = $request->input('jml_lembar_plano');
        $keb_bahan->ket = $request->input('keterangan');
        $keb_bahan->save();
        
        $total_druck = $keb_bahan->jml_lbr_p * $keb_bahan->pl_jadi * $pracetak->up;
        
        if($pracetak->id_komponen == 2){
            $total_druck = $total_druck * 2 / $model->jml_hlm_isi;
        }
        
        $total_druck = floor($total_druck);
        $insheet =  ($total_druck - $model->oplah)/$model->oplah * 100;
        $insheet = round($insheet, 2);
        $cetak = new \App\ModelCetak();
        
        $cetak->id_detail_order = $model->id_detail_order;
        $cetak->id_pra_cetak = $pracetak->id_pra_cetak;
        $cetak->id_keb_bahan = $keb_bahan->id_pra_cetak;
        $cetak->id_komponen = $request->input('id_komponen');
        $cetak->hal = null;
        $cetak->id_tinta = null;
        $cetak->jml_set = null;
        $cetak->jml_cetak_per_set = $model->oplah;
        
        $cetak->insheet_tiap_set = $insheet;
        $cetak->tot_druck_per_set = $total_druck;
        $cetak->save();
        
        $returnHTML = view('page.order.table_pracetak_bahan', compact('model'))->render();
        $returnHTML2 = view('page.order.table_cetak', compact('model'))->render();
        $success = true;
        return response()->json(array('success' => $success, 'html' => $returnHTML,'html2'=>$returnHTML2));
    }
    public function save_cetak_new(Request $request){
        
        $data = \App\ModelCetak::where(['id_cetak'=>$request->id_cetak])->first();
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$data->id_detail_order])->first();
        $data->ctk = $request->input('ctk');
        $data->hal = $request->input('hal');
        $data->id_tinta = $request->input('id_tinta');
        $data->jml_set = $request->input('jml_set');
        $data->save();
        
        $returnHTML = view('page.order.table_cetak', compact('model'))->render();
        $success = true;
        return response()->json(array('success' => $success, 'html' => $returnHTML));
    }
    public function save_finishing_new(Request $request){
        
        $model = \App\ModelDetailPemesanan::where(['id_detail_order'=>$request->input('id_detail')])->first();
        
        $data = new \App\ModelFinish();
        $data->id_detail_order = $model->id_detail_order;
        $data->id_finishing = $request->input('id_finishing');
        $data->ket = $request->input('ket');
        $data->save();
        
        $returnHTML = view('page.order.table_finishing', compact('model'))->render();
        $success = true;
        return response()->json(array('success' => $success, 'html' => $returnHTML));
    }
}
