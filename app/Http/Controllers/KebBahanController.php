<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ModelKebBahan;
use App\ModePraCetak;
use Yajra\Datatables\Datatables;
use Validator;

class KebBahanController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
      $data = [
                  'title' => 'Keb Bahan',
          ];
      return view('page.keb_bahan.keb_bahan', $data);
    }

    public function showform(Request $request)
    {
      //
    }

    public function showData()
    {
      // $data = \App\ModelDetailPemesanan::where(['finish_bahan'=>0,'status'=>1])->get();
      // $no = 1;
      
        DB::statement(DB::raw('set @rownum=0'));
        $data = \App\ModelDetailPemesanan::with(['order'])
                ->where(['finish_bahan'=>0,'status'=>2])
                ->select(['detail_order.*',DB::raw('@rownum  := @rownum  + 1 AS no')]);
        return Datatables::of($data)
        ->addColumn('no_soki', function ($data){
              return $data->order->no_soki;
          })
          ->addColumn('aksi', function($data) {
                if(($data->finish_pracetak != 1) && ($data->finish_bahan != 1)){
                    $aksi = '
                                <a href="kebBahan/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                    <i class="glyphicon glyphicon-list"></i> detail
                                </a>
                                <a data-href="detailPemesanan/delete/'.$data->id_detail_order.'" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus
                                </a>
                            ';
                }else{
                    $aksi = '<a href="kebBahan/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>';
                }

                return $aksi;
              })
          ->make(true);
    }

    public function kebBahan($id)
    {
        $record = \App\ModelPraCetak::where([ 'id_pra_cetak' => $id ])->first();
        $record_kebBahan = \App\ModelKebBahan::where([ 'id_pra_cetak' => $id ])->get();
        $data = [
                    'title' => 'Kebutuhan Bahan',
                    'row' => $record,
                    'det' => $record_kebBahan
            ];

        return view('page.keb_bahan.kebBahan', $data);
    }

    public function detailKebBahan($id)
    {
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_detail_order' => $id ])->first();
        $data = [
                'title' => 'Detail Pra Cetak',
                'det' => $record_detail,
                'detBahan' => $record_detail->kebbahan()->get()
            ];

        return view('page.keb_bahan.detailKebBahan', $data);
    }

    public function completed($id)
    {
        $data = ModelKebBahan::where(['id_keb_bahan' => $id])->first();
        if(!empty($data)) {
            $data->status = 1;
            $data->save();

            $data->detail_order->cek_tahap();
        }
        return redirect()->back();
    }
}
