<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PlanoDistributorController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelPlanoDistributor::query();
        $nama_plano = Input::get('nama_plano');
        $nama_distributor = Input::get('nama_distributor');

        if (!empty($nama_plano)) {
            $q->whereHas('plano',function($query) use ($nama_plano){
                $query->where('nama', 'like', '%' . $nama_plano . '%');
            });
        }

        if (!empty($nama_distributor)) {
            $q->whereHas('distributor',function($query) use ($nama_distributor){
                $query->where('nama', 'like', '%' . $nama_distributor . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_plano = \App\ModelPlano::dropdown();
        $list_distributor = \App\ModelDistributor::dropdown();

        return view('page.plano_distributor.index', compact('data', 'nama_plano','nama_distributor', 'list_plano', 'list_distributor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_plano' => 'required',
                    'id_distributor' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('plano_distributor');
        }

        $exist_other = \App\ModelPlanoDistributor::where('id_distributor', $request->id_distributor)
                        ->where('id_plano', $request->id_plano)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelPlanoDistributor Same Data found. Please, try again.');
            return redirect('plano_distributor');
        }

        $model = new \App\ModelPlanoDistributor();
        $model->id_plano = $request->id_plano;
        $model->id_distributor = $request->id_distributor;

        if ($model->save()) {

            Session::flash('success', 'ModelPlanoDistributor has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelPlanoDistributor could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('plano_distributor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelPlanoDistributor::where('id_plano_distributor', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelPlanoDistributor not found. Please, try again.');
            return redirect('plano_distributor');
        }

        //do cek relation

        $model->delete();
        return redirect('plano_distributor');
    }

}
