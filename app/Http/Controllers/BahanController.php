<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelBahan;
use Validator;

class BahanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$field = ModelBahan::all();
    	$data = [
    				'title' => 'Data Bahan',
    				'row' => $field
    		];
    	return view('page.bahan.bahan', $data);
    }

    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_bahan' => 'required',
                'id_plano' => 'required',
                'panjang' => 'required|numeric',
                'lebar' => 'required|numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $nama_bahan = $request->nama_bahan;
            $panjang = $request->panjang;
            $lebar = $request->lebar;
            $id_plano = $request->id_plano;
            $data = [
                        'nama_bahan' => $nama_bahan,
                        'id_plano' => $id_plano,
                        'panjang' => $panjang,
                        'lebar' => $lebar
                    ];

            ModelBahan::create($data);
            return redirect('bahan');
        }


    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_bahan' => 'required',
                'id_plano' => 'required',
                'panjang' => 'required|numeric',
                'lebar' => 'required|numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id = $request->id_bahan;
            $nama_bahan = $request->nama_bahan;
            $panjang = $request->panjang;
            $lebar = $request->lebar;
            $id_plano = $request->id_plano;
            $data = [
                        'nama_bahan' => $nama_bahan,
                        'id_plano' => $id_plano,
                        'panjang' => $panjang,
                        'lebar' => $lebar
                    ];
            $edit = ModelBahan::where(['id_bahan' => $id])->update($data);
            return redirect('bahan');
        }
    }

    public function delete($id)
    {
        $hapus = ModelBahan::where(['id_bahan' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('bahan');
    }
}
