<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelCetak;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Validator;

class CetakController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data = [
                    'title' => 'Cetak',
            ];
        return view('page.cetak.cetak', $data);
    }

    public function showData()
    {
        // $data = \App\ModelDetailPemesanan::where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>0,'status'=>1]);

        // $no = 1;

        DB::statement(DB::raw('set @rownum=0'));
        $data = \App\ModelDetailPemesanan::with(['order'])
                ->where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>0,'status'=>2])
                ->select(['detail_order.*',DB::raw('@rownum  := @rownum  + 1 AS no')]);
        return Datatables::of($data)
            ->addColumn('no_soki', function ($data){
                return $data->order->no_soki;
            })
            ->addColumn('aksi', function($data) {
                    return '
                            <a href="cetak/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="showEdit('.$data->id_detail_order. ')">
					 		    <span class="glyphicon glyphicon-edit"></span> edit
                            </a>
                            <a data-href="detailPemesanan/delete/'.$data->id_detail_order.'" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                <span class="glyphicon glyphicon-trash"></span> hapus
                            </a>
                            ';
                })
            ->make(true);
    }

    public function detailCetak($id)
    {
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_detail_order' => $id ])->first();
        $data = [
                'title' => 'Detail Cetak',
                'det' => $record_detail,
                'warna' => \App\ModelTinta::all(),
                'detCetak' => $record_detail->cetak()->get()
            ];

        return view('page.cetak.detailCetak', $data);
    }

    public function showDataEdit($id)
    {
        $data = ModelCetak::where(['id_cetak' => $id])->first();
        $dat = [
            'kombahan' => $data->pracetak->komponen['nama_komponen'] .' : '.$data->kebbahan->bahan['nama_bahan'],
            'pjg' => $data->kebbahan['panjang'],
            'lbr' => $data->kebbahan['lebar'],
            'mesin' => $data->pracetak->mesin['nama_mesin'],
            'id_tinta' => $data->tinta['id_tinta'],
            'warna' => $data->tinta['warna'],
            'deCetak' => $data
        ];
        return response()->json($dat);
    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'ctk' => 'required',
                'tinta' => 'required',
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_detail_order = $request->id_detail_order;
            $id_cetak = $request->id_cetak;
            $model_cetak = $request->ctk;
            $hal = $request->hal;
            $id_tinta = $request->tinta;
            $jml_set = $request->jml_set;
            $insheet_tiap_set = $request->insh;
            $jml_ctk = $request->jml_ctk;
            $tdruck = $jml_ctk + ($jml_ctk * $insheet_tiap_set / 100 );
            $tot_druck_per_set = $tdruck;

            $data = [
                        'model_cetak' => $model_cetak,
                        'hal' => $hal,
                        'id_tinta' => $id_tinta,
                        'jml_set' => $jml_set,
                        'insheet_tiap_set' => $insheet_tiap_set,
                        'tot_druck_per_set' => $tot_druck_per_set
                    ];

            ModelCetak::where(['id_cetak' => $id_cetak ])->update($data);
            $getId = $id_detail_order;
            return redirect("cetak/detail/$getId");
        }
    }

    public function completed($id)
    {
        $data = ModelCetak::where(['id_cetak' => $id])->first();
        if(!empty($data)) {
            $data->status = 1;
            $data->save();

            $data->detail_order->cek_tahap();
        }
        return redirect()->back();
    }
}
