<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelMesin;
use Validator;

class MesinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$field = ModelMesin::all();
    	$data = [
    				'title' => 'Data Mesin',
    				'content' => 'saat ini anda di halaman data mesin',
    				'row' => $field,
    		];
    	return view('page.mesin.mesin', $data);
    }

    public function coba(){
        echo "aaaa";
    }

    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'no_mesin' => 'required',
                'nama_mesin' => 'required',
                'id_tipe_mesin' => 'required'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $no_mesin = $request->no_mesin;
            $nama_mesin = $request->nama_mesin;
            $id_tipe_mesin = $request->id_tipe_mesin;
            $data = [
                        'no_mesin' => $no_mesin,
                        'nama_mesin' => $nama_mesin,
                        'id_tipe_mesin' => $id_tipe_mesin
                    ];

            ModelMesin::create($data);
            return redirect('mesin');
        }
    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_mesin' => 'required',
                'no_mesin' => 'required',
                'id_tipe_mesin' => 'required'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id = $request->id_mesin;
            $no_mesin = $request->no_mesin;
            $nama_mesin = $request->nama_mesin;
            $id_tipe_mesin = $request->id_tipe_mesin;
            $data = [
                        'nama_mesin' => $nama_mesin,
                        'no_mesin' => $no_mesin,
                        'id_tipe_mesin' => $id_tipe_mesin
                    ];
            $edit = ModelMesin::where(['id_mesin' => $id])->update($data);
            return redirect('mesin');
        }
    }

    public function delete($id)
    {
        $hapus = ModelMesin::where(['id_mesin' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('mesin');
    }
}
