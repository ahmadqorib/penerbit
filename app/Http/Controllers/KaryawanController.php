<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelKaryawan;
use Validator;
use Auth;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        // $this->middleware('auth');
        // $user = Auth::user();
        // if(($user->level != 1) or ($user->level == 2)){
        //     return redirect()->back();
        // }
        
    	$field = ModelKaryawan::all();
    	$data = [
    				'title' => 'Data Karyawan',
    				'row' => $field
    		];
    	return view('page.karyawan.karyawan', $data);
    }

    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_karyawan' => 'required',
                'alamat' => 'required',
                'no_telp' => 'required',
                'username'=>'required',
                'level'=>'required',
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $model = new ModelKaryawan();
            $model->nama_karyawan = $request->input('nama_karyawan');
            $model->username = str_replace(' ', '_', $request->input('username'));
            $model->alamat = $request->input('alamat');
            $model->no_telp = $request->input('no_telp');
            $model->level = $request->input('level');
            $model->password = md5('12345678');
            $model->save();
//            $nama_karyawan = $request->nama_karyawan;
//            $alamat = $request->alamat;
//            $no_telp = $request->no_telp;
//            
//            $data = [
//                        'nama_karyawan' => $nama_karyawan,
//                        'alamat' => $alamat,
//                        'no_telp' => $no_telp
//                    ];
//
//            ModelKaryawan::create($data);
            return redirect('karyawan');
        }


    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_karyawan' => 'required',
                'alamat' => 'required',
                'no_telp' => 'required|numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id = $request->id_karyawan;
            $nama_karyawan = $request->nama_karyawan;
            $alamat = $request->alamat;
            $no_telp = $request->no_telp;
            $data = [
                        'nama_karyawan' => $nama_karyawan,
                        'alamat' => $alamat,
                        'no_telp' => $no_telp
                    ];
            $edit = ModelKaryawan::where(['id_karyawan' => $id])->update($data);
            return redirect('karyawan');
        }
    }

    public function delete($id)
    {
        $hapus = ModelKaryawan::where(['id_karyawan' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('karyawan');
    }
}
