<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MesinTintaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelMesinTinta::query();
        $nama_tinta = Input::get('nama_tinta');
        $nama_mesin = Input::get('nama_mesin');

        if (!empty($nama_tinta)) {
            $q->whereHas('tinta',function($query) use ($nama_tinta){
                $query->where('warna', 'like', '%' . $nama_tinta . '%');
            });
        }

        if (!empty($nama_mesin)) {
            $q->whereHas('mesin',function($query) use ($nama_mesin){
                $query->where('nama_mesin', 'like', '%' . $nama_mesin . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_tinta = \App\ModelTinta::dropdown();
        $list_mesin = \App\ModelMesin::dropdown();

        return view('page.mesin_tinta.index', compact('data', 'nama_tinta','nama_mesin', 'list_tinta', 'list_mesin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_tinta' => 'required',
                    'id_mesin' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('mesin_tinta');
        }

        $exist_other = \App\ModelMesinTinta::where('id_mesin', $request->id_mesin)
                        ->where('id_tinta', $request->id_tinta)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelMesinTinta Same Data found. Please, try again.');
            return redirect('mesin_tinta');
        }

        $model = new \App\ModelMesinTinta();
        $model->id_tinta = $request->id_tinta;
        $model->id_mesin = $request->id_mesin;

        if ($model->save()) {

            Session::flash('success', 'ModelMesinTinta has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelMesinTinta could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('mesin_tinta');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelMesinTinta::where('id_mesin_tinta', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelMesinTinta not found. Please, try again.');
            return redirect('mesin_tinta');
        }

        //do cek relation

        $model->delete();
        return redirect('mesin_tinta');
    }

}
