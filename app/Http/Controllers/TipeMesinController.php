<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TipeMesinController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelTipeMesin::query();
        $default = true;
        $nama = Input::get('nama');
        
        if (!empty($nama)) {
            $q->where('nama', 'like', '%' . $nama . '%');
        }

        $data = $q->paginate(10);
        
        
        return view('page.tipe_mesin.index', compact('data','nama'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
            'nama' => 'required|unique:tipe_mesin',
            
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('tipe_mesin');
        }
            
            $model = new \App\ModelTipeMesin();
            $model->nama = $request->nama;
            
            if ($model->save()) {
                
                Session::flash('success', 'ModelTipeMesin has been saved.');
    //            return redirect('bus');
            } else {
                Session::flash('error', 'ModelTipeMesin could not be saved. Please, try again.');
    //            return redirect('categories/create')->withInput($request->except('_token'));
            }
        
        
        
        return redirect('tipe_mesin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $valid = Validator::make($request->all(), [
            'id_tipe_mesin'=>'required',
            'nama' => 'required',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('tipe_mesin');
        }
        
        $id = $request->id_tipe_mesin;
        
        $exist_other = \App\ModelTipeMesin::where('id_tipe_mesin','!=', $id)->where('nama',$request->nama)->first();
        if(!empty($exist_other)){
            Session::flash('error', 'ModelTipeMesin Same Data found. Please, try again.');
            return redirect('plano');
        }
        
        $model = \App\ModelTipeMesin::where('id_tipe_mesin', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelTipeMesin not found. Please, try again.');
            return redirect('tipe_mesin');
        }
        
        $model->nama = $request->nama;
        $model->update();
                
        return redirect('tipe_mesin');
    }

    public function delete($id = null) {
        $model = \App\ModelTipeMesin::where('id_tipe_mesin', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelTipeMesin not found. Please, try again.');
            return redirect('tipe_mesin');
        } 
        
        //do cek relation
        
        $model->delete();
        return redirect('tipe_mesin');
    }

}
