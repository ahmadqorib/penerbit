<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ApiModel;
use App\ModelKaryawan;

class V1Controller extends Controller {

    //

    public function call() {

        $api = new ApiModel();
//        var_dump($api->signature);die();
        $post = file_get_contents("php://input");
        $data = json_decode($post, true);
        $log = new \App\ApiLog();
        $log->request = $post;
        if (!empty($data) AND is_array($data)) {
            if (empty($data['signature']) OR $data['signature'] != $api->signature) {
                $output = [
                    'status' => 204,
                    'message' => 'Invalid signature',
                    'errorMessage' => 'Invalid signature',
                    'data' => null,
                ];
                $log->response = json_encode($output);
                $log->save();
                return json_encode($output);
            }

            $authKey = isset($data['apiKey']) ? $data['apiKey'] : 'no-key';
            $log->apikey = $authKey;
            if ($authKey != 'no-key') {
                $api->userModel = ModelKaryawan::where('apikey', $authKey)->first();
                if (empty($api->userModel)) {
                    $output = [
                        'status' => 202,
                        'message' => 'Invalid User',
                        'errorMessage' => 'Invalid User',
                        'data' => null,
                    ];
                    
                    $log->response = json_encode($output);
                    $log->save();
                    return json_encode($output);
                }
            }

            $api->HeaderCode = isset($data['HeaderCode']) ? $data['HeaderCode'] : null;
            $log->headercode = $api->HeaderCode;
            
            if (isset($data['data'])) {
                $api->data = $data['data'];
            } else {
                $api->data = array();
            }
            
            $log->data = json_encode($api->data);
        } else {

            $output = [
                'status' => 201,
                'message' => 'Invalid Request',
                'errorMessage' => 'Invalid Request',
                'data' => null,
            ];
            
            $log->response = json_encode($output);
            $log->save();
            return json_encode($output);
        }
        
        $response = $api->execute();
        $log->response = json_encode($response);
        $log->save();
        
        return view('api/call', ['data' => $response]);
    }
    
    public function upload(){
        $apiKey = Input::get('apiKey');
        $signature = Input::get('signature');
        $file = Input::file('file');
        $kategori = Input::get('kategori');
        $note = Input::get('note');
        $api = new ApiModel();
        
        if(empty($signature) || $signature != $api->signature){
            $output = [
                'status' => 104,
                'message' => 'Invalid signature',
                'errorMessage' => 'Invalid signature',
                'data' => null,
            ];
            return json_encode($output);
        }
        
        $user = ModelKaryawan::where('apikey',$apiKey)->first(); 
        
        if(empty($apiKey) || empty($user)){
            $output = [
                'status' => 102,
                'message' => 'Invalid User',
                'errorMessage' => 'Invalid User',
                'data' => null,
            ];
            return json_encode($output);
        }
        
        if(empty($file) || empty($kategori)){
            $output = [
                'status' => 103,
                'message' => 'Error Format Request',
                'errorMessage' => 'Error Format Request',
                'data' => null,
            ];
            return json_encode($output);
        }
        
        $rules = array('file' => 'required');
        $validator = \Illuminate\Support\Facades\Validator::make(array('file' => $file), $rules);
        $fileUpload = new \App\FilesUpload();    
        
        if ($validator->passes()) {
                $destinationPath = $fileUpload->folder;
                $baseName = $file->getClientOriginalName(); // get the original filename + extension
                $extension = $file->getClientOriginalExtension(); // get the original extension without the dot
                $size = $file->getSize();
                $fileName = basename($baseName, '.' . $extension); // get the original filename only
                $slug = str_slug($fileName, '-'); // slug the original filename
                $newName = $slug . '-' . time();
                $newFileName = $newName . '.' . $extension;
                $uploadSuccess = $file->move($destinationPath, $newFileName);
                
                
                $fileUpload->file = $fileUpload->folder.'/'. $newFileName;
                $fileUpload->id_user = $user->id_karyawan;
                $fileUpload->kategori = $kategori;
                $fileUpload->note = $note;
                $fileUpload->filesize = $size;
                $fileUpload->filename = $newFileName;
                        
                if($fileUpload->save()){
                
                    $dataResult = array(
                        'foto'=>$fileUpload->path_file(),
                        'kategori'=>$fileUpload->kategori,
                        'note'=>$fileUpload->note,
                        'filename'=>$fileUpload->filename,
                        'filesize'=>$fileUpload->filesize
                    );
                    $output = [
                        'status' => 200,
                        'message' => 'Upload Profile Success',
                        'errorMessage' => '',
                        'data' => $dataResult,
                    ];

                    return json_encode($output);
                }
        }
        
        
        $output = [
            'status' => 211,
            'message' => 'Upload File Filed',
            'errorMessage' => 'Upload File Filed',
        ];

        return json_encode($output);
    }
    
    
}
