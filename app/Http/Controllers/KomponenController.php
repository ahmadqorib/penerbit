<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelKomponen;
use Validator;

class KomponenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$field = ModelKomponen::all();
    	$data = [
    				'title' => 'Data Komponen',
    				'row' => $field
    		];
    	return view('page.komponen.komponen', $data);
    }

    public function create(Request $request)
    {
      $valid = Validator::make($request->all(), [
              'nama_komponen' => 'required'
          ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $nama_komponen = $request->nama_komponen;
            $data = [
                        'nama_komponen' => $nama_komponen
                    ];

            ModelKomponen::create($data);
            return redirect('komponen');
        }


    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_komponen' => 'required'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id = $request->id_komponen;
            $nama_komponen = $request->nama_komponen;
            $data = [
                        'nama_komponen' => $nama_komponen
                    ];
            $edit = ModelKomponen::where(['id_komponen' => $id])->update($data);
            return redirect('komponen');
        }
    }

    public function delete($id)
    {
        $hapus = ModelKomponen::where(['id_komponen' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('komponen');
    }
}
