<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ModelTinta;
use Validator;

class TintaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$field = ModelTinta::all();
    	$data = [
    				'title' => 'Data Tinta',
    				'row' => $field
    		];
    	return view('page.tinta.tinta', $data);
    }

    public function create(Request $request)
    {
      $valid = Validator::make($request->all(), [
              'warna' => 'required'
          ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $warna = $request->warna;
            $data = [
                        'warna' => $warna
                    ];

            ModelTinta::create($data);
            return redirect('tinta');
        }


    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'warna' => 'required'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id = $request->id_tinta;
            $warna = $request->warna;
            $data = [
                        'warna' => $warna
                    ];
            $edit = ModelTinta::where(['id_tinta' => $id])->update($data);
            return redirect('tinta');
        }
    }

    public function delete($id)
    {
        $hapus = ModelTinta::where(['id_tinta' => $id])->first();
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect('tinta');
    }
}
