<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PlanoStokController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelPlanoStok::query();
        $nama_plano = Input::get('nama_plano');
        

        if (!empty($nama_plano)) {
            $q->whereHas('plano',function($query) use ($nama_plano){
                $query->where('nama', 'like', '%' . $nama_plano . '%');
            });
        }
        
        $q->orderBy('created_at','desc');
        
        $data = $q->paginate(10);
       
        $list_plano = \App\ModelPlano::dropdown();

        return view('page.plano_stok.index', compact('data', 'nama_plano', 'list_plano'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_plano' => 'required',
                    'masuk'=>'required'
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('plano_stok');
        }

        $model = new \App\ModelPlanoStok();
        $model->id_plano = $request->id_plano;
        $model->masuk = $request->masuk;

        if ($model->save()) {
            $model->update_stok_plano();
            Session::flash('success', 'ModelPlanoStok has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelPlanoStok could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('plano_stok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelPlanoStok::where('id_plano_stok', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelPlanoStok not found. Please, try again.');
            return redirect('plano_stok');
        }

        //do cek relation

        $model->delete();
        $model->delete_stok_plano();
        
        return redirect('plano_stok');
    }

}
