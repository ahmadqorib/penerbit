<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DistributorController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelDistributor::query();
        $default = true;
        $nama = Input::get('nama');
        
        if (!empty($nama)) {
            $q->where('nama', 'like', '%' . $nama . '%');
        }

        $data = $q->paginate(10);
        
        
        return view('page.distributor.index', compact('data','nama'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
            'nama' => 'required|unique:distributor',
            'alamat' => 'required',
            'no_telp' => 'required',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('distributor');
        }
            
            $model = new \App\ModelDistributor();
            $model->nama = $request->nama;
            $model->alamat = $request->alamat;
            $model->no_telp = $request->no_telp;
            
            if ($model->save()) {
                
                Session::flash('success', 'ModelDistributor has been saved.');
    //            return redirect('bus');
            } else {
                Session::flash('error', 'ModelDistributor could not be saved. Please, try again.');
    //            return redirect('categories/create')->withInput($request->except('_token'));
            }
        
        
        
        return redirect('distributor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $valid = Validator::make($request->all(), [
            'id_distributor'=>'required',
            'nama' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
                   
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('distributor');
        }
        
        $id = $request->id_distributor;
        
        $exist_other = \App\ModelDistributor::where('id_distributor','!=', $id)->where('nama',$request->nama)->first();
        if(!empty($exist_other)){
            Session::flash('error', 'ModelDistributor Same Data found. Please, try again.');
            return redirect('distributor');
        }
        
        $model = \App\ModelDistributor::where('id_distributor', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelDistributor not found. Please, try again.');
            return redirect('distributor');
        }
        
        $model->nama = $request->nama;
        $model->alamat = $request->alamat;
        $model->no_telp = $request->no_telp;
        $model->update();
                
        return redirect('distributor');
    }

    public function delete($id = null) {
        $model = \App\ModelDistributor::where('id_distributor', $id)->first();
        if(empty($model)){
            Session::flash('error', 'ModelDistributor not found. Please, try again.');
            return redirect('distributor');
        } 
        
        //do cek relation
        
        $model->delete();
        return redirect('distributor');
    }

}
