<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MesinPlanoController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelMesinPlano::query();
        $nama_plano = Input::get('nama_plano');
        $nama_mesin = Input::get('nama_mesin');

        if (!empty($nama_plano)) {
            $q->whereHas('plano',function($query) use ($nama_plano){
                $query->where('nama', 'like', '%' . $nama_plano . '%');
            });
        }

        if (!empty($nama_mesin)) {
            $q->whereHas('mesin',function($query) use ($nama_mesin){
                $query->where('nama_mesin', 'like', '%' . $nama_mesin . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_plano = \App\ModelPlano::dropdown();
        $list_mesin = \App\ModelMesin::dropdown();

        return view('page.mesin_plano.index', compact('data', 'nama_plano','nama_mesin', 'list_plano', 'list_mesin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_plano' => 'required',
                    'id_mesin' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('mesin_plano');
        }

        $exist_other = \App\ModelMesinPlano::where('id_mesin', $request->id_mesin)
                        ->where('id_plano', $request->id_plano)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelMesinPlano Same Data found. Please, try again.');
            return redirect('mesin_plano');
        }

        $model = new \App\ModelMesinPlano();
        $model->id_plano = $request->id_plano;
        $model->id_mesin = $request->id_mesin;

        if ($model->save()) {

            Session::flash('success', 'ModelMesinPlano has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelMesinPlano could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('mesin_plano');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelMesinPlano::where('id_mesin_plano', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelMesinPlano not found. Please, try again.');
            return redirect('mesin_plano');
        }

        //do cek relation

        $model->delete();
        return redirect('mesin_plano');
    }

}
