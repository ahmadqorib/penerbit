<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class KomponenJenisPercetakanController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelKomponenJenisPercetakan::query();
        $nama_komponen = Input::get('nama_komponen');
        $nama_jenis_percetakan = Input::get('nama_jenis_percetakan');

        if (!empty($nama_komponen)) {
            $q->whereHas('komponen',function($query) use ($nama_komponen){
                $query->where('nama', 'like', '%' . $nama_komponen . '%');
            });
        }

        if (!empty($nama_jenis_percetakan)) {
            $q->whereHas('jenis_percetakan',function($query) use ($nama_jenis_percetakan){
                $query->where('nama', 'like', '%' . $nama_jenis_percetakan . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_komponen = \App\ModelKomponen::dropdown();
        $list_jenis_percetakan = \App\ModelJenisPercetakan::dropdown();

        return view('page.komponen_jenis_percetakan.index', compact('data', 'nama_komponen','nama_jenis_percetakan', 'list_komponen', 'list_jenis_percetakan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_komponen' => 'required',
                    'id_jenis_percetakan' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('komponen_jenis_percetakan');
        }

        $exist_other = \App\ModelKomponenJenisPercetakan::where('id_jenis_percetakan', $request->id_jenis_percetakan)
                        ->where('id_komponen', $request->id_komponen)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelKomponenJenisPercetakan Same Data found. Please, try again.');
            return redirect('komponen_jenis_percetakan');
        }

        $model = new \App\ModelKomponenJenisPercetakan();
        $model->id_komponen = $request->id_komponen;
        $model->id_jenis_percetakan = $request->id_jenis_percetakan;

        if ($model->save()) {

            Session::flash('success', 'ModelKomponenJenisPercetakan has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelKomponenJenisPercetakan could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('komponen_jenis_percetakan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelKomponenJenisPercetakan::where('id_komponen_jenis_percetakan', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelKomponenJenisPercetakan not found. Please, try again.');
            return redirect('komponen_jenis_percetakan');
        }

        //do cek relation

        $model->delete();
        return redirect('komponen_jenis_percetakan');
    }

}
