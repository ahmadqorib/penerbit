<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ModelPemesanan;
use Illuminate\Support\Facades\Redirect;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class PemesananNoFileController extends Controller
{
    public function showNoFile()
    {   
        $data = [
                    'title' => 'Data Pemesanan',
                    'karyawan' => \App\ModelKaryawan::all(),
                    'kondisi' => 0
            ];
        return view('page.orderNoFile.pemesananNoFile', $data);
    }

    public function showData_nofile()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $query = new \App\ModelDetailPemesanan();
        $data = ModelPemesanan::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id_order',
            'no_soki', 
            'no_sp',
            'nama_order',
            'nama_pemesan',
            'tgl_order'
        ])->whereDoesntHave('detail')->orWhereIn('id_order', $query->where(['file' => NULL])->get(['id_order']));

        return Datatables::of($data)
            ->addColumn('aksi', function($data) {
                    return '
                            <a href="pemesanan/detail_nofile/'.$data->id_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>
                            <a href="pemesanan/edit/'.$data->id_order.'" class="btn btn-xs btn-primary" style="margin-top: 2px">
                                <i class="glyphicon glyphicon-edit"></i> edit
                            </a>
                            <a data-href="pemesanan/delete/'.$data->id_order.'" class="btn btn-danger btn-xs"
                                data-toggle="modal" data-target="#modalHapus" style="margin-top: 2px">
                                <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            ';
                })
            ->addColumn('status', function($data){
                    $st = ModelPemesanan::where(['id_order'=>$data->id_order])->first();
                    return $st->status($data->id_order);
                })
            ->make(true);
    }

    public function detailPemesananNoFile($id)
    {
        $user = auth::user();
        $record = ModelPemesanan::where([ 'id_order' => $id ])->first();
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_order' => $id, 'status' => '1'])->get();
        $data = [
                    'title' => 'Detail Pemesanan',
                    'row' => $record,
                    // 'det' => $record_detail,
                    'det' => $record->detail_no_file,
            ];

        return view('page.orderNoFile.detailPemesananNoFile', $data);
    }
}
