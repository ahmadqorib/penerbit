<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelPemesanan;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Validator;
use Auth;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = [
            'title' => 'Data Pemesanan',
            'karyawan' => \App\ModelKaryawan::all(),
            'kondisi' => 1
        ];
        return view('page.sales.pemesananSales', $data);   
    }

    public function pemesanan()
    {
        $user = auth::user();
        $query = new \App\ModelDetailPemesanan();
        DB::statement(DB::raw('set @rownum=0'));
        $data = ModelPemesanan::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id_order',
            'no_soki', 
            'no_sp',
            'nama_order',
            'nama_pemesan',
            'tgl_order',
        ])->where(['id_penerima' => $user->id_karyawan]);

        return Datatables::of($data)
            ->addColumn('aksi', function ($data) {
                    return '
                            <a href="data_pemesanan/detail/'.$data->id_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>
                            <a href="data_pemesanan/edit/'.$data->id_order.'" class="btn btn-xs btn-primary" style="margin-top: 2px">
                                <i class="glyphicon glyphicon-edit"></i> edit
                            </a>
                            <a data-href="data_pemesanan/delete/'.$data->id_order.'" class="btn btn-danger btn-xs"
                                data-toggle="modal" data-target="#modalHapus" style="margin-top: 2px">
                                <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            ';
                })
            ->addColumn('status', function($data){
                    $st = ModelPemesanan::where(['id_order'=>$data->id_order])->first();
                    return $st->status($data->id_order);
                })
            ->make(true);
    }

    public function detailPemesananSales($id)
    {
        $user = auth::user();
        $record = ModelPemesanan::where([ 'id_order' => $id ])->first();
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_order' => $id, 'status' => '1'])->get();
            $data = [
                'title' => 'Detail Pemesanan',
                'row' => $record,
                // 'det' => $record_detail,
                'det' => $record->detail,
                'level' => $user->level
        ];
        return view('page.sales.detailPemesananSales', $data);
    }

    public function delete($id)
    {
        $cek_data = \App\ModelDetailPemesanan::where(['id_order' => $id])->first();
        $hapus = ModelPemesanan::where(['id_order' => $id]);
        if(empty($cek_data)){
            if(!empty($hapus)) {
                $hapus->delete();
            }
        }
        return redirect('data_pemesanan');
    }

    public function showEdit($id)
    {
        $cek = ModelPemesanan::where(['id_order' => $id])->first();
        $data = [
                'title' => 'Edit Data Pemesanan',
                'record' => $cek
          ];
        return view('page.sales.editPemesananSales', $data);
    }

    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'nama_pesanan' => 'required',
                'nama_pemesan' => 'required',
                'alamat_pemesan' => 'required',
                'no_telp_pemesan' => 'required',
                'ket' => 'required'
            ]);
        if($valid->fails()) {
             return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_order = $request->id_order;
            $no_soki = $request->no_soki;
            $no_sp = $request->no_sp;
            $nama_pesanan = $request->nama_pesanan;
            $nama_pemesan = $request->nama_pemesan;
            $tgl_buat = $request->tgl_buat;
            $alamat_pemesan = $request->alamat_pemesan;
            $no_telp_pemesan = $request->no_telp_pemesan;
            $ket = $request->ket;

            $data = [
                        'nama_order' => $nama_pesanan,
                        'nama_pemesan' => $nama_pemesan,
                        'alamat_pemesan' => $alamat_pemesan,
                        'no_telp_pemesan' => $no_telp_pemesan,
                        'tgl_order' => '2018-12-12',
                        'ket' => $ket
                  ];

            $save = ModelPemesanan::where(['id_order' => $id_order ])->update($data);
            return redirect("data_pemesanan");
        }
    }
}
