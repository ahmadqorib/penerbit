<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ModelPraCetak;
use Yajra\Datatables\Datatables;
use Validator;

class PraCetakController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data = [
                    'title' => 'Pra Cetak'
            ];
        return view('page.praCetak.praCetak', $data);
    }

    public function showform(Request $request)
    {
        $data = [
              'title' => 'Tambah Data Pra Cetak',

          ];
        return view('page.praCetak.tambahPraCetak', $data);
    }

    public function showData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = \App\ModelDetailPemesanan::with(['order'])
                ->where(['finish_pracetak'=>0,'status'=>2])
                ->select(['detail_order.*',DB::raw('@rownum  := @rownum  + 1 AS no')]);
//        
        return Datatables::of($data)
            ->addColumn('no_soki', function ($data){
                return $data->order->no_soki;
            })
            ->addColumn('aksi', function($data) {
                if(($data->finish_pracetak != 1) && ($data->finish_bahan != 1)){
                    $aksi = '<a href="praCetak/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>
                            <a data-href="detailPemesanan/delete/'.$data->id_detail_order.'" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                <span class="glyphicon glyphicon-trash"></span> hapus
                            </a>';
                }else{
                    $aksi = '<a href="praCetak/detail/'.$data->id_detail_order.'" class="btn btn-xs btn-warning">
                                <i class="glyphicon glyphicon-list"></i> detail
                            </a>';
                }
                    return $aksi;
            })
            ->make(true);
    }


    // public function kebBahan($id)
    // {
    //     $record = \App\ModelPraCetak::where([ 'id_pra_cetak' => $id ])->first();
    //     $record_kebBahan = \App\ModelKebBahan::where([ 'id_pra_cetak' => $id ])->get();
    //     $data = [
    //                 'title' => 'Kebutuhan Bahan',
    //                 'row' => $record,
    //                 'det' => $record_kebBahan
    //         ];
    //
    //     return view('page.praCetak.kebBahan', $data);
    // }

    public function KonfirmasiKebBahan($id)
    {
        $cek = \App\ModelDetailPemesanan::where(['id_detail_order' => $id])->first();
        if(!empty($cek)){
            $cek->update(['status'=>'3']);
            $getId = $cek->id_order;
        }
        $data = [
                    'title' => 'Pra Cetak',
            ];
        return view('page.praCetak', $data);
        // return route('praCetak');
    }
    
    public function detailPraCetak($id)
    {
        $record_detail = \App\ModelDetailPemesanan::where([ 'id_detail_order' => $id ])->first();
//        foreach($record->detail as $item){
//            foreach($item->pracetak as $subitem){
//                foreach($subitem->komponen as $subitems){
//                    echo $subitems;
//                }
//            }
//        }
        $item_mesin = \App\ModelMesin::all();
        $item_komponen = \App\ModelKomponen::all();
        $item_pra = $record_detail->praCetak()->get();
        $data = [
                'title' => 'Detail Pra Cetak',
                'det' => $record_detail,
                'pra' => $item_pra,
                'mesin' => $item_mesin,
                'komponen' => $item_komponen
            ];
        
        return view('page.praCetak.detailPraCetak', $data);
    }
    
    public function create(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'komponen' => 'required',
                'mesin' => 'required',
                'up' => 'numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_detail_order = $request->id_detail_order;
            $id_komponen = $request->komponen;
            $id_mesin = $request->mesin;
            $up = $request->up;
            $model_montage = $request->model_montage;
            $master = $request->master;
            $ket = $request->ket;
            
            $data = [
                        'id_detail_order' => $id_detail_order,
                        'id_komponen' => $id_komponen,
                        'id_mesin' => $id_mesin,
                        'up' => $up,
                        'model_montage' => $model_montage,
                        'master' => $master,
                        'ket' => $ket
                    ];

            ModelPraCetak::create($data);
            $getId = $id_detail_order;
            return redirect("praCetak/detail/$getId");
        }
    }
    
    public function showDataEdit($id)
    {
        $data = ModelPraCetak::where(['id_pra_cetak' => $id])->first();
        $dat = [
            'id_detail_order' => $data->id_detail_order,
            'id_pra_cetak' => $data->id_pra_cetak,
            'id_komponen' => $data->id_komponen,
            'nama_komponen' => $data->komponen['nama_komponen'],
            'id_mesin' => $data->id_mesin,
            'nama_mesin' => $data->mesin['nama_mesin'],
            'up' => $data->up,
            'model_montage' => $data->model_montage,
            'master' => $data->master,
            'ket' => $data->ket
        ];
        return response()->json($dat);
    }
    
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'komponen' => 'required',
                'mesin' => 'required',
                'up' => 'numeric'
            ]);
        if($valid->fails()) {
            return redirect()->back()->withErrors($valid)->withInput();
        }else{
            $id_detail_order = $request->id_detail_order;
            $id_pra_cetak = $request->id_pra_cetak;
            $id_komponen = $request->komponen;
            $id_mesin = $request->mesin;
            $up = $request->up;
            $model_montage = $request->model_montage;
            $master = $request->master;
            $ket = $request->ket;
            
            $data = [
                        'id_komponen' => $id_komponen,
                        'id_mesin' => $id_mesin,
                        'up' => $up,
                        'model_montage' => $model_montage,
                        'master' => $master,
                        'ket' => $ket
                    ];

            ModelPraCetak::where(['id_pra_cetak' => $id_pra_cetak ])->update($data);
            $getId = $id_detail_order;
            return redirect("praCetak/detail/$getId");
        }
    }
    
    public function delete($id)
    {
        $hapus = ModelPraCetak::where(['id_pra_cetak' => $id]);
        if(!empty($hapus)) {
            $hapus->delete();
        }
        return redirect()->back();
    }
//
//    public function KonfirmasiKebBahan($id)
//    {
//        $cek = \App\ModelDetailPemesanan::where(['id_detail_order' => $id])->first();
//        if(!empty($cek)){
//            $cek->update(['status'=>'3']);
//            $getId = $cek->id_order;
//        }
//        return redirect("kebBahan/detail/$id");
//        // return route('praCetak');
//    }
    
    public function completed($id)
    {
        $data = ModelPraCetak::where(['id_pra_cetak' => $id])->first();
        if(!empty($data)) {
            $data->status = 1;
            $data->save();
            
            $data->detail_order->cek_tahap();
        }
        return redirect()->back();
    }
}
