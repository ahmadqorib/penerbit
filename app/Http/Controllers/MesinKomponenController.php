<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MesinKomponenController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $q = \App\ModelMesinKomponen::query();
        $nama_komponen = Input::get('nama_komponen');
        $nama_mesin = Input::get('nama_mesin');

        if (!empty($nama_komponen)) {
            $q->whereHas('komponen',function($query) use ($nama_komponen){
                $query->where('nama_komponen', 'like', '%' . $nama_komponen . '%');
            });
        }

        if (!empty($nama_mesin)) {
            $q->whereHas('mesin',function($query) use ($nama_mesin){
                $query->where('nama_mesin', 'like', '%' . $nama_mesin . '%');
            });
        }

        $data = $q->paginate(10);
       
        $list_komponen = \App\ModelKomponen::dropdown();
        $list_mesin = \App\ModelMesin::dropdown();

        return view('page.mesin_komponen.index', compact('data', 'nama_komponen','nama_mesin', 'list_komponen', 'list_mesin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //     
        $valid = Validator::make($request->all(), [
                    'id_komponen' => 'required',
                    'id_mesin' => 'required',
        ]);

        if ($valid->fails()) {
            Session::flash('error', $valid->errors()->first());
            return redirect('mesin_komponen');
        }

        $exist_other = \App\ModelMesinKomponen::where('id_mesin', $request->id_mesin)
                        ->where('id_komponen', $request->id_komponen)->first();

        if (!empty($exist_other)) {
            Session::flash('error', 'ModelMesinKomponen Same Data found. Please, try again.');
            return redirect('mesin_komponen');
        }

        $model = new \App\ModelMesinKomponen();
        $model->id_komponen = $request->id_komponen;
        $model->id_mesin = $request->id_mesin;

        if ($model->save()) {

            Session::flash('success', 'ModelMesinKomponen has been saved.');
            //            return redirect('bus');
        } else {
            Session::flash('error', 'ModelMesinKomponen could not be saved. Please, try again.');
            //            return redirect('categories/create')->withInput($request->except('_token'));
        }



        return redirect('mesin_komponen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    public function delete($id = null) {
        $model = \App\ModelMesinKomponen::where('id_mesin_komponen', $id)->first();
        if (empty($model)) {
            Session::flash('error', 'ModelMesinKomponen not found. Please, try again.');
            return redirect('mesin_komponen');
        }

        //do cek relation

        $model->delete();
        return redirect('mesin_komponen');
    }

}
