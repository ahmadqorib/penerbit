<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelFinishing extends Model
{
    protected $table = 'finishing';
//    public $timestamps = false;
    protected $primaryKey = 'id_finishing';
//    protected $guarded = ['id_finishing'];
    protected $fillable = [        
        'nama_finishing',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelFinishing::get();
        foreach ($model as $item){
            $list[$item->id_finishing] = $item->nama_finishing;
        }
        return $list;
    }
}
