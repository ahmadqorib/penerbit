<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelKomponenJenisPercetakan extends Model
{
    protected $table = 'komponen_jenis_percetakan';
//    public $timestamps = false;
    protected $primaryKey = 'id_komponen_jenis_percetakan';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'id_jenis_percetakan',
        'id_komponen',
        
    ];
    
    public function komponen() 
    {
        return $this->belongsTo('App\ModelKomponen', 'id_komponen', 'id_komponen');
    }
    
    public function jenis_percetakan() 
    {
        return $this->belongsTo('App\ModelJenisPercetakan', 'id_jenis_percetakan', 'id_jenis_percetakan');
    }
    
    public static function dropdown(){
        $list = array();
        $model = ModelKomponenJenisPercetakan::get();
        foreach ($model as $item){
            $list[$item->id_komponen_jenis_percetakan] = $item->jenis_percetakan->nama.' - '.$item->komponen->nama_komponen;
        }
        return $list;
    }
    public static function dropdown_komponen_by_jenis($id){
        $list = array();
        $model = ModelKomponenJenisPercetakan::where('id_jenis_percetakan',$id)->get();
        foreach ($model as $item){
            $list[$item->id_komponen] = $item->komponen->nama_komponen;
        }
        return $list;
    }
    
    public function nama_komponen_jenis_percetakan(){
        return $this->jenis_percetakan->nama.' - '.$this->komponen->nama_komponen;
    }
}
