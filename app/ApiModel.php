<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Mail\UserMail;
use Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\ModelKaryawan;

class ApiModel extends Model {

    use SendsPasswordResetEmails;

    //
    public $HeaderCode;
    public $data;
    public $userModel;
    private $_status = null;
    private $_message = null;
    private $_errorMessage = null;
    private $_data = [];
    private $rememberMe = 2592000;
    // HeaderCode + apiKey + signature + data
    public $function = array(
        '0101' => 'Login', //data: { username,password}
        '0102' => 'CekToken',
        '0103' => 'ListPemesanan',
        '0104' => 'ListKaryawan',
        '0105' => 'CreatePemesanan',
        '0106' => 'DetailPemesanan',
        '0107' => 'ListTahapPracetak',
        '0108' => 'ListTahapKebBahan',
        '0109' => 'ListTahapCetak',
        '0110' => 'ListTahapFinishing',
        '0111' => 'DetailPracetak',
        '0112' => 'CompletedPracetak',
        '0113' => 'DetailKebBahan',
        '0114' => 'CompletedKebBahan',
        '0115' => 'DetailCetak',
        '0116' => 'CompletedCetak',
        '0117' => 'DetailFinishing',
        '0118' => 'CompletedFinishing',
        '0119' => 'ListTahapPending',
        '0120' => 'ListTahapCompleted',

    );
    // HeaderCode+ signature + data
    private $publicFunction = [
        '0101',
        '0102',

    ];
    public $signature;

    public function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->signature = sha1('2018penerbitproject2018');
    }

    /**
     * Pintu masuk API, cukup panggil function ini dari luar.
     * function2 lainnya akan dipanggil dari function ini
     * mengikuti $this->function.
     *
     * pastikan function $this->function ada di class ini (ApiModel)
     *
     * @return array API Result
     *
     * @author a@a
     */
    public function execute() {
        // cek jika $this->function terdefini, dan fungsinya juga ada.
        if ($this->HeaderCode AND isset($this->function[$this->HeaderCode]) AND method_exists(get_class($this), $this->function[$this->HeaderCode])) {
            // panngil API function nya
            if (empty($this->userModel) and ! in_array($this->HeaderCode, $this->publicFunction)) {
                $this->_status = 202;
                $this->_message = 'Invalid User';
                $this->_errorMessage = "user must login.";
                return $this->output();
            } else {
                $method = $this->function[$this->HeaderCode];
                $this->$method();

                if (!is_null($this->_status))
                    return $this->output();
            }
        }
        else {
            $this->_status = 201;
            $this->_message = 'Invalid Request';
            $this->_errorMessage = "Request {$this->HeaderCode} is invalid";
            return $this->output();
        }
    }

    public function output() {
        $output = [
            'status' => $this->_status,
            'message' => $this->_message,
            'errorMessage' => $this->_errorMessage,
            'data' => $this->_data,
        ];

        return $output;
    }

    private function error400() {
        $this->_status = 400;
        $this->_message = 'Bad Request';
        $this->_errorMessage = "Bad Request";
        return $this->output();
    }

    private function errorFormat() {
        $this->_status = 203;
        $this->_message = 'Error Format Request';
        $this->_errorMessage = "Error Format Request";
        return $this->output();
    }

    private function errorContent() {
        $this->_status = 205;
        $this->_message = 'Error Content Not Found';
        $this->_errorMessage = "Error Content Not Found";
        return $this->output();
    }

    private function errorDuplicateContent() {
        $this->_status = 207;
        $this->_message = 'Error Duplicate Content';
        $this->_errorMessage = "Error Duplicate Content";
        return $this->output();
    }

    private function errorSaveContent() {
        $this->_status = 208;
        $this->_message = 'Error Failed Save Content';
        $this->_errorMessage = "Error Failed Save Content";
        return $this->output();
    }

    private function errorMaintenance() {
        $this->_status = 206;
        $this->_message = 'Sorry System on Maintance';
        $this->_errorMessage = "Sorry System on Maintance";
        $this->_data = array();
        return $this->output();
    }

    // '0101' => 'Login',
    public function Login() {
        $data = $this->data;

        if (!isset($data['username']) || !isset($data['password'])) {
            return $this->errorFormat();
        }

        $username = $this->data['username'];
        $password = $this->data['password'];

        $userModel = ModelKaryawan::where([
                    'username' => $username,
                    'password' => md5($password)
                ])->first();

        if (empty($userModel)) {
            $this->_status = 202;
            $this->_message = 'login Failed';
            $this->_errorMessage = "login Failed";
            return $this->output();
        } else {

            if (empty($userModel->apikey)) {
                $auth_key = str_random(50);
                $api_token = sha1($auth_key . time());
                $userModel->apikey = $api_token;
                $userModel->update();
            }

            $dataResult = [
                'apiKey' => $userModel->apikey,
                'username' => $userModel->username,
                'nama_lengkap'=>$userModel->nama_karyawan,
                'alamat'=>$userModel->alamat,
                'no_telp'=>$userModel->no_telp,
                'level' => $userModel->level,
                'level_text'=>$userModel->level_text()
            ];

            $this->_status = 200;
            $this->_message = 'Login Success';
            $this->_data = $dataResult;
            return $this->output();
        }
    }

    public function CekToken() {
        $data = $this->data;
        if (!isset($data['token'])) {
            return $this->errorFormat();
        }

        $userModel = ModelKaryawan::where('apikey', $data['token'])->first();
        if (!empty($userModel)) {
            $dataResult = [
                'apiKey' => $userModel->apikey,
                'username' => $userModel->username,
                'nama_lengkap'=>$userModel->nama_karyawan,
                'alamat'=>$userModel->alamat,
                'no_telp'=>$userModel->no_telp,
                'level' => $userModel->level,
                'level_text'=>$userModel->level_text()
            ];

            $this->_status = 200;
            $this->_message = 'Cek Token Success';
            $this->_data = $dataResult;
            return $this->output();
        } else {
            return $this->errorContent();
        }
    }

    public function ListPemesanan(){
        $userModel = $this->userModel;
        $model = ModelPemesanan::orderBy('status','asc')->get();
        $dataResult =  array();
        $detail = array();
        foreach ($model as $item){
            if(count($item->detail) < 1){
                continue;
            }

            $detail = $item->detail->toArray();
            $dataResult[] = array(
                'id_order'=>$item->id_order,
                'no_soki' => $item->no_soki,
                'no_sp' => $item->no_sp,
                'nama_order' => $item->nama_order,
                'id_penerima' => $item->id_penerima,
                'nama_penerima' => @$item->karyawan->nama_karyawan,
                'nama_pemesan' => $item->nama_pemesan,
                'alamat_pemesan' => $item->alamat_pemesan,
                'no_telp_pemesan' => $item->no_telp_pemesan,
                'ket' => $item->ket,
                'tgl_order'=>$item->tgl_order,
                'status'=>$item->status,
                'detail'=>$detail
            );
        }

        $this->_status = 200;
        $this->_message = 'List Pemesanan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CreatePemesanan(){
        $data = $this->data;
        $userModel = $this->userModel;

        if( !isset($data['nama_order']) || !isset($data['nama_pemesan']) || !isset($data['alamat_pemesan']) || !isset($data['no_telp_pemesan']) ||
                !isset($data['tgl_order']) || !isset($data['detail'])){
            return $this->errorFormat();
        }
        if(empty($data['detail'])){
            return $this->errorFormat();
        }

        foreach ($data['detail'] as $isi){
            if(!isset($isi['produk']) || !isset($isi['oplah']) || !isset($isi['satuan']) || !isset($isi['jenis_order']) ||
                    !isset($isi['panjang']) || !isset($isi['lebar']) || !isset($isi['jml_hlm_isi'])){
                return $this->errorFormat();
                break;
            }
        }
//        $exist_soki = ModelPemesanan::where('no_soki',$data['no_soki'])->first();
//        if($exist_soki){
//            return $this->errorDuplicateContent();
//        }
//        $exist_sp = ModelPemesanan::where('no_sp',$data['no_sp'])->first();
//        if($exist_sp){
//            return $this->errorDuplicateContent();
//        }



        $model = new ModelPemesanan();
        $model->nama_order = $data['nama_order'];
        $model->id_penerima = $userModel->id_karyawan;
        $model->nama_pemesan = $data['nama_pemesan'];
        $model->alamat_pemesan = $data['alamat_pemesan'];
        $model->no_telp_pemesan = $data['no_telp_pemesan'];
        $model->tgl_order = $data['tgl_order'];
        $model->ket = isset($data['ket'])?$data['ket']:null;
        $model->status = 0;

        if($model->save()){
            $model->no_soki = $model->id_order;
            $model->no_sp = $model->id_order;
            $model->update();

            foreach ($data['detail'] as $isi){
                $detail = new ModelDetailPemesanan();
                $detail->id_order = $model->id_order;
                $detail->produk = $isi['produk'];
                $detail->oplah = $isi['oplah'];
                $detail->satuan = $isi['satuan'];
                $detail->jenis_order = $isi['jenis_order'];
                $detail->panjang = $isi['panjang'];
                $detail->lebar = $isi['lebar'];
                $detail->jml_hlm_isi = $isi['jml_hlm_isi'];
                $detail->status = 0;
                $detail->save();
            }

            $dataResult = $model->toArray();
            $dataResult['detail'] = $model->detail->toArray();
            $this->_status = 200;
            $this->_message = 'Create Pemesanan Success';
            $this->_data = $dataResult;
            return $this->output();
        }else{
            return $this->errorSaveContent();
        }

    }

    public function ListKaryawan(){
        $model = ModelKaryawan::get();
        $dataResult = array();
        foreach ($model as $item){
            $dataResult[] = array(
                'id_karyawan'=>$item->id_karyawan,
                'nama_karyawan'=>$item->nama_karyawan
            );
        }

        $this->_status = 200;
        $this->_message = 'List Karyawan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function DetailPemesanan(){
        $data = $this->data;
        if(!isset($data['id_order'])){
            return $this->errorFormat();
        }
        $model = ModelPemesanan::where('id_order',$data['id_order'])->first();
        if(empty($model) || (count($model->detail) < 1)){
            return $this->errorContent();
        }
        $dataResult = $model->detail->toArray();

        $this->_status = 200;
        $this->_message = 'Detail Pemesanan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapPending(){
        $list = ModelDetailPemesanan::where(['finish_pracetak'=>0,'status'=>0])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'List Tahap Pending Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapPracetak(){
        $list = ModelDetailPemesanan::where(['finish_pracetak'=>0,'status'=>1])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'List Tahap Pra Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function DetailPracetak(){
        $data = $this->data;
        if(!isset($data['id_detail_order'])){
            return $this->errorFormat();
        }

        $model = ModelDetailPemesanan::where(['id_detail_order'=>$data['id_detail_order']])->first();

        if(empty($model) || empty($model->pracetak)){
            return $this->errorContent();
        }

        $dataResult =  $model->pracetak->toArray();
        $this->_status = 200;
        $this->_message = 'Detail Pra Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CompletedPracetak(){
        $data = $this->data;
        if(!isset($data['id_pra_cetak'])){
            return $this->errorFormat();
        }

        $model = ModelPraCetak::where(['id_pra_cetak'=>$data['id_pra_cetak']])->first();

        if(empty($model)){
            return $this->errorContent();
        }

        $model->status = 1;
        $model->update();

        $model->detail_order->cek_tahap();

        $dataResult =  $model->toArray();
        $this->_status = 200;
        $this->_message = 'Completed Pra Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapKebBahan(){
        $list = ModelDetailPemesanan::where(['finish_bahan'=>0,'status'=>1])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'Detail List Tahap Kebutuhan Bahan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function DetailKebBahan(){
        $data = $this->data;
        if(!isset($data['id_detail_order'])){
            return $this->errorFormat();
        }

        $model = ModelDetailPemesanan::where(['id_detail_order'=>$data['id_detail_order']])->first();

        if(empty($model) || empty($model->kebbahan)){
            return $this->errorContent();
        }

        $dataResult =  $model->kebbahan->toArray();
        $this->_status = 200;
        $this->_message = 'Detail Kebutuhan Bahan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CompletedKebBahan(){
        $data = $this->data;
        if(!isset($data['id_pra_cetak'])){
            return $this->errorFormat();
        }

        $model = ModelKebBahan::where(['id_keb_bahan'=>$data['id_keb_bahan']])->first();

        if(empty($model)){
            return $this->errorContent();
        }

        $model->status = 1;
        $model->update();

        $model->detail_order->cek_tahap();

        $dataResult =  $model->toArray();
        $this->_status = 200;
        $this->_message = 'Completed Kebutuhan Bahan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapCetak(){
        $list = ModelDetailPemesanan::where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>0,'status'=>1])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'Detail List Tahap Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }


    public function DetailCetak(){
        $data = $this->data;
        if(!isset($data['id_detail_order'])){
            return $this->errorFormat();
        }

        $model = ModelDetailPemesanan::where(['id_detail_order'=>$data['id_detail_order']])->first();

        if(empty($model) || empty($model->cetak)){
            return $this->errorContent();
        }

        $dataResult =  $model->cetak->toArray();
        $this->_status = 200;
        $this->_message = 'Detail Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CompletedCetak(){
        $data = $this->data;
        if(!isset($data['id_cetak'])){
            return $this->errorFormat();
        }

        $model = ModelCetak::where(['id_cetak'=>$data['id_cetak']])->first();

        if(empty($model)){
            return $this->errorContent();
        }

        $model->status = 1;
        $model->update();

        $dataResult =  $model->toArray();
        $this->_status = 200;
        $this->_message = 'Completed Cetak Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapFinishing(){
        $list = ModelDetailPemesanan::where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>1,'finish_finishing'=>0,'status'=>1])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'Detail List Tahap Finishing Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function DetailFinishing(){
        $data = $this->data;
        if(!isset($data['id_detail_order'])){
            return $this->errorFormat();
        }

        $model = ModelDetailPemesanan::where(['id_detail_order'=>$data['id_detail_order']])->first();

        if(empty($model) || empty($model->finish)){
            return $this->errorContent();
        }

        $dataResult =  $model->finish->toArray();
        $this->_status = 200;
        $this->_message = 'Detail Finishing Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CompletedFinishing(){
        $data = $this->data;
        if(!isset($data['id_finish'])){
            return $this->errorFormat();
        }

        $model = ModelFinish::where(['id_finish'=>$data['id_finish']])->first();

        if(empty($model)){
            return $this->errorContent();
        }

        $model->status = 1;
        $model->update();

        $model->detail_order->cek_tahap();

        $dataResult =  $model->toArray();
        $this->_status = 200;
        $this->_message = 'Completed Finishing Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTahapCompleted(){
        $list = ModelDetailPemesanan::where(['finish_bahan'=>1,'finish_pracetak'=>1,'finish_cetak'=>1,'finish_finishing'=>1,'status'=>1])->get();
        $dataResult =  $list->toArray();
        $this->_status = 200;
        $this->_message = 'Detail List Tahap Completed Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListBahan(){
        $model = ModelBahan::get();
        $dataResult = array();
        foreach ($model as $item){
            $dataResult[] = array(
                'id_bahan'=>$item->id_bahan,
                'nama_bahan'=>$item->nama_bahan,
                'panjang'=>$item->panjang,
                'lebar'=>$item->lebar
            );
        }

        $this->_status = 200;
        $this->_message = 'List Bahan Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListMesin(){
        $model = ModelMesin::get();
        $dataResult = array();
        foreach ($model as $item){
            $dataResult[] = array(
                'id_mesin'=>$item->id_mesin,
                'no_mesin'=>$item->no_mesin,
                'nama_mesin'=>$item->nama_mesin,
                'tipe_mesin'=>$item->tipe_mesin
            );
        }

        $this->_status = 200;
        $this->_message = 'List Mesin Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function ListTinta(){
        $model = ModelTinta::get();
        $dataResult = array();
        foreach ($model as $item){
            $dataResult[] = array(
                'id_tinta'=>$item->id_tinta,
                'warna'=>$item->warna
            );
        }

        $this->_status = 200;
        $this->_message = 'List Tinta Success';
        $this->_data = $dataResult;
        return $this->output();
    }

    public function CreateBahan(){
        $nama_bahan = $this->data['nama_bahan'];
        $panjang = $this->data['panjang'];
        $lebar = $this->data['lebar'];

        $bahan = new ModelBahan();
        $bahan->nama_bahan = $nama_bahan;
        $bahan->panjang = $panjang;
        $bahan->lebar = $lebar;

        if($bahan->save()){
            $dataResult = [
                    'nama_bahan' => $bahan->nama_bahan,
                    'panjang' => $bahan->panjang,
                    'lebar' => $bahan->lebar
            ];

            $this->_status = 200;
            $this->_message = 'Save Data Success';
            $this->_data = $dataResult;
            return $this->output();
        }else{
            $this->_status = 110;
            $this->_message = 'Save Data Failed';
            return $this->output();
        }
    }

    public function CreateMesin(){
        $no_mesin = $this->data['no_mesin'];
        $nama_mesin = $this->data['nama_mesin'];
        $tipe_mesin = $this->data['tipe_mesin'];

        $mesin = new ModelMesin();
        $mesin->no_mesin = $no_mesin;
        $mesin->nama_mesin = $nama_mesin;
        $mesin->tipe_mesin = $tipe_mesin;

        if($mesin->save()){
            $dataResult = [
                    'no_mesin' => $mesin->no_mesin,
                    'nama_mesin' => $mesin->nama_mesin,
                    'tipe_mesin' => $mesin->tipe_mesin
            ];

            $this->_status = 200;
            $this->_message = 'Save Data Success';
            $this->_data = $dataResult;
            return $this->output();
        }else{
            $this->_status = 110;
            $this->_message = 'Save Data Failed';
            return $this->output();
        }
    }

    public function CreateTinta(){
        $warna = $this->data['warna'];

        $tinta = new ModelTinta();
        $tinta->warna = $warna;

        if($tinta->save()){
            $dataResult = [
                    'warna' => $tinta->warna
            ];

            $this->_status = 200;
            $this->_message = 'Save Data Success';
            $this->_data = $dataResult;
            return $this->output();
        }else{
            $this->_status = 110;
            $this->_message = 'Save Data Failed';
            return $this->output();
        }
    }


}
