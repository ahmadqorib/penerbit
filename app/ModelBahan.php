<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelBahan extends Model
{
    protected $table = 'bahan';
//    public $timestamps = false;
    protected $primaryKey = 'id_bahan';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama_bahan',
        'panjang',
        'lebar',
        'id_plano',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelBahan::get();
        foreach ($model as $item){
            $list[$item->id_bahan] = $item->nama_bahan." (".$item->panjang .'x'.$item->lebar.')' ;
        }
        return $list;
    }
    public static function dropdown_by_plano($id){
        $list = array();
        $model = ModelBahan::where('id_plano',$id)->get();
        foreach ($model as $item){
            $list[$item->id_bahan] = $item->nama_bahan." (".$item->panjang .'x'.$item->lebar.')' ;
        }
        return $list;
    }
    
    public function plano() 
    {
        return $this->belongsTo('App\ModelPlano', 'id_plano', 'id_plano');
    }
}
