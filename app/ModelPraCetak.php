<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPraCetak extends Model
{
    protected $table = 'pra_cetak';
//    public $timestamps = false;
    protected $primaryKey = 'id_pra_cetak';
    protected $fillable = ['id_detail_order', 'id_komponen', 'id_mesin', 'up', 'model_montage', 'master', 'ket', 'status'];
    
    public function mesin()
    {
        return $this->belongsTo('App\ModelMesin', 'id_mesin', 'id_mesin');
    }
    
    public function komponen()
    {
        return $this->belongsTo('App\ModelKomponen', 'id_komponen', 'id_komponen');
    }
    
    public function detail_order()
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
}
