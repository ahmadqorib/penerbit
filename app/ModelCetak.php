<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelCetak extends Model
{
    protected $table = 'cetak';
//    public $timestamps = false;
    protected $primaryKey = 'id_cetak';
//    protected $guarded = ['id_cetak'];
    
    protected $fillable = [
        'id_pra_cetak',
        'model_cetak', 
        'hal',
        'id_tinta',
        'jml_set',
        'insheet_tiap_set',
        'tot_druck_per_set',
        'id_detail_order',
        'status',
        'id_komponen',
        'id_keb_bahan',
        'ctk',
        'jml_cetak_per_set'
    ];
    
    public function pracetak()
    {
        return $this->belongsTo('App\ModelPraCetak', 'id_pra_cetak', 'id_pra_cetak');
    }
    
    public function kebbahan()
    {
        return $this->belongsTo('App\ModelKebBahan', 'id_keb_bahan', 'id_keb_bahan');
    }
    
    public function detailorder()
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
    
    public function tinta()
    {
        return $this->belongsTo('App\ModelTinta', 'id_tinta', 'id_tinta');
    }
    
    public function detail_order()
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
    
    public function komponen()
    {
        return $this->belongsTo('App\ModelKomponen', 'id_komponen', 'id_komponen');
    }
}
