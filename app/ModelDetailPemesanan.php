<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelDetailPemesanan extends Model
{
    protected $table = 'detail_order';
//    public $timestamps = false;
//    protected $guarded = ['id_detail_order'];  
    protected $primaryKey = 'id_detail_order';
    
    protected $fillable = [        
        'id_order',
        'produk',
        'oplah',
        'satuan',
        'jenis_order',
        'panjang',
        'lebar',
        'jml_hlm_isi',
        'status',
        'finish_pracetak',
        'finish_bahan',
        'finish_cetak',
        'finish_finishing',
        'file',
        
    ];
    public function order() 
    {
        return $this->belongsTo('App\ModelPemesanan', 'id_order', 'id_order');
    }
    
    public function jenis_percetakan() 
    {
        return $this->belongsTo('App\ModelJenisPercetakan', 'id_jenis_percetakan', 'id_jenis_percetakan');
    }
    
    public function mesin() {
        return $this->hasMany('App\ModelMesinAntrian', 'id_detail_order', 'id_detail_order');
    }
    
    public static function status_array(){
        // return array(
        //     0 => 'Pending',
        //     1 => 'Approved',
        //     2 => 'Finish'
        // );
        return array(
            0 => 'No File',
            1 => 'Pending',
            2 => 'Approved',
            3 => 'Finish'
        );
    }
    
    public function status_text(){
        $list = ModelDetailPemesanan::status_array();
        return $list[$this->status];
    }

    public function pracetak() 
    {
        return $this->hasMany('App\ModelPraCetak', 'id_detail_order', 'id_detail_order');
    }
    
    public function kebbahan()
    {
        return $this->hasMany('App\ModelKebBahan', 'id_detail_order', 'id_detail_order');
    }
    
    public function cetak()
    {
        return $this->hasMany('App\ModelCetak', 'id_detail_order', 'id_detail_order');
    }
    
    public function finish()
    {
        return $this->hasMany('App\ModelFinish', 'id_detail_order', 'id_detail_order');
    }
    
    public function cek_rencana(){
        // if(!empty($this->pracetak) && !empty($this->kebbahan) && !empty($this->cetak) && !empty($this->finish)){
            
        //     if($this->status == 0){
        //         $this->status = 1;
        //         $this->save();
        //     }
        // }
        // dd($this->file);
        $count_pracetak = count($this->pracetak);
        $count_kebbahan = count($this->kebbahan);
        $count_cetak = count($this->cetak);
        $count_finish = count($this->finish);

        if($this->file != NULL){
            if($count_pracetak != 0 && $count_kebbahan != 0 && $count_cetak != 0 && $count_finish != 0 ){
                $this->status = 2;
                $this->save();
            }else{
                $this->status = 1;
                $this->save();
            }
        }else{
            $this->status = 0;
            $this->save();
        }
    }
    
    public function cek_tahap(){
        if($this->status == 2){
            $pracetak = 0;
            $kebbahan = 0;
            $cetak = 0;
            $finish = 0;
            $count_pracetak = count($this->pracetak);
            $count_kebbahan = count($this->kebbahan);
            $count_cetak = count($this->cetak);
            $count_finish = count($this->finish);

            foreach ($this->pracetak as $item){
                if($item->status == 1){
                    $pracetak++;
                }
            }   
            foreach ($this->kebbahan as $item){
                if($item->status == 1){
                    $kebbahan++;
                }
            }   
            foreach ($this->cetak as $item){
                if($item->status == 1){
                    $cetak++;
                }
            } 
            foreach ($this->finish as $item){
                if($item->status == 1){
                    $finish++;
                }
            } 
            
            if($count_pracetak > 0 && $count_pracetak == $pracetak){
                $this->finish_pracetak = 1;
                $this->save();
            }
            if($count_kebbahan > 0 && $count_kebbahan == $kebbahan){
                $this->finish_bahan = 1;
                $this->save();
            }
            
            if($this->finish_pracetak == 1 && $this->finish_bahan == 1 && $count_cetak > 0 && $cetak == $count_cetak){
                $this->finish_cetak = 1;
                $this->save();
            }
            
            if($this->finish_pracetak == 1 && $this->finish_bahan == 1 && $this->finish_cetak == 1 && $count_finish > 0 && $finish == $count_finish){
                $this->finish_finishing = 1;
                $this->status = 3;
                $this->save();
            }
        }
    }
    
    
}
