<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelMesinPlano extends Model
{
    protected $table = 'mesin_plano';
//    public $timestamps = false;
    public $primaryKey = 'id_mesin_plano';
    protected $fillable = [
        'id_mesin',
        'id_plano', 
        
    ];
    
    public function plano() 
    {
        return $this->belongsTo('App\ModelPlano', 'id_plano', 'id_plano');
    }
    
    public function mesin() 
    {
        return $this->belongsTo('App\ModelMesin', 'id_mesin', 'id_mesin');
    }
    
}
