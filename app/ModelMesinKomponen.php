<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelMesinKomponen extends Model
{
    protected $table = 'mesin_komponen';
//    public $timestamps = false;
    public $primaryKey = 'id_mesin_komponen';
    protected $fillable = [
        'id_mesin',
        'id_komponen',
    ];
    
    
    public function komponen() 
    {
        return $this->belongsTo('App\ModelKomponen', 'id_komponen', 'id_komponen');
    }
    
    public function mesin() 
    {
        return $this->belongsTo('App\ModelMesin', 'id_mesin', 'id_mesin');
    }
}
