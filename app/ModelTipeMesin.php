<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTipeMesin extends Model
{
    protected $table = 'tipe_mesin';
//    public $timestamps = false;
    protected $primaryKey = 'id_tipe_mesin';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama'
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelTipeMesin::get();
        foreach ($model as $item){
            $list[$item->id_tipe_mesin] = $item->nama ;
        }
        return $list;
    }
}
