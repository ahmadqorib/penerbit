<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelMesinTinta extends Model
{
    protected $table = 'mesin_tinta';
//    public $timestamps = false;
    public $primaryKey = 'id_mesin_tinta';
    protected $fillable = [
        'id_mesin',
        'id_tinta', 
        
    ];
    
    public function tinta() 
    {
        return $this->belongsTo('App\ModelTinta', 'id_tinta', 'id_tinta');
    }
    
    public function mesin() 
    {
        return $this->belongsTo('App\ModelMesin', 'id_mesin', 'id_mesin');
    }
    
}
