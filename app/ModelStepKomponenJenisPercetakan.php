<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelStepKomponenJenisPercetakan extends Model
{
    protected $table = 'step_komponen_jenis_percetakan';
//    public $timestamps = false;
    protected $primaryKey = 'id_step_komponen_jenis_percetakan';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'id_komponen_jenis_percetakan',
        'id_step_percetakan',
        'urutan',
        
    ];
    
    
    public function komponen_jenis_percetakan() 
    {
        return $this->belongsTo('App\ModelKomponenJenisPercetakan', 'id_komponen_jenis_percetakan', 'id_komponen_jenis_percetakan');
    }
    
    public function step_percetakan() 
    {
        return $this->belongsTo('App\ModelStepPercetakan', 'id_step_percetakan', 'id_step_percetakan');
    }
   
}
