<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ModelKaryawan extends Authenticatable
{
    protected $table = 'karyawan';
//    public $timestamps = false;
//    protected $guarded = ['id_karyawan'];
    protected $primaryKey = 'id_karyawan';
    
    protected $fillable = [        
        'nama_karyawan',
        'alamat',
        'no_telp',
        'username',
        'password',
        'apikey',
        'level',
        'remember_token',
        
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function order() {
        return $this->hasMany('App\ModelPemesanan', 'id_karyawan', 'id_penerima');
    }
    
    public static function level_array(){
        return array(
            1=>'Superadmin',
            2=>'PCIC',
            3=>'User Pracetak',
            4=>'User Bahan',
            5=>'User Cetak',
            6=>'User Finishing',
            7=>'User Official',
            8=>'User Sales'
        );
    }
    
    public function level_text(){
        $list = ModelKaryawan::level_array();
        return isset($list[$this->level])?$list[$this->level]:'-';
    }
    
}
