<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelStepPercetakan extends Model
{
    protected $table = 'step_percetakan';
//    public $timestamps = false;
    protected $primaryKey = 'id_step_percetakan';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelStepPercetakan::get();
        foreach ($model as $item){
            $list[$item->id_step_percetakan] = $item->nama;
        }
        return $list;
    }
}
