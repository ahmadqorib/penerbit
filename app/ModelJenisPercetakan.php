<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelJenisPercetakan extends Model
{
    protected $table = 'jenis_percetakan';
//    public $timestamps = false;
    protected $primaryKey = 'id_jenis_percetakan';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelJenisPercetakan::get();
        foreach ($model as $item){
            $list[$item->id_jenis_percetakan] = $item->nama;
        }
        return $list;
    }
    
    public function komponen() {
        return $this->hasMany('App\ModelKomponenJenisPercetakan', 'id_jenis_percetakan', 'id_jenis_percetakan');
    }
}
