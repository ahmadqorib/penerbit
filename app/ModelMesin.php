<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelMesin extends Model
{
    protected $table = 'mesin';
//    public $timestamps = false;
    public $primaryKey = 'id_mesin';
    protected $fillable = [
        'nama_mesin',
        'no_mesin', 
        'id_tipe_mesin',
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelMesin::get();
        foreach ($model as $item){
            $list[$item->id_mesin] = $item->no_mesin.' ('.$item->nama_mesin.')';
        }
        return $list;
    }
    
    public function tipe_mesin() 
    {
        return $this->belongsTo('App\ModelTipeMesin', 'id_tipe_mesin', 'id_tipe_mesin');
    }
    
    public function komponen() {
        return $this->hasMany('App\ModelMesinKomponen', 'id_mesin', 'id_mesin');
    }
    
    public function plano() {
        return $this->hasMany('App\ModelMesinPlano', 'id_mesin', 'id_mesin');
    }
    
    public function tinta() {
        return $this->hasMany('App\ModelMesinTinta', 'id_mesin', 'id_mesin');
    }
    
    public function detail_order() {
        return $this->hasMany('App\ModelMesinAntrian', 'id_mesin', 'id_mesin');
    }
    
    public function my_last_antrian_order(){
        $last = ModelMesinAntrian::where(['id_mesin'=> $this->id_mesin])->where('status','!=',2)->orderBy('finish','desc')->first();
        
        return $last;
    }
    
    public function date_my_last_antrian_order(){
        $last = $this->my_last_antrian_order();
        
        if(!empty($last)){
            return date('Y-m-d', strtotime($last->finish));
        }
        return date('Y-m-d');
    }
    
}
