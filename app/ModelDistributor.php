<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelDistributor extends Model
{
    protected $table = 'distributor';
//    public $timestamps = false;
    protected $primaryKey = 'id_distributor';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama',
        'alamat',
        'no_telp',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelDistributor::get();
        foreach ($model as $item){
            $list[$item->id_distributor] = $item->nama;
        }
        return $list;
    }
    
    public function plano() {
        return $this->hasMany('App\ModelPlanoDistributor', 'id_distributor', 'id_distributor');
    }
}
