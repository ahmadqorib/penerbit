<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPemesanan extends Model
{
    protected $table = 'order';
//    public $timestamps = false;
//    protected $guarded = ['id_order']; 
    protected $primaryKey = 'id_order';
    
    protected $fillable = [        
        'id_penerima',
        'no_soki',
        'no_sp',
        'nama_order',
        'nama_pemesan',
        'alamat_pemesan',
        'no_telp_pemesan',
        'tgl_order',
        'status',
        'ket',
        
    ];
    
    public function karyawan() {
        return $this->belongsTo('App\ModelKaryawan', 'id_penerima', 'id_karyawan');
    }
    
    public function detail() {
        return $this->hasMany('App\ModelDetailPemesanan', 'id_order', 'id_order');
    }

    public function detail_no_file()
    {
        $data = $this->detail()->whereNull('file');
        return $data;

    }

    public function detail_file()
    {
        $data = $this->detail()->whereNotNull('file');
        return $data;
    }
    
    public function status_array(){
        return array(
            0 => 'Pending',
            1 => 'Acc'
        );
    }
    
    public function status_text(){
        $list = ModelPemesanan::status_array();
        return $list[$this->status];
    }

    public function status($kondisi){
        $jumlah = \App\ModelDetailPemesanan::where(['id_order' => $kondisi])->count();
        $finish = \App\ModelDetailPemesanan::where(['id_order' => $kondisi, 'status' => 3])->count();
        
        if($jumlah==0 && $finish==0){
            $label = "label-danger";
        }else{
            if($jumlah == $finish){
                $label = "label-success";
            }else{
                $label = "label-warning";
            }
        }
        
        return "<div class='label $label'>".$finish."/".$jumlah." Complete"."</div>";
    }


}
