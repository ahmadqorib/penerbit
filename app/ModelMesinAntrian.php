<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelMesinAntrian extends Model
{
    protected $table = 'mesin_antrian';
//    public $timestamps = false;
    public $primaryKey = 'id_mesin_antrian';
    protected $fillable = [
        'id_mesin',
        'id_detail_order', 
        'start',
        'finish',
        'status',
        'jadwal_start',
        'jadwal_finish',
    ];
    
    public function detail_order() 
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
    
    public function mesin() 
    {
        return $this->belongsTo('App\ModelMesin', 'id_mesin', 'id_mesin');
    }
    
}
