<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelKomponen extends Model
{
    protected $table = 'komponen';
//    public $timestamps = false;
    protected $primaryKey = 'id_komponen';
//    protected $guarded = ['id_komponen'];
    protected $fillable = [        
        'nama_komponen',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelKomponen::get();
        foreach ($model as $item){
            $list[$item->id_komponen] = $item->nama_komponen;
        }
        return $list;
    }
    
    public function jenis_percetakan() {
        return $this->hasMany('App\ModelKomponenJenisPercetakan', 'id_komponen', 'id_komponen');
    }
    
    public function mesin() {
        return $this->hasMany('App\ModelMesinKomponen', 'id_komponen', 'id_komponen');
    }
}
