<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    //
    protected $table = 'api_log';
    protected $primaryKey = 'id';
//    public $timestamps = false;
    protected $fillable = [        
        'headercode',
        'apikey',
        'data',
        'request',
        'response'
        
    ];
    
}
