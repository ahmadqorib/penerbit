<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelFinish extends Model
{
    protected $table = 'finish';
//    public $timestamps = false;
    protected $primaryKey = 'id_finish';
//    protected $guarded = ['id_finish'];
    protected $fillable = [        
        'id_detail_order',
        'id_finishing',
        'ket',
        'status',
        
    ];
    
    public function finishing()
    {
        return $this->belongsTo('App\ModelFinishing', 'id_finishing', 'id_finishing');
    }
    
    public function detail_order()
    {
        return $this->belongsTo('App\ModelDetailPemesanan', 'id_detail_order', 'id_detail_order');
    }
}
