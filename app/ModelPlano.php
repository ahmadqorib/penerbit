<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPlano extends Model
{
    protected $table = 'plano';
//    public $timestamps = false;
    protected $primaryKey = 'id_plano';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'nama',
        'panjang',
        'lebar',
        'stok',
        
    ];
    
    public static function dropdown(){
        $list = array();
        $model = ModelPlano::get();
        foreach ($model as $item){
            $list[$item->id_plano] = $item->nama." (".$item->panjang .'x'.$item->lebar.')' ;
        }
        return $list;
    }
    
    public function nama_ukuran(){
        return $this->nama." (".$this->panjang .'x'.$this->lebar.')' ;
    }


    public function distributor() {
        return $this->hasMany('App\ModelPlanoDistributor', 'id_plano', 'id_plano');
    }
    
    public function mesin() {
        return $this->hasMany('App\ModelMesinKomponen', 'id_plano', 'id_plano');
    }
    
    public function bahan() {
        return $this->hasMany('App\ModelBahan', 'id_plano', 'id_plano');
    }
    
    public static function Otorisasi(){
        return array(
            1=>'Potrait',
            2=>'Landscape'
        );
    }
}
