<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPlanoStok extends Model
{
    protected $table = 'plano_stok';
//    public $timestamps = false;
    protected $primaryKey = 'id_plano_stok';
//    protected $guarded = ['id_bahan'];
    protected $fillable = [        
        'id_plano',
        'masuk',
        'keluar',
        'stok_awal',
        'stok_akhir',
        
    ];
    
    public function plano() 
    {
        return $this->belongsTo('App\ModelPlano', 'id_plano', 'id_plano');
    }
    
    public function update_stok_plano(){
        $stok_awal = $this->plano->stok;
        $stok_akhir = $stok_awal + $this->masuk - $this->keluar;
        $this->stok_awal = $stok_awal;
        $this->stok_akhir = $stok_akhir;
        $this->update();
        $this->plano->stok = $stok_akhir;
        $this->plano->update();
        
    }
    
    
    public function delete_stok_plano(){
        $stok_deleted = $this->masuk - $this->keluar;
        $this->plano->stok = $this->plano->stok - $stok_deleted;
        $this->plano->update();
    }
    
    
}
