# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: penerbit
# Generation Time: 2018-09-22 05:39:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table api_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api_log`;

CREATE TABLE `api_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request` text,
  `response` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `headercode` varchar(30) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table bahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bahan`;

CREATE TABLE `bahan` (
  `id_bahan` int(10) NOT NULL AUTO_INCREMENT,
  `nama_bahan` varchar(50) NOT NULL,
  `panjang` int(10) NOT NULL,
  `lebar` int(10) NOT NULL,
  PRIMARY KEY (`id_bahan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `bahan` WRITE;
/*!40000 ALTER TABLE `bahan` DISABLE KEYS */;

INSERT INTO `bahan` (`id_bahan`, `nama_bahan`, `panjang`, `lebar`)
VALUES
	(1,'dsfa',12,12);

/*!40000 ALTER TABLE `bahan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cetak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cetak`;

CREATE TABLE `cetak` (
  `id_cetak` int(10) NOT NULL,
  `id_det_order` int(10) NOT NULL,
  `id_komponen` int(10) NOT NULL,
  `id_mesin` int(10) NOT NULL,
  `up` int(10) NOT NULL,
  `model_montage` varchar(50) NOT NULL,
  `master` varchar(10) NOT NULL,
  `warna` varchar(10) NOT NULL,
  `model_cetak` varchar(10) NOT NULL,
  `panjang` int(10) NOT NULL,
  `lebar` int(10) NOT NULL,
  `id_bahan` int(10) NOT NULL,
  `jml_set` int(10) NOT NULL,
  `jml_cetak_per_set` int(10) NOT NULL,
  `insh_per_set` varchar(10) NOT NULL,
  `tot_drunck` int(10) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`id_cetak`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table det_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `det_order`;

CREATE TABLE `det_order` (
  `id_det_order` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `produk` int(50) NOT NULL,
  `oplah` int(10) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `jenis_order` varchar(50) NOT NULL,
  `panjang` int(10) NOT NULL,
  `lebar` int(10) NOT NULL,
  `jml_hlm_isi` int(10) NOT NULL,
  PRIMARY KEY (`id_det_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table finishing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finishing`;

CREATE TABLE `finishing` (
  `id_finishing` int(10) NOT NULL,
  `finishing` varchar(200) NOT NULL,
  PRIMARY KEY (`id_finishing`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table finishing_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finishing_order`;

CREATE TABLE `finishing_order` (
  `id_finishing_order` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `id_finishing` int(10) NOT NULL,
  PRIMARY KEY (`id_finishing_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table karyawan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table ket_packing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ket_packing`;

CREATE TABLE `ket_packing` (
  `id_ket_pack` int(10) NOT NULL,
  `id_pengiriman` int(10) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`id_ket_pack`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table komponen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `id_komponen` int(10) NOT NULL,
  `nama_komponen` varchar(50) NOT NULL,
  `id_det_order` int(10) NOT NULL,
  `id_bahan` int(10) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`id_komponen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table mesin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin`;

CREATE TABLE `mesin` (
  `id_mesin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mesin` varchar(50) NOT NULL,
  `no_mesin` int(50) NOT NULL,
  `tipe_mesin` varchar(50) NOT NULL,
  PRIMARY KEY (`id_mesin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mesin` WRITE;
/*!40000 ALTER TABLE `mesin` DISABLE KEYS */;

INSERT INTO `mesin` (`id_mesin`, `nama_mesin`, `no_mesin`, `tipe_mesin`)
VALUES
	(1,'aaaaaaaa',3233434,'aa');

/*!40000 ALTER TABLE `mesin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_04_03_090609_create_model_mesins_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table model_mesins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model_mesins`;

CREATE TABLE `model_mesins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id_order` int(10) NOT NULL AUTO_INCREMENT,
  `nama_pesanan` varchar(50) NOT NULL,
  `no_soki` int(10) NOT NULL,
  `no_sp` int(10) NOT NULL,
  `id_penerima` int(10) NOT NULL,
  `nama_pemesan` varchar(50) NOT NULL,
  `tgl_buat` date NOT NULL,
  `alamat_pemesan` varchar(200) NOT NULL,
  `no_telp_pemesan` varchar(20) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;

INSERT INTO `order` (`id_order`, `nama_pesanan`, `no_soki`, `no_sp`, `id_penerima`, `nama_pemesan`, `tgl_buat`, `alamat_pemesan`, `no_telp_pemesan`, `ket`)
VALUES
	(1,'dsfa',12,12,12,'sfdfsd','2018-04-03','sdfsa','12321','sdfas'),
	(2,'dsfa',12,12,12,'sfdfsd','2018-04-03','sdfsa','12321','sdfas'),
	(3,'adf',123,123,1,'ahmad','2018-12-12','fds','092321','fds'),
	(4,'adf',123,123,1,'ahmad','2018-12-12','adfs','092321','dasd'),
	(5,'adf',123,123,1,'ahmad','2018-12-12','adfs','092321','dasd'),
	(6,'adf',123,123,1,'ahmad','2018-12-12','fdsa','092321','fd'),
	(7,'adf',123,123,1,'ahmad','2018-12-12','fdsa','092321','fd'),
	(8,'adf',123,123,1,'ahmad','2018-12-12','fdsa','092321','fd');

/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pengiriman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pengiriman`;

CREATE TABLE `pengiriman` (
  `id_pengiriman` int(10) NOT NULL,
  `id_order` int(10) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `tujuan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tinta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tinta`;

CREATE TABLE `tinta` (
  `id_tinta` int(3) NOT NULL AUTO_INCREMENT,
  `warna` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tinta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tinta` WRITE;
/*!40000 ALTER TABLE `tinta` DISABLE KEYS */;

INSERT INTO `tinta` (`id_tinta`, `warna`)
VALUES
	(1,'CMY');

/*!40000 ALTER TABLE `tinta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id_user`, `username`, `password`)
VALUES
	(1,'a','a');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'ahmad qorib','ahmadqorib@yahoo.com','$2y$10$oCkZGFuc6.XiZ2htv33WmeCV9ZLTmaTs9VgaAqhY8Q5QjcsCLIJNW','wj0I849GF1VOSAdZ4BYqFrZ000ZhHdvNXSVw6NJXX6SZYXCXzjTXo5wqBlMm','2018-04-10 04:07:56','2018-04-10 04:07:58'),
	(3,'ahmad qorib','ahmadqorib.aq@gmail.com','$2y$10$/Hzc6Perw6rYZi3/UQGoaOLKCuP4IPEMKS9ZhLLyfkBKd.x2j8huW','elkatLOOXVUNVdNRPdxwbLWYqlwUu6kDtqJWlt7Al2AZUOwn0SSjTD8d4cWS','2018-04-10 04:09:38','2018-04-10 04:09:41');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
