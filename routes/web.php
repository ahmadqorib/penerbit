<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();
Route::any('/logout', 'Auth\LoginController@logout');

//Route::get('/', 'Home@index');
//Route::get('/adminpanel', 'Home@adminpanel');
//Route::post('/login', 'Home@login');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
//Route::get('/logout', 'Home@logout')->name('logout');

Route::get('/mesin', 'MesinController@index')->name('mesin');
Route::post('/mesin/create', 'MesinController@create');
Route::post('/mesin/update', 'MesinController@update');
Route::get('/mesin/delete/{id}', 'MesinController@delete');

Route::get('/bahan', 'BahanController@index')->name('bahan');
Route::post('/bahan/create', 'BahanController@create');
Route::post('/bahan/update', 'BahanController@update');
Route::get('/bahan/delete/{id}', 'BahanController@delete');

Route::get('/tinta', 'TintaController@index')->name('tinta');
Route::post('/tinta/create', 'TintaController@create');
Route::post('/tinta/update', 'TintaController@update');
Route::get('/tinta/delete/{id}', 'TintaController@delete');

Route::get('/karyawan', 'KaryawanController@index')->name('karyawan');
Route::post('/karyawan/create', 'KaryawanController@create');
Route::post('/karyawan/update', 'KaryawanController@update');
Route::get('/karyawan/delete/{id}', 'KaryawanController@delete');

Route::get('/komponen', 'KomponenController@index')->name('komponen');
Route::post('/komponen/create', 'KomponenController@create');
Route::post('/komponen/update', 'KomponenController@update');
Route::get('/komponen/delete/{id}', 'KomponenController@delete');

Route::get('/finishing', 'FinishingController@index')->name('finishing');
Route::post('/finishing/create', 'FinishingController@create');
Route::post('/finishing/update', 'FinishingController@update');
Route::get('/finishing/delete/{id}', 'FinishingController@delete');

Route::get('/pemesanan', 'PemesananController@index')->name('pemesanan');
Route::get('/showDataPemesanan', 'PemesananController@showData');
Route::get('/pemesanan/add', 'PemesananController@showform')->name('pemesanan/add');
Route::get('/pemesanan/edit/{id}', 'PemesananController@showEdit')->name('pemesanan/edit');
Route::post('/pemesanan/create', 'PemesananController@create');
Route::get('/pemesanan/delete/{id}', 'PemesananController@delete');
Route::post('/pemesanan/update', 'PemesananController@update');
Route::get('/pemesanan/detail/{id}', 'PemesananController@detailPemesanan');

Route::post('/pemesanan/save_detail', 'PemesananController@save_detail');
Route::post('/pemesanan/save_pracetak', 'PemesananController@save_pracetak');
Route::post('/pemesanan/save_kebbahan', 'PemesananController@save_kebbahan');
Route::post('/pemesanan/save_cetak', 'PemesananController@save_cetak');
Route::post('/pemesanan/save_finish', 'PemesananController@save_finish');
Route::post('/pemesanan/upload_file', 'PemesananController@upload');

Route::get('/pemesananNoFile', 'PemesananNoFileController@showNoFile')->name('pemesananNoFile');
Route::get('/showPemesananNoFile', 'PemesananNoFileController@showData_nofile');
Route::get('/pemesanan/detail_nofile/{id}', 'PemesananNoFileController@detailPemesananNoFile');

Route::get('/data_pemesanan', 'SalesController@index')->name('data_pemesanan');
Route::get('/showPemesananSales', 'SalesController@pemesanan');
Route::get('/data_pemesanan/edit/{id}', 'SalesController@showEdit')->name('data_pemesanan/edit');
Route::post('/data_pemesanan/update', 'SalesController@update');
Route::get('/data_pemesanan/detail/{id}', 'SalesController@detailPemesananSales');
Route::get('/data_pemesanan/delete/{id}', 'SalesController@delete');

Route::post('/detailPemesanan/create', 'DetailPemesananController@create');
Route::get('/detailPemesanan/showEdit/{id}', 'DetailPemesananController@showDataEdit');
Route::post('/detailPemesanan/update', 'DetailPemesananController@update');
Route::get('/detailPemesanan/delete/{id}', 'DetailPemesananController@delete');
Route::get('/detailPemesanan/confirmPra/{id}', 'DetailPemesananController@KonfirmasiPra');
Route::get('/pemesanan/print/{id}', 'DetailPemesananController@do_print');

Route::get('/praCetak', 'PraCetakController@index')->name('praCetak');
Route::get('/praCetak/detail/{id}', 'PraCetakController@detailPraCetak');
Route::get('/showDataDet', 'PraCetakController@showData');
Route::post('/praCetak/create', 'PraCetakController@create');
Route::get('/praCetak/showEdit/{id}', 'PraCetakController@showDataEdit');
Route::post('/praCetak/update', 'PraCetakController@update');
Route::get('/praCetak/delete/{id}', 'PraCetakController@delete');
Route::get('/praCetak/confirmKebBahan/{id}', 'PraCetakController@KonfirmasiKebBahan');
Route::get('/praCetak/completed/{id}', 'PraCetakController@completed');

Route::get('/kebBahan', 'KebBahanController@index')->name('kebBahan');
Route::get('/kebBahan/detail/{id}', 'KebBahanController@detailKebBahan');
Route::get('/kebBahan/completed/{id}', 'KebBahanController@completed');
//Route::get('/kebBahan/detail/{id}', 'KebBahanController@kebBahan');
Route::get('/showDataBahan', 'KebBahanController@showData');


Route::get('/pemesanan/add_new', 'PemesananController@add');
Route::get('/pemesanan/show_detail/{id}', 'PemesananController@show_detail');
Route::get('/pemesanan/show_detail_new/{id}', 'PemesananController@show_detail_new');
Route::post('/pemesanan/save_pemesanan', 'PemesananController@save_pemesanan');
Route::post('/pemesanan/save_detail', 'PemesananController@save_detail');
Route::post('/pemesanan/select_bahan', 'PemesananController@select_bahan');
Route::post('/pemesanan/select_mesin', 'PemesananController@select_mesin');
Route::post('/pemesanan/save_pracetak_bahan', 'PemesananController@save_pracetak_bahan');
Route::post('/pemesanan/save_cetak_new', 'PemesananController@save_cetak_new');
Route::post('/pemesanan/save_finishing_new', 'PemesananController@save_finishing_new');

Route::post('/praCetak/create', 'PraCetakController@create');
Route::get('/praCetak/showEdit/{id}', 'PraCetakController@showDataEdit');
Route::post('/praCetak/update', 'PraCetakController@update');
Route::get('/praCetak/delete/{id}', 'PraCetakController@delete');
Route::get('/cetak', 'CetakController@index')->name('cetak');
Route::get('/showDataCetak', 'CetakController@showData');
Route::get('/cetak/detail/{id}', 'CetakController@detailCetak');
Route::get('/Cetak/showEdit/{id}', 'CetakController@showDataEdit');
Route::post('/cetak/update', 'CetakController@update');
Route::get('/cetak/completed/{id}', 'CetakController@completed');

Route::get('/finish', 'FinishController@index')->name('finish');
Route::get('/finish/detail/{id}', 'FinishController@detailFinish');
Route::get('/finish/completed/{id}', 'FinishController@completed');
Route::get('/showDataFinish', 'FinishController@showData');

Route::get('/plano', 'PlanoController@index')->name('plano');
Route::post('/plano/store', 'PlanoController@store');
Route::post('/plano/update', 'PlanoController@update');
Route::get('/plano/delete/{id}', 'PlanoController@delete');

Route::get('/jenis_percetakan', 'JenisPercetakanController@index')->name('jenis_percetakan');
Route::post('/jenis_percetakan/store', 'JenisPercetakanController@store');
Route::post('/jenis_percetakan/update', 'JenisPercetakanController@update');
Route::get('/jenis_percetakan/delete/{id}', 'JenisPercetakanController@delete');

Route::get('/tipe_mesin', 'TipeMesinController@index')->name('tipe_mesin');
Route::post('/tipe_mesin/store', 'TipeMesinController@store');
Route::post('/tipe_mesin/update', 'TipeMesinController@update');
Route::get('/tipe_mesin/delete/{id}', 'TipeMesinController@delete');

Route::get('/distributor', 'DistributorController@index')->name('distributor');
Route::post('/distributor/store', 'DistributorController@store');
Route::post('/distributor/update', 'DistributorController@update');
Route::get('/distributor/delete/{id}', 'DistributorController@delete');

Route::get('/plano_distributor', 'PlanoDistributorController@index')->name('plano_distributor');
Route::post('/plano_distributor/store', 'PlanoDistributorController@store');
Route::get('/plano_distributor/delete/{id}', 'PlanoDistributorController@delete');

Route::get('/komponen_jenis_percetakan', 'KomponenJenisPercetakanController@index')->name('komponen_jenis_percetakan');
Route::post('/komponen_jenis_percetakan/store', 'KomponenJenisPercetakanController@store');
Route::get('/komponen_jenis_percetakan/delete/{id}', 'KomponenJenisPercetakanController@delete');

Route::get('/mesin_plano', 'MesinPlanoController@index')->name('mesin_plano');
Route::post('/mesin_plano/store', 'MesinPlanoController@store');
Route::get('/mesin_plano/delete/{id}', 'MesinPlanoController@delete');

Route::get('/mesin_komponen', 'MesinKomponenController@index')->name('mesin_komponen');
Route::post('/mesin_komponen/store', 'MesinKomponenController@store');
Route::get('/mesin_komponen/delete/{id}', 'MesinKomponenController@delete');

Route::get('/mesin_tinta', 'MesinTintaController@index')->name('mesin_tinta');
Route::post('/mesin_tinta/store', 'MesinTintaController@store');
Route::get('/mesin_tinta/delete/{id}', 'MesinTintaController@delete');

Route::get('/step_percetakan', 'StepPercetakanController@index')->name('step_percetakan');
Route::post('/step_percetakan/store', 'StepPercetakanController@store');
Route::post('/step_percetakan/update', 'StepPercetakanController@update');
Route::get('/step_percetakan/delete/{id}', 'StepPercetakanController@delete');

Route::get('/step_komponen_jenis_percetakan', 'StepKomponenJenisPercetakanController@index')->name('step_komponen_jenis_percetakan');
Route::post('/step_komponen_jenis_percetakan/store', 'StepKomponenJenisPercetakanController@store');
Route::post('/step_komponen_jenis_percetakan/update', 'StepKomponenJenisPercetakanController@update');
Route::get('/step_komponen_jenis_percetakan/delete/{id}', 'StepKomponenJenisPercetakanController@delete');


Route::get('/plano_stok', 'PlanoStokController@index')->name('plano_stok');
Route::post('/plano_stok/store', 'PlanoStokController@store');
Route::get('/plano_stok/delete/{id}', 'PlanoStokController@delete');

//Auth::routes();
