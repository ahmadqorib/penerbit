# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: percetakan
# Generation Time: 2018-10-04 04:54:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table api_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `api_log`;

CREATE TABLE `api_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request` text,
  `response` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `headercode` varchar(30) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table bahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bahan`;

CREATE TABLE `bahan` (
  `id_bahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bahan` varchar(45) DEFAULT NULL,
  `panjang` float DEFAULT NULL,
  `lebar` float DEFAULT NULL,
  `id_plano` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_bahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bahan` WRITE;
/*!40000 ALTER TABLE `bahan` DISABLE KEYS */;

INSERT INTO `bahan` (`id_bahan`, `nama_bahan`, `panjang`, `lebar`, `id_plano`, `created_at`, `updated_at`)
VALUES
	(5,'HVS1',30,20,1,NULL,'2018-09-29 04:35:02'),
	(6,'Bp 57 gr',109,79,1,NULL,NULL),
	(7,'HVS1 80gr',100,70,1,'2018-09-29 04:35:36','2018-09-29 04:35:36');

/*!40000 ALTER TABLE `bahan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cetak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cetak`;

CREATE TABLE `cetak` (
  `id_cetak` int(11) NOT NULL AUTO_INCREMENT,
  `id_pra_cetak` int(11) DEFAULT NULL,
  `model_cetak` varchar(45) DEFAULT NULL,
  `hal` varchar(45) DEFAULT NULL,
  `id_tinta` int(11) DEFAULT NULL,
  `jml_set` int(11) DEFAULT NULL,
  `insheet_tiap_set` varchar(45) DEFAULT NULL,
  `tot_druck_per_set` int(11) DEFAULT NULL,
  `id_detail_order` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  `id_keb_bahan` int(11) DEFAULT NULL,
  `ctk` varchar(255) DEFAULT NULL,
  `jml_cetak_per_set` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_cetak`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cetak` WRITE;
/*!40000 ALTER TABLE `cetak` DISABLE KEYS */;

INSERT INTO `cetak` (`id_cetak`, `id_pra_cetak`, `model_cetak`, `hal`, `id_tinta`, `jml_set`, `insheet_tiap_set`, `tot_druck_per_set`, `id_detail_order`, `status`, `created_at`, `updated_at`, `id_komponen`, `id_keb_bahan`, `ctk`, `jml_cetak_per_set`)
VALUES
	(1,1,NULL,'-',3,1,'3.28',10328,1,0,'2018-10-04 03:54:57','2018-10-04 04:30:48',1,1,'4/0',10000),
	(3,3,NULL,'-',3,18,'4.73',10473,1,0,'2018-10-04 03:59:30','2018-10-04 04:34:26',2,3,'4/4',10000),
	(4,4,NULL,'BK',3,1,'4.73',10473,1,0,'2018-10-04 04:03:15','2018-10-04 04:48:41',2,4,'4/4',10000);

/*!40000 ALTER TABLE `cetak` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detail_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detail_order`;

CREATE TABLE `detail_order` (
  `id_detail_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `id_jenis_percetakan` int(11) DEFAULT NULL,
  `produk` varchar(200) DEFAULT NULL,
  `oplah` int(11) DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `jenis_order` varchar(45) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `jml_hlm_isi` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `finish_pracetak` int(1) DEFAULT '0',
  `finish_bahan` int(1) DEFAULT '0',
  `finish_cetak` int(1) DEFAULT '0',
  `finish_finishing` int(1) DEFAULT '0',
  `file` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_detail_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `detail_order` WRITE;
/*!40000 ALTER TABLE `detail_order` DISABLE KEYS */;

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_jenis_percetakan`, `produk`, `oplah`, `satuan`, `jenis_order`, `panjang`, `lebar`, `jml_hlm_isi`, `status`, `finish_pracetak`, `finish_bahan`, `finish_cetak`, `finish_finishing`, `file`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'Cerita Lucu',10000,'eksemplar','buku',17,24,304,1,0,0,0,0,'17. SNI 7657-2010 Singkatan nama kota.pdf',NULL,'2018-10-03 02:22:49');

/*!40000 ALTER TABLE `detail_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table distributor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `distributor`;

CREATE TABLE `distributor` (
  `id_distributor` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_distributor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `distributor` WRITE;
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;

INSERT INTO `distributor` (`id_distributor`, `nama`, `alamat`, `no_telp`, `created_at`, `updated_at`)
VALUES
	(1,'PT Kertas Indah','Jawa Timur','0898379333','2018-09-29 03:43:18','2018-09-29 03:43:18'),
	(2,'PT Sinar Indah','Jawa Tengah','7986869793','2018-09-29 03:43:37','2018-09-29 03:43:37');

/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table finish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finish`;

CREATE TABLE `finish` (
  `id_finish` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) DEFAULT NULL,
  `id_finishing` int(11) DEFAULT NULL,
  `ket` text,
  `status` int(2) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_finish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `finish` WRITE;
/*!40000 ALTER TABLE `finish` DISABLE KEYS */;

INSERT INTO `finish` (`id_finish`, `id_detail_order`, `id_finishing`, `ket`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'okeke',0,'2018-10-03 05:14:16','2018-10-03 05:14:16'),
	(2,1,3,'oke',0,'2018-10-03 05:14:49','2018-10-03 05:14:49');

/*!40000 ALTER TABLE `finish` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table finishing
# ------------------------------------------------------------

DROP TABLE IF EXISTS `finishing`;

CREATE TABLE `finishing` (
  `id_finishing` int(11) NOT NULL AUTO_INCREMENT,
  `nama_finishing` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_finishing`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `finishing` WRITE;
/*!40000 ALTER TABLE `finishing` DISABLE KEYS */;

INSERT INTO `finishing` (`id_finishing`, `nama_finishing`, `created_at`, `updated_at`)
VALUES
	(1,'Lipat',NULL,NULL),
	(3,'Packing',NULL,NULL);

/*!40000 ALTER TABLE `finishing` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis_percetakan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_percetakan`;

CREATE TABLE `jenis_percetakan` (
  `id_jenis_percetakan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jenis_percetakan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `jenis_percetakan` WRITE;
/*!40000 ALTER TABLE `jenis_percetakan` DISABLE KEYS */;

INSERT INTO `jenis_percetakan` (`id_jenis_percetakan`, `nama`, `created_at`, `updated_at`)
VALUES
	(1,'Buku','2018-09-29 03:26:42','2018-09-29 03:26:42'),
	(2,'Kalender','2018-09-29 03:26:48','2018-09-29 03:26:48'),
	(3,'Kartu Nama','2018-09-29 03:26:55','2018-09-29 03:30:38'),
	(4,'Undangan','2018-09-29 03:27:01','2018-09-29 03:27:01'),
	(5,'Poster','2018-09-29 03:27:07','2018-09-29 03:27:07');

/*!40000 ALTER TABLE `jenis_percetakan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table karyawan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_karyawan` varchar(200) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(12) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `apikey` text,
  `level` int(2) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `alamat`, `no_telp`, `username`, `password`, `apikey`, `level`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'superadmin','Daerah Istimewa Yogyakarta','081234567443','superadmin','25d55ad283aa400af464c76d713c07ad','5243af8256922b7edc0bd5ee0809cbfd7106da1e',1,'jWnTyPL1v6k450EzXnn7YOuzlSorbVWyT3kJcl6zMvYJqIwsShfIWqvnc3K7',NULL,NULL),
	(3,'pcic','Daerah Istimewa Yogyakarta','082111122332','pcic','25d55ad283aa400af464c76d713c07ad',NULL,2,'zZOABuwn4Tw2ecBT16PdbJ1pR2bFEWVIqtgu6wnJVM0y7ftRuvfIb2zaC8le',NULL,NULL),
	(10,'pracetak','Daerah Istimewa Yogyakarta','082111122332','pracetak','25d55ad283aa400af464c76d713c07ad',NULL,3,'BiuG4lycNAh9Sm7q3bTibco1mVfl6nx8DydX8L778cfkj0CNKhSTCnL8DViZ',NULL,NULL),
	(11,'kebbahan','Daerah Istimewa Yogyakarta','083782818721','kebbahan','25d55ad283aa400af464c76d713c07ad',NULL,4,'Lv8hOk12DQ8LOn7xr0kQ6d2HjGKX4DY1Z85fs02lMmrNYjbERGsybv7hL0IG',NULL,NULL),
	(12,'cetak','Daerah Istimewa Yogyakarta','081234567443','cetak','25d55ad283aa400af464c76d713c07ad',NULL,5,'LzoUdrPIsGNz2zTxh9Bz2LYX5OBYxBKtTksucXARtHdjE0xkNgQYGSXz1NkZ',NULL,NULL),
	(13,'finishing','Daerah Istimewa Yogyakarta','082111122332','finishing','25d55ad283aa400af464c76d713c07ad',NULL,6,'lS35m201icDS8YBrdIkRNZu7LGVMOCF6Yv217PB2EwqvWv7RazxXB35jRpzI',NULL,NULL),
	(14,'sales','Daerah Istimewa Yogyakarta','082111122332','sales','25d55ad283aa400af464c76d713c07ad',NULL,8,'x5fxhookqAEFladUVzheliLh15bO8IKBzOtLBulEmyyUIQ8TcJkrHpjOKsqc',NULL,NULL);

/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table keb_bahan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `keb_bahan`;

CREATE TABLE `keb_bahan` (
  `id_keb_bahan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pra_cetak` int(11) DEFAULT NULL,
  `id_bahan` int(11) DEFAULT NULL,
  `panjang` float DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `pl_jadi` int(11) DEFAULT NULL,
  `jml_lbr_p` float DEFAULT NULL,
  `ket` varchar(45) DEFAULT NULL,
  `id_detail_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_keb_bahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `keb_bahan` WRITE;
/*!40000 ALTER TABLE `keb_bahan` DISABLE KEYS */;

INSERT INTO `keb_bahan` (`id_keb_bahan`, `id_pra_cetak`, `id_bahan`, `panjang`, `lebar`, `pl_jadi`, `jml_lbr_p`, `ket`, `id_detail_order`, `status`, `created_at`, `updated_at`, `id_komponen`)
VALUES
	(1,1,5,26,26,8,1291,'OKE',1,0,'2018-10-04 03:54:57','2018-10-04 03:54:57',1),
	(3,3,7,72,72,1,99500,'oke',1,0,'2018-10-04 03:59:30','2018-10-04 03:59:30',2),
	(4,4,5,72,72,1,99500,'oke',1,0,'2018-10-04 04:03:15','2018-10-04 04:03:15',2);

/*!40000 ALTER TABLE `keb_bahan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table komponen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `komponen`;

CREATE TABLE `komponen` (
  `id_komponen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_komponen` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_komponen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `komponen` WRITE;
/*!40000 ALTER TABLE `komponen` DISABLE KEYS */;

INSERT INTO `komponen` (`id_komponen`, `nama_komponen`, `created_at`, `updated_at`)
VALUES
	(1,'Cover',NULL,NULL),
	(2,'Isi',NULL,NULL),
	(3,'Kertas Pembatas',NULL,NULL);

/*!40000 ALTER TABLE `komponen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table komponen_jenis_percetakan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `komponen_jenis_percetakan`;

CREATE TABLE `komponen_jenis_percetakan` (
  `id_komponen_jenis_percetakan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_jenis_percetakan` int(11) DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_komponen_jenis_percetakan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `komponen_jenis_percetakan` WRITE;
/*!40000 ALTER TABLE `komponen_jenis_percetakan` DISABLE KEYS */;

INSERT INTO `komponen_jenis_percetakan` (`id_komponen_jenis_percetakan`, `id_jenis_percetakan`, `id_komponen`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2018-09-29 04:43:37','2018-09-29 04:43:37'),
	(2,1,2,'2018-09-29 04:44:21','2018-09-29 04:44:21'),
	(3,2,1,'2018-09-29 04:44:34','2018-09-29 04:44:34'),
	(4,3,1,'2018-09-29 04:44:41','2018-09-29 04:44:41');

/*!40000 ALTER TABLE `komponen_jenis_percetakan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mesin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin`;

CREATE TABLE `mesin` (
  `id_mesin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mesin` varchar(45) DEFAULT NULL,
  `no_mesin` varchar(45) DEFAULT NULL,
  `id_tipe_mesin` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mesin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mesin` WRITE;
/*!40000 ALTER TABLE `mesin` DISABLE KEYS */;

INSERT INTO `mesin` (`id_mesin`, `nama_mesin`, `no_mesin`, `id_tipe_mesin`, `created_at`, `updated_at`)
VALUES
	(4,'SSMesin 1','M00001',2,NULL,'2018-09-29 04:28:57'),
	(5,'SM3942','M00002',1,'2018-09-29 04:27:52','2018-09-29 04:28:19');

/*!40000 ALTER TABLE `mesin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mesin_antrian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin_antrian`;

CREATE TABLE `mesin_antrian` (
  `id_mesin_atrian` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_mesin` int(11) DEFAULT NULL,
  `id_detail_order` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `finish` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `jadwal_start` date DEFAULT NULL,
  `jadwal_finish` date DEFAULT NULL,
  PRIMARY KEY (`id_mesin_atrian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table mesin_komponen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin_komponen`;

CREATE TABLE `mesin_komponen` (
  `id_mesin_komponen` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_mesin` int(11) DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mesin_komponen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mesin_komponen` WRITE;
/*!40000 ALTER TABLE `mesin_komponen` DISABLE KEYS */;

INSERT INTO `mesin_komponen` (`id_mesin_komponen`, `id_mesin`, `id_komponen`, `created_at`, `updated_at`)
VALUES
	(1,4,2,'2018-10-01 02:15:12','2018-10-01 02:15:12'),
	(2,5,3,'2018-10-01 02:15:20','2018-10-01 02:15:20'),
	(3,4,1,'2018-10-02 08:56:45','2018-10-02 08:56:45');

/*!40000 ALTER TABLE `mesin_komponen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mesin_plano
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin_plano`;

CREATE TABLE `mesin_plano` (
  `id_mesin_plano` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_mesin` int(11) DEFAULT NULL,
  `id_plano` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mesin_plano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mesin_plano` WRITE;
/*!40000 ALTER TABLE `mesin_plano` DISABLE KEYS */;

INSERT INTO `mesin_plano` (`id_mesin_plano`, `id_mesin`, `id_plano`, `created_at`, `updated_at`)
VALUES
	(1,4,1,'2018-10-01 02:08:10','2018-10-01 02:08:10'),
	(2,5,2,'2018-10-01 02:08:44','2018-10-01 02:08:44');

/*!40000 ALTER TABLE `mesin_plano` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mesin_tinta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mesin_tinta`;

CREATE TABLE `mesin_tinta` (
  `id_mesin_tinta` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_mesin` int(11) DEFAULT NULL,
  `id_tinta` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_mesin_tinta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mesin_tinta` WRITE;
/*!40000 ALTER TABLE `mesin_tinta` DISABLE KEYS */;

INSERT INTO `mesin_tinta` (`id_mesin_tinta`, `id_mesin`, `id_tinta`, `created_at`, `updated_at`)
VALUES
	(1,4,3,'2018-10-01 02:16:05','2018-10-01 02:16:05'),
	(2,5,4,'2018-10-01 02:16:13','2018-10-01 02:16:13');

/*!40000 ALTER TABLE `mesin_tinta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `no_soki` varchar(45) DEFAULT NULL,
  `no_sp` varchar(45) DEFAULT NULL,
  `nama_order` varchar(200) DEFAULT NULL,
  `id_penerima` int(11) DEFAULT NULL,
  `nama_pemesan` varchar(200) DEFAULT NULL,
  `alamat_pemesan` text,
  `no_telp_pemesan` varchar(13) DEFAULT NULL,
  `tgl_order` date DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `ket` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `no_soki_UNIQUE` (`no_soki`),
  UNIQUE KEY `no_sp_UNIQUE` (`no_sp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;

INSERT INTO `order` (`id_order`, `no_soki`, `no_sp`, `nama_order`, `id_penerima`, `nama_pemesan`, `alamat_pemesan`, `no_telp_pemesan`, `tgl_order`, `status`, `ket`, `created_at`, `updated_at`)
VALUES
	(1,'1','1','Buku Cerita',1,'Risman','Jogja','085725972201','2018-09-22',0,'oke',NULL,NULL);

/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plano
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plano`;

CREATE TABLE `plano` (
  `id_plano` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_plano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plano` WRITE;
/*!40000 ALTER TABLE `plano` DISABLE KEYS */;

INSERT INTO `plano` (`id_plano`, `nama`, `panjang`, `lebar`, `stok`, `created_at`, `updated_at`)
VALUES
	(1,'1 Plano',79,109,0,'2018-09-29 02:50:47','2018-10-01 03:27:53'),
	(2,'1/2 Plano',40,60,0,'2018-09-29 02:54:03','2018-09-29 02:54:03');

/*!40000 ALTER TABLE `plano` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plano_distributor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plano_distributor`;

CREATE TABLE `plano_distributor` (
  `id_plano_distributor` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_plano` int(11) DEFAULT NULL,
  `id_distributor` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_plano_distributor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plano_distributor` WRITE;
/*!40000 ALTER TABLE `plano_distributor` DISABLE KEYS */;

INSERT INTO `plano_distributor` (`id_plano_distributor`, `id_plano`, `id_distributor`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2018-09-29 04:08:44','2018-09-29 04:08:44'),
	(2,1,2,'2018-09-29 04:09:25','2018-09-29 04:09:25');

/*!40000 ALTER TABLE `plano_distributor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plano_stok
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plano_stok`;

CREATE TABLE `plano_stok` (
  `id_plano_stok` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_plano` int(11) DEFAULT NULL,
  `masuk` int(11) DEFAULT '0',
  `keluar` int(11) DEFAULT '0',
  `stok_awal` int(11) DEFAULT '0',
  `stok_akhir` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_plano_stok`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pra_cetak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pra_cetak`;

CREATE TABLE `pra_cetak` (
  `id_pra_cetak` int(11) NOT NULL AUTO_INCREMENT,
  `id_detail_order` int(11) DEFAULT NULL,
  `id_komponen` int(11) DEFAULT NULL,
  `id_mesin` int(11) DEFAULT NULL,
  `up` int(11) DEFAULT NULL,
  `model_montage` varchar(45) DEFAULT NULL,
  `master` varchar(45) DEFAULT NULL,
  `ket` text,
  `status` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pra_cetak`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pra_cetak` WRITE;
/*!40000 ALTER TABLE `pra_cetak` DISABLE KEYS */;

INSERT INTO `pra_cetak` (`id_pra_cetak`, `id_detail_order`, `id_komponen`, `id_mesin`, `up`, `model_montage`, `master`, `ket`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,1,4,1,'-','CTP','OKE',0,'2018-10-04 03:54:57','2018-10-04 03:54:57'),
	(3,1,2,4,16,'-','CTP','oke',0,'2018-10-04 03:59:30','2018-10-04 03:59:30'),
	(4,1,2,4,16,'-','CTP','oke',0,'2018-10-04 04:03:15','2018-10-04 04:03:15');

/*!40000 ALTER TABLE `pra_cetak` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table step_komponen_jenis_percetakan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `step_komponen_jenis_percetakan`;

CREATE TABLE `step_komponen_jenis_percetakan` (
  `id_step_komponen_jenis_percetakan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_komponen_jenis_percetakan` int(11) DEFAULT NULL,
  `id_step_percetakan` int(11) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_step_komponen_jenis_percetakan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `step_komponen_jenis_percetakan` WRITE;
/*!40000 ALTER TABLE `step_komponen_jenis_percetakan` DISABLE KEYS */;

INSERT INTO `step_komponen_jenis_percetakan` (`id_step_komponen_jenis_percetakan`, `id_komponen_jenis_percetakan`, `id_step_percetakan`, `urutan`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'2018-10-01 02:40:05','2018-10-01 02:40:05'),
	(2,2,1,1,'2018-10-01 02:47:14','2018-10-01 02:47:14'),
	(3,3,1,1,'2018-10-01 02:51:04','2018-10-01 02:51:04'),
	(4,2,2,2,'2018-10-01 02:51:17','2018-10-01 02:51:17'),
	(5,2,3,3,'2018-10-01 02:51:27','2018-10-01 02:51:27'),
	(6,1,2,2,'2018-10-01 02:54:44','2018-10-01 02:54:44');

/*!40000 ALTER TABLE `step_komponen_jenis_percetakan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table step_percetakan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `step_percetakan`;

CREATE TABLE `step_percetakan` (
  `id_step_percetakan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_step_percetakan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `step_percetakan` WRITE;
/*!40000 ALTER TABLE `step_percetakan` DISABLE KEYS */;

INSERT INTO `step_percetakan` (`id_step_percetakan`, `nama`, `created_at`, `updated_at`)
VALUES
	(1,'Cetak','2018-10-01 02:37:37','2018-10-01 02:37:37'),
	(2,'Potong','2018-10-01 02:37:46','2018-10-01 02:37:46'),
	(3,'Jilid','2018-10-01 02:37:53','2018-10-01 02:37:53');

/*!40000 ALTER TABLE `step_percetakan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tinta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tinta`;

CREATE TABLE `tinta` (
  `id_tinta` int(11) NOT NULL AUTO_INCREMENT,
  `warna` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tinta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tinta` WRITE;
/*!40000 ALTER TABLE `tinta` DISABLE KEYS */;

INSERT INTO `tinta` (`id_tinta`, `warna`, `created_at`, `updated_at`)
VALUES
	(3,'CMYK',NULL,NULL),
	(4,'RGB',NULL,NULL);

/*!40000 ALTER TABLE `tinta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tipe_mesin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipe_mesin`;

CREATE TABLE `tipe_mesin` (
  `id_tipe_mesin` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipe_mesin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tipe_mesin` WRITE;
/*!40000 ALTER TABLE `tipe_mesin` DISABLE KEYS */;

INSERT INTO `tipe_mesin` (`id_tipe_mesin`, `nama`, `created_at`, `updated_at`)
VALUES
	(1,'Mesin Cetak','2018-09-29 03:35:51','2018-09-29 03:35:51'),
	(2,'Mesin Potong','2018-09-29 03:35:59','2018-09-29 03:35:59'),
	(3,'Mesin Lipat','2018-09-29 03:36:06','2018-09-29 03:36:06');

/*!40000 ALTER TABLE `tipe_mesin` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
