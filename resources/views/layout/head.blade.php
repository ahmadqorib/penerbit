<title>@yield('title')</title>
<link href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
<link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/coba/jquery.dataTables.min.js') }}"></script>
<style type="text/css">
    #form .form-group label {
        font-weight: lighter;
        color: #808080;
        font-size: 14px;
    }

</style>
