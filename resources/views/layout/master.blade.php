<!DOCTYPE html>
<html>
<head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
	@include('layout.head')
</head>
<body>
	<div>
		<div id="sidebar">
			<div class="profile">
					<img src="{{ asset('images/user.ico') }}" width="200px">
			</div>
<!--
			<div class="profile">
				<h5>ahmad qorib</h5>
			</div>
-->
			@include('layout.menu')
		</div>

		<div id="konten">
			<div class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button class="btn btn-info navbar-btn" id="btnCollapse">
							<span class="glyphicon glyphicon-menu-hamburger"></span>
						</button>
					</div>
					<ul class="nav navbar-nav navbar-right menu">
						<li>
							<a data-href="{{ route('logout') }}" class="btn" data-toggle="modal" data-target="#modalKeluar">
								<span class="glyphicon glyphicon-log-out text-danger"></span>
								<span class="text-danger">Keluar</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="isi">
                            @if(Session::has('error'))
                                    <p class="alert alert-danger">{!! Session::get('error') !!}</p>
                                @endif
				@yield('content')
			</div>
		</div>
	</div>
    @include('layout.footer')
    
    
    <script>
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    </script>        
    @yield('script')
</body>
</html>
