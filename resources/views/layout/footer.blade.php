    
	<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>


    <!-- Konfirmasi hapus -->
	<div class="modal fade" id="modalHapus">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin akan menghapus data ini ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-danger btn-ok btn-sm">Hapus</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

    <!-- Konfirmasi keluar -->
	<div class="modal fade" id="modalKeluar">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin akan keluar  ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-danger btn-ok btn-sm">Keluar</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

   <!-- Konfirmasi keluar -->
	<div class="modal fade" id="modalKonfirmasi">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin akan meneruskan  ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-danger btn-ok btn-sm">OK</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

   <!-- Konfirmasi Completed -->
	<div class="modal fade" id="modalCompleted">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="text-danger">PERHATIAN!!!</h4>
				</div>
				<div class="modal-body">
					Apakah anda yakin data ini sudah Completed ?
				</div>
				<div class="modal-footer">
					<a class="btn btn-success btn-ok btn-sm">Completed</a>
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

    <script type="text/javascript">
	    $(document).ready(function() {
	        $('#modalHapus').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	        });
            $('#modalKonfirmasi').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	        });
	        $('#modalKeluar').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	        });
            $('#modalCompleted').on('show.bs.modal', function(e) {
	            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	        });
	    });
    </script>

	<script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").niceScroll({
                cursorcolor: '#53619d',
                cursorwidth: 4,
                cursorborder: 'none'
            });

            $('#btnCollapse').on('click', function () {
                $('#sidebar, #konten').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>

<!--
    <script type="text/javascript">
        $(function() {
            var oTable = $('#table-pemesanan').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("showDataPemesanan") }}'
                },
                columns: [
                {data: 'nama_pesanan', name: 'nama_pesanan'},
                {data: 'no_soki', name: 'no_soki'},
                {data: 'no_sp', name: 'no_sp'},
                {data: 'id_penerima', name: 'id_penerima', orderable: false},
                {data: 'nama_pemesan', name: 'nama_pemesan', orderable: false, searchable: false},
            ],
            });
        });
    </script>
-->
