<?php 
	$user = auth::user();
?>
<ul class="list-unstyled">
	<li><a href="{{ route('dashboard') }}">
			<span class="glyphicon glyphicon-th-large"></span>
			<span>Dashboard</span>
		</a>
	</li>
	@if(($user->level == 1) or ($user->level == 2))
	<li>
		<a href="#pageData" data-toggle="collapse" aria-expanded="false">
			<span class="glyphicon glyphicon-th-list"></span>
			<span>Data</span>
		</a>
        <ul class="collapse list-unstyled" id="pageData">
            <li><a href="{{ route('jenis_percetakan') }}">Jenis Percetakan</a></li>
            <li><a href="{{ route('plano') }}">Plano</a></li>
            <li><a href="{{ route('plano_stok') }}">Plano Stok</a></li>
            <li><a href="{{ route('tipe_mesin') }}">Tipe Mesin</a></li>
            <li><a href="{{ route('distributor') }}">Distributor</a></li>
            <li><a href="{{ route('plano_distributor') }}">Distributor Plano</a></li>
            <li><a href="{{ route('komponen') }}">Komponen</a></li>
            <li><a href="{{ route('komponen_jenis_percetakan') }}">Komponen Jenis Percetakan</a></li>
            <li><a href="{{ route('step_percetakan') }}">Step Percetakan</a></li>
            <li><a href="{{ route('step_komponen_jenis_percetakan') }}">Step Komponen Jenis Percetakan</a></li>
            <li><a href="{{ route('bahan') }}">Bahan</a></li>
            <li><a href="{{ route('tinta') }}">Tinta</a></li>
            <li><a href="{{ route('mesin') }}">Mesin</a></li>
            <li><a href="{{ route('mesin_plano') }}">Mesin Plano</a></li>
            <li><a href="{{ route('mesin_komponen') }}">Mesin Komponen</a></li>
            <li><a href="{{ route('mesin_tinta') }}">Mesin Tinta</a></li>
            <li><a href="{{ route('finishing') }}">Finishing</a></li>
            @if(($user->level == 1) or ($user->level == 2))
            <li><a href="{{ route('karyawan') }}">Karyawan</a></li>
            
            @endif
        </ul>
	</li>
	<li>
		<a href="{{ route('pemesananNoFile') }}">
			<span class="glyphicon glyphicon-tag"></span>
			<span>Pemesanan Baru</span>
		</a>
	</li>
	@endif
	@if(($user->level == 1) or ($user->level == 2))
	<li>
		<a href="{{ route('pemesanan') }}">
			<span class="glyphicon glyphicon-tag"></span>
			<span>Pemesanan</span>
		</a>
	</li>
	@endif
	@if($user->level == 8)
	<li>
		<a href="{{ route('data_pemesanan') }}">
			<span class="glyphicon glyphicon-tag"></span>
			<span>Pemesanan</span>
		</a>
	</li>
	@endif
	@if(($user->level == 3) or ($user->level == 1) or ($user->level == 2))
	<li>
		<a href="{{ route('praCetak') }}">
			<span class="glyphicon glyphicon-print"></span>
			<span>Pra Cetak</span>
		</a>
	</li>
	@endif

	@if(($user->level == 4) or ($user->level == 1) or ($user->level == 2))
	<li>
		<a href="{{ route('kebBahan') }}">
			<span class="glyphicon glyphicon-file"></span>
			<span>Kebutuhan Bahan</span>
		</a>
	</li>
	@endif

	@if(($user->level == 5) or ($user->level == 1) or ($user->level == 2))
	<li>
		<a href="{{ route('cetak') }}">
			<span class="glyphicon glyphicon-print"></span>
			<span>Cetak</span>
		</a>
	</li>
	@endif
	
	@if(($user->level == 6) or ($user->level == 1) or ($user->level == 2))
	<li>
		<a href="{{ route('finish') }}">
			<span class="glyphicon glyphicon-gift"></span>
			<span>Finish</span>
		</a>
	</li>
	@endif
</ul>
