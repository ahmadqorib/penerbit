<!-- modal tambah data mesin -->
	<div id="modalEditMesin" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Mesin</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/mesin/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                        <input type="hidden" name="id_mesin" id="id_mesin">
						<div class="form-group">
							<label class="control-label col-md-3">No Mesin* :</label>
							<div class="col-md-8">
								<input type="text" name="no_mesin" class="form-control input-sm" id="no_mesin">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama Mesin* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_mesin" class="form-control input-sm" id="nama_mesin">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Type Mesin* :</label>
							<div class="col-md-8">
                                                            {{ Form::select('id_tipe_mesin', [''=>'Select Tipe Mesin'] + \App\ModelTipeMesin::dropdown(), null,['class'=>'form-control input-sm','id'=>'tipe_mesin','required'=>true])}}
							
							</div>
						</div>
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Edit">
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
   
    <script>
        $('#modalEditMesin').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var idmesin = button.data('id') 
            var nomesin = button.data('nomesin') 
            var namamesin = button.data('namamesin') 
            var tipemesin = button.data('tipemesin') 
            var modal = $(this)
            modal.find('.modal-body #id_mesin').val(idmesin)
            modal.find('.modal-body #no_mesin').val(nomesin)
            modal.find('.modal-body #nama_mesin').val(namamesin)
            modal.find('.modal-body #tipe_mesin').val(tipemesin)
        })
    </script>