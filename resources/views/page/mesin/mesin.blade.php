@extends('layout/master') 

@section('title', $title) 

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Mesin
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddMesin">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">No</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Mesin</td>
                        <td>Type Mesin</td>
                        <td width="13%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->no_mesin }}</td>
                        <td>{{ $key->nama_mesin }}</td>
                        <td>{{ $key->tipe_mesin->nama }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditMesin" data-id="{{ $key->id_mesin }}" data-nomesin="{{ $key->no_mesin }}" data-namamesin="{{ $key->nama_mesin }}" data-tipemesin="{{ $key->id_tipe_mesin }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/mesin/delete/'.$key->id_mesin) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.mesin.tambahMesin') 
    @include('page.mesin.editMesin') 
@endsection
