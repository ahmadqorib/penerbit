<!-- modal tambah data mesin -->
	<div id="modalAddMesin" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Mesin</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/mesin/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<div class="form-group">
							<label class="control-label col-md-3">No Mesin* :</label>
							<div class="col-md-8">
								<input type="text" name="no_mesin" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama Mesin* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_mesin" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Type Mesin* :</label>
							<div class="col-md-8">
								{{ Form::select('id_tipe_mesin', [''=>'Select Tipe Mesin'] + \App\ModelTipeMesin::dropdown(), null,['class'=>'form-control input-sm','required'=>true])}}
							</div>
						</div>
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>