@extends('layout/master') 
@section('title', $title) 
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detail Pra Cetak
            <div style="float: right">
                <a href="{{ route('praCetak') }}" class="btn btn-xs btn-warning"> kembali</a>
<!--                <a data-href="{{ URL('/praCetak/confirmKebBahan/'.$det->id_detail_order) }}" class="btn btn-xs btn-success"
                    data-toggle="modal" data-target="#modalKonfirmasi">
                    <i class="glyphicon glyphicon-file"></i> kon. keb. bahan
                </a>-->
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Produk</td>
                            <td>:</td>
                            <td>{{ $det->produk }}</td>
                        </tr>
                        <tr>
                            <td>Oplah</td>
                            <td>:</td>
                            <td>{{ $det->oplah }}</td>
                        </tr>
                        <tr>
                            <td>Satuan</td>
                            <td>:</td>
                            <td>{{ $det->satuan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Pesanan</td>
                            <td>:</td>
                            <td>{{ $det->jenis_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Panjang</td>
                            <td width="5%">:</td>
                            <td>{{ $det->panjang }}</td>
                        </tr>
                        <tr>
                            <td>Lebar</td>
                            <td>:</td>
                            <td>{{ $det->lebar }}</td>
                        </tr>
                        <tr>
                            <td>Jum. Hal. Isi</td>
                            <td>:</td>
                            <td>{{ $det->jml_hlm_isi }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $det->status_text() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                       &nbsp;
<!--                        <div style="float: right">
                            <a href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddPra" data-id="{{ $det->id_detail_order }}">
                                <span class="glyphicon glyphicon-plus"></span> tambah
                            </a>
                        </div>-->
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%">No</td>
                                    <td>Komponen</td>
                                    <td>Mesin</td>
                                    <td>Up</td>
                                    <td>Model Montage</td>
                                    <td>Master</td>
                                    <td>Keterangan</td>
                                    <td width="13%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pra as $value => $itemp)
                                <tr>
                                    <td width="3%">{{ $value + 1 }}</td>
                                    <td>{{ $itemp->komponen['nama_komponen'] }}</td>
                                    <td>{{ $itemp->mesin['nama_mesin'] }}</td>
                                    <td>{{ $itemp->up }}</td>
                                    <td>{{ $itemp->model_montage }}</td>
                                    <td>{{ $itemp->master }}</td>
                                    <td>{{ $itemp->ket }}</td>
                                    <td width="13%">
                                        <?php if($itemp->status == 0):?>
                                        <a data-href="{{ URL('/praCetak/completed/'.$itemp->id_pra_cetak) }}" class="btn btn-success btn-xs" 
                                            data-toggle="modal" data-target="#modalCompleted"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-ok-circle"></span> completed
                                        </a>
                                        <?php endif;?>
<!--                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="showEdit({{ $itemp->id_pra_cetak }})"  style="margin-top: 3px">
					 		                <span class="glyphicon glyphicon-edit"></span> edit
                                        </a>
                                        <a data-href="{{ URL('/praCetak/delete/'.$itemp->id_pra_cetak) }}" class="btn btn-danger btn-xs" 
                                            data-toggle="modal" data-target="#modalHapus"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-trash"></span> hapus
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('page.praCetak.tambahPraCetak')
    @include('page.praCetak.editPraCetak')
@endsection
