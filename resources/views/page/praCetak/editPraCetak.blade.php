<!-- modal edit detail pemesanan -->
	<div id="modalEditPra" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Pra Cetak</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/praCetak/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                    <div class="form-group">
                        <input type="hidden" name="id_pra_cetak" id="id_pra_cetak">
                        <input type="hidden" name="id_detail_order" id="id_detail_order">
                        <label class="control-label col-md-3">Komponen* :</label>
                        <div class="col-md-8">
                            <select name="komponen" class="form-control input-sm" id="komponen">
                                <option id="vOption"></option>
                                <option value="">Pilih Komponen</option>
                                @foreach($komponen as $komp)
                                <option value="{{ $komp->id_komponen }}">{{ $komp->nama_komponen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Mesin* :</label>
                        <div class="col-md-8">
                            <select name="mesin" class="form-control input-sm">
                                <option id="vOptionM"></option>
                                <option value="">Pilih Mesin</option>
                                @foreach($mesin as $msn)
                                <option value="{{ $msn->id_mesin }}">{{ $msn->nama_mesin }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Up :</label>
                        <div class="col-md-8">
                            <input type="text" name="up" class="form-control input-sm" id="up">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Model Montage :</label>
                        <div class="col-md-8">
                            <input type="text" name="model_montage" class="form-control input-sm" id="model_montage">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Master :</label>
                        <div class="col-md-8">
                            <input type="text" name="master" class="form-control input-sm" id="master">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Keterangan :</label>
                        <div class="col-md-8">
                            <textarea class="form-control input-sm" name="ket" id="ket"></textarea>
                        </div>
                    </div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Edit">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<script type="text/javascript">
        function showEdit(id)
        {
            var options="";
            $.ajax({
                url : "{{ url('/praCetak/showEdit/') }}/"+id,
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    $('#id_detail_order').val(data.id_detail_order);
                    $('#id_pra_cetak').val(data.id_pra_cetak);
                    $("#vOption").attr("value", data.id_komponen);
                    $("#vOption").html(data.nama_komponen);
                    $("#vOptionM").attr("value", data.id_mesin);
                    $("#vOptionM").html(data.nama_mesin);
                    $('#up').val(data.up);
                    $('#model_montage').val(data.model_montage);
                    $('#master').val(data.master);
                    $('#ket').val(data.ket);
                    $('#modalEditPra').modal('show'); // show bootstrap modal when complete loaded
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
        }
    </script>