<!-- modal tambah detail pemesanan -->
	<div id="modalAddPra" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Pra Cetak</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/praCetak/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<input type="hidden" name="id_detail_order" id="iddetail">
						<div class="form-group">
							<label class="control-label col-md-3">Komponen* :</label>
							<div class="col-md-8">
								<select name="komponen" class="form-control input-sm">
								    <option value="">Pilih Komponen</option>
								    @foreach($komponen as $komp)
								    <option value="{{ $komp->id_komponen }}">{{ $komp->nama_komponen }}</option>
								    @endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Mesin* :</label>
							<div class="col-md-8">
								<select name="mesin" class="form-control input-sm">
								    <option value="">Pilih Mesin</option>
								    @foreach($mesin as $msn)
								    <option value="{{ $msn->id_mesin }}">{{ $msn->nama_mesin }}</option>
								    @endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Up :</label>
							<div class="col-md-8">
								<input type="text" name="up" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Model Montage :</label>
							<div class="col-md-8">
								<input type="text" name="model_montage" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Master :</label>
							<div class="col-md-8">
								<input type="text" name="master" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Keterangan :</label>
							<div class="col-md-8">
								<textarea class="form-control input-sm" name="ket"></textarea>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Simpan">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<script>
        $('#modalAddPra').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var iddetail = button.data('id') 
            var modal = $(this)
            modal.find('.modal-body #iddetail').val(iddetail)
        })
    </script>