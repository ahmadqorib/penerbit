@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Step Percetakan
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddStepPercetakan">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama" placeholder="Cari Nama Step Percetakan" value="<?= $nama ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Step Percetakan</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditStepPercetakan" data-id="{{ $item->id_step_percetakan }}" data-nama="{{ $item->nama }}" data-panjang="{{ $item->panjang }}" data-lebar="{{ $item->lebar }}">
                                    <span class="glyphicon glyphicon-edit"></span> edit</button>
                                <a data-href="{{ URL('/step_percetakan/delete/'.$item->id_step_percetakan) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama' => $nama,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data mesin -->
<div id="modalAddStepPercetakan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data StepPercetakan</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/step_percetakan/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Step Percetakan :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<!-- modal tambah data bahan -->
<div id="modalEditStepPercetakan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Data Step Percetakan</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/step_percetakan/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                <input type="hidden" name="id_step_percetakan" id="id_step_percetakan">
                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Step Percetakan :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm" id="nama">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Edit">
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>




@endsection

@section('script')
<script>
    $('#modalEditStepPercetakan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id_step_percetakan = button.data('id');
        var nama = button.data('nama');
        var modal = $(this);
        modal.find('.modal-body #id_step_percetakan').val(id_step_percetakan);
        modal.find('.modal-body #nama').val(nama);
    })
</script>

@endsection    
