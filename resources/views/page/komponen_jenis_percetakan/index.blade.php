@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Komponen Jenis Percetakan
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddKomponenJenisPercetakan">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-1 pull-right hidden">
                        <button  class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_jenis_percetakan" placeholder="Cari Nama Jenis Percetakan" value="<?= $nama_jenis_percetakan ?>">

                    </div>
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_komponen" placeholder="Cari Nama Komponen" value="<?= $nama_komponen ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Komponen</td>
                            <td>Nama Jenis Percetakan</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ @$item->komponen->nama_komponen }}</td>
                            <td>{{ @$item->jenis_percetakan->nama }}</td>
                            <td>
                                
                                <a data-href="{{ URL('/komponen_jenis_percetakan/delete/'.$item->id_komponen_jenis_percetakan) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama_komponen' => $nama_komponen,
                        'nama_jenis_percetakan' => $nama_jenis_percetakan,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data mesin -->
<div id="modalAddKomponenJenisPercetakan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data JenisPercetakan Komponen</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/komponen_jenis_percetakan/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Komponen :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_komponen', [''=>'Select Nama Komponen'] + $list_komponen, null,['class'=>'form-control input-sm','required'=>true])}}
                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Jenis Percetakan :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_jenis_percetakan', [''=>'Select Nama Jenis Percetakan'] + $list_jenis_percetakan, null,['class'=>'form-control input-sm','required'=>true])}}
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>






@endsection

@section('script')
@endsection    
