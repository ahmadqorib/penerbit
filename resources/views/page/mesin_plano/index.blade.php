@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Mesin Plano
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddMesinPlano">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-1 pull-right hidden">
                        <button  class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_mesin" placeholder="Cari Nama Mesin" value="<?= $nama_mesin ?>">

                    </div>
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_plano" placeholder="Cari Nama Plano" value="<?= $nama_plano ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Plano</td>
                            <td>Nama Mesin</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ @$item->plano->nama }}</td>
                            <td>{{ @$item->mesin->nama_mesin }}</td>
                            <td>
                                
                                <a data-href="{{ URL('/mesin_plano/delete/'.$item->id_mesin_plano) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama_plano' => $nama_plano,
                        'nama_mesin' => $nama_mesin,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data mesin -->
<div id="modalAddMesinPlano" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Mesin Plano</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/mesin_plano/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Plano :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_plano', [''=>'Select Nama Plano'] + $list_plano, null,['class'=>'form-control input-sm','required'=>true])}}
                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Mesin :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_mesin', [''=>'Select Nama Mesin'] + $list_mesin, null,['class'=>'form-control input-sm','required'=>true])}}
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>






@endsection

@section('script')
@endsection    
