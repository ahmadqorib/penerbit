<!-- modal tambah data bahan -->
	<div id="modalEditBahan" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Bahan</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/bahan/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                        <input type="hidden" name="id_bahan" id="id_bahan">
						<div class="form-group">
							<label class="control-label col-md-3">Nama Bahan* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_bahan" class="form-control input-sm" id="nama_bahan">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama Plano* :</label>
							<div class="col-md-8">
								{{ Form::select('id_plano', [''=>'Select Nama Plano'] + \App\ModelPlano::dropdown(), null,['class'=>'form-control input-sm','id'=>'id_plano','required'=>true])}}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Panjang* :</label>
							<div class="col-md-8">
								<input type="text" name="panjang" class="form-control input-sm" id="panjang">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Lebar* :</label>
							<div class="col-md-8">
								<input type="text" name="lebar" class="form-control input-sm" id="lebar">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Edit">
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
   
    <script>
        $('#modalEditBahan').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var idbahan = button.data('id')
            var id_plano = button.data('id_plano')
            var namabahan = button.data('namabahan')
            var panjang = button.data('panjang')
            var lebar = button.data('lebar')
            var modal = $(this)
            modal.find('.modal-body #id_bahan').val(idbahan)
            modal.find('.modal-body #id_plano').val(id_plano)
            modal.find('.modal-body #nama_bahan').val(namabahan)
            modal.find('.modal-body #panjang').val(panjang)
            modal.find('.modal-body #lebar').val(lebar)
        })
    </script>
