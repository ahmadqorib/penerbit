@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Bahan
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddBahan">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">NO</td>
                        <td>Nama Bahan</td>
                        <td>Nama Plano</td>
                        <td>Panjang</td>
                        <td>Lebar</td>
                        <td width="13%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->nama_bahan }}</td>
                        <td>{{ $key->plano->nama }}</td>
                        <td>{{ $key->panjang }}</td>
                        <td>{{ $key->lebar }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditBahan" data-id="{{ $key->id_bahan }}" data-id_plano="{{ $key->id_plano }}" data-namabahan="{{ $key->nama_bahan }}" data-panjang="{{ $key->panjang }}" data-lebar="{{ $key->lebar }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/bahan/delete/'.$key->id_bahan) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.bahan.tambahBahan')
    @include('page.bahan.editBahan')
@endsection
