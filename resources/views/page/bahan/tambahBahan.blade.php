<!-- modal tambah data bahan -->
	<div id="modalAddBahan" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Bahan</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/bahan/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<!-- <div class="form-group">
							<label class="control-label col-md-3">ID Bahan*</label>
							<div class="col-md-8">
								<input type="text" name="id_bahan" class="form-control">
							</div>
						</div> -->
						<div class="form-group">
							<label class="control-label col-md-3">Nama Bahan* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_bahan" class="form-control input-sm">
							</div>
						</div>
                                                <div class="form-group">
							<label class="control-label col-md-3">Nama Plano* :</label>
							<div class="col-md-8">
								{{ Form::select('id_plano', [''=>'Select Nama Plano'] + \App\ModelPlano::dropdown(), null,['class'=>'form-control input-sm','required'=>true])}}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Panjang* :</label>
							<div class="col-md-8">
								<input type="text" name="panjang" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Lebar* :</label>
							<div class="col-md-8">
								<input type="text" name="lebar" class="form-control input-sm">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
