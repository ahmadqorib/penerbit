@extends('layout/master')

@section('title', $title)

@section('content')
	<h2>{{ $content }}</h2>
@endsection