@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Karyawan
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddKaryawan">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">NO</td>
                        <td>Nama Karyawan</td>
                        <td>Username</td>
                        <td>Alamat</td>
                        <td>No Telepon</td>
                        <td>Level</td>
                        <td width="13%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->nama_karyawan }}</td>
                        <td>{{ $key->username }}</td>
                        <td>{{ $key->alamat }}</td>
                        <td>{{ $key->no_telp }}</td>
                        <td>{{ $key->level_text() }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditKaryawan" data-id="{{ $key->id_karyawan }}" data-namakaryawan="{{ $key->nama_karyawan }}" data-alamat="{{ $key->alamat }}" data-notelp="{{ $key->no_telp }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/karyawan/delete/'.$key->id_karyawan) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.karyawan.tambahKaryawan')
    @include('page.karyawan.editKaryawan')
@endsection
