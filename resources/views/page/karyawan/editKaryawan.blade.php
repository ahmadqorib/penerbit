<!-- modal tambah data bahan -->
	<div id="modalEditKaryawan" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Karyawan</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/karyawan/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                        <input type="hidden" name="id_karyawan" id="id_karyawan">
						<div class="form-group">
							<label class="control-label col-md-3">Nama Karyawan* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_karyawan" class="form-control input-sm" id="nama_karyawan">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Alamat* :</label>
							<div class="col-md-8">
								<input type="text" name="alamat" class="form-control input-sm" id="alamat">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">No Telepon* :</label>
							<div class="col-md-8">
								<input type="text" name="no_telp" class="form-control input-sm" id="no_telp">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary" value="Edit">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>

    <script>
        $('#modalEditKaryawan').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var idkaryawan = button.data('id')
            var namakaryawan = button.data('namakaryawan')
            var alamat = button.data('alamat')
            var notelp = button.data('notelp')
            var modal = $(this)
            modal.find('.modal-body #id_karyawan').val(idkaryawan)
            modal.find('.modal-body #nama_karyawan').val(namakaryawan)
            modal.find('.modal-body #alamat').val(alamat)
            modal.find('.modal-body #no_telp').val(notelp)
        })
    </script>
