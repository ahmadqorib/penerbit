<!-- modal tambah data bahan -->
	<div id="modalAddKaryawan" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Karyawan</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/karyawan/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<!-- <div class="form-group">
							<label class="control-label col-md-3">ID Bahan*</label>
							<div class="col-md-8">
								<input type="text" name="id_bahan" class="form-control">
							</div>
						</div> -->
						<div class="form-group">
							<label class="control-label col-md-3">Nama Karyawan* :</label>
							<div class="col-md-8">
                                                            <input type="text" name="nama_karyawan" required="" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Username* :</label>
							<div class="col-md-8">
								<input type="text" name="username" required=""  class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Alamat* :</label>
							<div class="col-md-8">
								<input type="text" name="alamat" required="" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">No Telp* :</label>
							<div class="col-md-8">
								<input type="text" name="no_telp" required="" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Level* :</label>
							<div class="col-md-8">
                                                            {{ Form::select('level', [''=>'Select Level'] + \App\ModelKaryawan::level_array(), null,['class'=>'form-control input-sm','required'=>true])}}
								
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary" value="Simpan">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
