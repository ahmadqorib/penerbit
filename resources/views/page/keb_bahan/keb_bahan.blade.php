@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Kebutuhan Bahan
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive" id="table-pra">
                <thead>
                    <tr>
                        <td width="3%">No</td>
                        <td>No SOKI</td>
                        <td>Produk</td>
                        <td>Oplah</td>
                        <td>Satuan</td>
                        <td>Jenis Pesanan</td>
                        <td>Juml. Hal. Isi</td>
                        <!--<td>Status</td>-->
                        <td width="15%">Aksi</td>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            var oTable = $('#table-pra').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("showDataBahan") }}'
                },
                columns: [
                    {data: 'no', name: 'no', orderable: false, searchable: false},
                    {data: 'no_soki', name: 'no_soki'},
                    {data: 'produk', name: 'produk'},
                    {data: 'oplah', name: 'oplah'},
                    {data: 'satuan', name: 'satuan'},
//                    {data: 'id_penerima', name: 'id_penerima', orderable: false},
                    {data: 'jenis_order', name: 'jenis_order'},
                    {data: 'jml_hlm_isi', name: 'jml_hlm_isi'},
//                    {data: 'status', name: 'status'},
                    {data: 'aksi', name: 'aksi', orderable: false}
                ],
            });
        });
    </script>
@endsection
