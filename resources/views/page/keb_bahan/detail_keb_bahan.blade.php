@extends('layout/master') 
@section('title', $title) 
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detail Kebutuhan Bahan
<!--            <div style="float: right">
                <a href="{{ route('praCetak') }}" class="btn btn-xs btn-warning"> kembali</a>
                <a data-href="{{ URL('kebBahan/confirmCetak/'.$det->id_detail_order) }}" class="btn btn-xs btn-success"
                    data-toggle="modal" data-target="#modalKonfirmasi">
                    <i class="glyphicon glyphicon-print"></i> kon. cetak
                </a>
            </div>-->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Produk</td>
                            <td>:</td>
                            <td>{{ $det->produk }}</td>
                        </tr>
                        <tr>
                            <td>Oplah</td>
                            <td>:</td>
                            <td>{{ $det->oplah }}</td>
                        </tr>
                        <tr>
                            <td>Satuan</td>
                            <td>:</td>
                            <td>{{ $det->satuan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Pesanan</td>
                            <td>:</td>
                            <td>{{ $det->jenis_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Panjang</td>
                            <td width="5%">:</td>
                            <td>{{ $det->panjang }}</td>
                        </tr>
                        <tr>
                            <td>Lebar</td>
                            <td>:</td>
                            <td>{{ $det->lebar }}</td>
                        </tr>
                        <tr>
                            <td>Jum. Hal. Isi</td>
                            <td>:</td>
                            <td>{{ $det->jml_hlm_isi }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $det->status_text() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                       &nbsp;
<!--                        <div style="float: right">
                            <a href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddKeb" data-id="{{ $det->id_detail_order }}">
                                <span class="glyphicon glyphicon-plus"></span> tambah
                            </a>
                        </div>-->
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%">No</td>
                                    <td>Nama Bahan</td>
                                    <td colspan="2">Uk. NC</td>
                                    <td colspan="2">Uk. Plano</td>
                                    <td>Pl Jadi</td>
                                    <td>Jml Lebar p</td>
                                    <td>Keterangan</td>
                                    <td width="13%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($detBahan as $value => $row)
                                <tr>
                                    <td width="3%">{{ $value + 1 }}</td>
                                    <td>{{ $row->bahan['nama_bahan'] }}</td>
                                    <td>{{ $row->panjang }}</td>
                                    <td>{{ $row->lebar }}</td>
                                    <td>{{ $row->bahan['panjang'] }}</td>
                                    <td>{{ $row->bahan['lebar'] }}</td>
                                    <td>{{ $row->pl_jadi }}</td>
                                    <td>{{ $row->jml_lbr_p }}</td>
                                    <td>{{ $row->ket }}</td>
                                    <td width="13%">
                                        <?php if($row->status == 0):?>
                                        <a data-href="{{ URL('/kebBahan/completed/'.$row->id_keb_bahan) }}" class="btn btn-success btn-xs" 
                                            data-toggle="modal" data-target="#modalCompleted"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-ok-circle"></span> completed
                                        </a>
                                        <?php endif;?>
<!--                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick=""  style="margin-top: 3px">
					 		                <span class="glyphicon glyphicon-edit"></span> edit
                                        </a>
                                        <a data-href="" class="btn btn-danger btn-xs" 
                                            data-toggle="modal" data-target="#modalHapus"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-trash"></span> hapus
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
