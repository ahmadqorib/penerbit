@extends('layout/master')
@section('title', $title)
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Kebutuhan Bahan
            <div style="float: right">
                <a href="{{ route('praCetak') }}" class="btn btn-xs btn-warning"> kembali</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row" style="padding: 5px;">
                <div class="col-md-6">
                    <label>ID Pra Cetak:</label>
                    <input type="text" disabled value="{{ $row->id_pra_cetak }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">ID Detail Order</td>
                            <td>:</td>
                            <td>{{ $row->id_detail_order }}</td>
                        </tr>
                        <tr>
                            <td>ID Komponen</td>
                            <td>:</td>
                            <td>{{ $row->id_komponen }}</td>
                        </tr>
                        <tr>
                            <td>ID Mesin</td>
                            <td>:</td>
                            <td>{{ $row->id_mesin }}</td>
                        </tr>
                        <tr>
                            <td>Nama pesanan</td>
                            <td>:</td>
                            <td>{{ $row->nama_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Up</td>
                            <td>:</td>
                            <td>{{ $row->up }}</td>
                        </tr>
                        <tr>
                            <td>Master</td>
                            <td>:</td>
                            <td>{{ $row->master }}</td>
                        </tr>
                        <tr>
                            <td>Ket</td>
                            <td>:</td>
                            <td>{{ $row->ket }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                       &nbsp;
                        <div style="float: right">
                            <a href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddDetail" data-id="{{ $row->id_order }}">
                                <span class="glyphicon glyphicon-plus"></span> tambah
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%">No</td>
                                    <td>Bahan</td>
                                    <td>Oplah</td>
                                    <td>Satuan</td>
                                    <td>Jenis Pesanan</td>
                                    <td>Panjang</td>
                                    <td>Lebar</td>
                                    <td>Juml. Hal. Isi</td>
                                    <td>Status</td>
                                    <td width="13%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($det as $value => $key)
                                <tr>
                                    <td width="3%">{{ $value + 1 }}</td>
                                    <td>{{ $key->produk }}</td>
                                    <td>{{ $key->oplah }}</td>
                                    <td>{{ $key->satuan }}</td>
                                    <td>{{ $key->jenis_order }}</td>
                                    <td>{{ $key->panjang }}</td>
                                    <td>{{ $key->lebar }}</td>
                                    <td>{{ $key->jml_hlm_isi}}</td>
                                    <td>{{ $key->status }}</td>
                                    <td>
                                        <a data-href="{{ URL('/detailPemesanan/confirmPra/'.$key->id_detail_order) }}" class="btn btn-xs btn-success"
                                            data-toggle="modal" data-target="#modalKonfirmasi">
                                            <span class="glyphicon glyphicon-hdd"></span> pra cetak
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-success" onclick="showEdit({{ $key->id_detail_order }})"  style="margin-top: 3px">
					 		                <span class="glyphicon glyphicon-edit"></span> edit
                                        </a>
                                        <a data-href="{{ URL('/detailPemesanan/delete/'.$key->id_detail_order) }}" class="btn btn-danger btn-xs"
                                            data-toggle="modal" data-target="#modalHapus"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-trash"></span> hapus
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('page.order.tambahDetailPemesanan')
    @include('page.order.editDetailPemesanan')
@endsection
