    <div id="modalUpload" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Upload File Produk</h4>
				</div>
				<div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Upload File</h4>
                            <form method="post" action="{{ url('/pemesanan/upload_file') }}" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                    <div class="col-md-10"> 
                                        <input type="hidden" name="id" value="" id="id_detail">
                                        <input type="file" name="file" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="submit" name="" class="btn btn-primary pull-right btn-sm" value="Save">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5">
                                        @if ($errors->has('file'))
                                            <span class="text-danger">
                                                {{ $errors->first('file') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
    <script>
        $('#modalUpload').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var iddetail = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #id_detail').val(iddetail)
        })
    </script>