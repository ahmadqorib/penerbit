@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Pemesanan
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive" id="table-pemesanan">
                <thead>
                    <tr>
                        <td width="3%">No</td>
                        <td>No SOKI</td>
                        <td>No SP</td>
                        <td>Nama Pesanan</td>
                        <td>Nama Pemesan</td>
                        <td>Tanggal Buat</td>
                        <td>Status</td>
                        <td width="7%">Aksi</td>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
     @include('page.order.tambahPemesanan')
    <script type="text/javascript">
        $(function() {
            var oTable = $('#table-pemesanan').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    @if($kondisi == "0" )
                        url: '{{ url("showPemesananNoFile") }}'
                    @else
                        url: '{{ url("showDataPemesanan") }}'
                    @endif
                },
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'no_soki', name: 'no_soki'},
                    {data: 'no_sp', name: 'no_sp'},
                    {data: 'nama_order', name: 'nama_order'},
                    {data: 'nama_pemesan', name: 'nama_pemesan'},
                    {data: 'tgl_order', name: 'tgl_order'},
                    {data: 'status', name: 'status', orderable: false, searchable: false},
                    {data: 'aksi', name: 'aksi', orderable: false, searchable:false}
                ],
            });
        });
    </script>
@endsection
