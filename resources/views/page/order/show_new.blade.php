@extends('layout/master') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        ACC Pemesanan Detail

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">  
                <h4>Data Pemesanan</h4>
                <div class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-2">Nama Produk* :</label>
                        <div class="col-md-10">
                            {{$data->produk}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-2">Jenis Percetakan* :</label>
                        <div class="col-md-10">
                            {{@$data->jenis_percetakan->nama}}
                        </div>
                    </div>
                </div>
                <div class="row">    
                    <div class="form-group">
                        <label class="control-label col-md-2">File Produk* :</label>
                        <div class="col-md-10">
                            {{@$data->file}}
                        </div>
                    </div>
                </div>
                </div>

            </div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-sm-12"> 

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#detail" class="j-tab-pracetak">Detail</a></li>
                    <li><a data-toggle="tab" href="#bahan" class="j-tab-pracetak">Pracetak & Kebutuhan Bahan</a></li>
                    <li><a data-toggle="tab" href="#cetak" class="j-tab-cetak">Cetak</a></li>
                    <li><a data-toggle="tab" href="#finishing" class="j-tab-finishing">Finishing</a></li>
                </ul>

                <div class="tab-content">
                    <div id="detail" class="tab-pane fade in active">
                        <br/>
                        <form action="" class="form-horizontal" id="form-save-detail">
                            <input type="hidden" name="id" value="<?=$data->id_detail_order?>">
                            <div class="">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Oplah :</label>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control j-oplah" name="oplah" placeholder="Masukkan Oplah" value="{{$data->oplah}}" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Satuan :</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control j-satuan" name="satuan" placeholder="Masukkan Satuan" value="{{$data->satuan}}" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Jenis Pesanan :</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control j-jenis" name="jenis_pesanan" placeholder="Masukkan Jenis Pesanan" value="{{$data->jenis_order}}"  required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Panjang (cm) :</label>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control j-panjang" name="panjang" placeholder="Masukkan Panjang" value="{{$data->panjang}}"  required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Lebar (cm) :</label>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control j-lebar" name="lebar" placeholder="Masukkan Lebar" value="{{$data->lebar}}"  required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Jumlah Halaman Isi :</label>
                                        <div class="col-md-4">
                                            <input type="number" class="form-control j-jml-hal" name="jml_hal" placeholder="Masukkan Jumlah Halaman Isi" value="{{$data->jml_hlm_isi}}" required="">
                                        </div>
                                    </div>
                                    
                                </div>
                                <?php if(count($data->pracetak) == 0):?>
                                    <div class="text-right col-sm-12 col-md-6"><button class="btn btn-primary j-save-detail" data-id="<?=$data->id_detail_order?>">Save</button></div>
                                <?php endif;?>        
                            </div>
                        </form>
                    </div>
                    <div id="bahan" class="tab-pane fade">
                        
                        <form action="" class="form-horizontal" id="form-save-pracetak-bahan">
                            <br/>
                            <input type="hidden" name="id_detail" value="<?=$data->id_detail_order?>">
                            <div class="">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Komponen :</label>
                                    <div class="col-md-4">
                                        {{ Form::select('id_komponen', [''=>'Select Komponen']+ \App\ModelKomponenJenisPercetakan::dropdown_komponen_by_jenis($data->id_jenis_percetakan), null,['class'=>'form-control j-select-komponen','required'=>true])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">UP :</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control j-up" name="up" placeholder="Masukkan UP" value="" min="1" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Plano :</label>
                                    <div class="col-md-4">
                                        {{ Form::select('id_plano', [''=>'Select Plano']+ \App\ModelPlano::dropdown(), null,['class'=>'form-control j-select-plano','required'=>true])}}
                                        <input type="hidden" class="j-panjang-plano" value="0">
                                        <input type="hidden" class="j-lebar-plano" value="0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Bahan :</label>
                                    <div class="col-md-4">
                                        {{ Form::select('id_bahan', [''=>'Select Bahan'], null,['class'=>'form-control j-select-bahan','required'=>true])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Panjang NC (cm) :</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control j-panjang-nc" name="panjang_nc" placeholder="Masukkan Panjang NC"   required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Lebar NC (cm) :</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control j-lebar-nc" name="lebar_nc" placeholder="Masukkan Lebar NC"  required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Penempatan Kertas :</label>
                                    <div class="col-md-4">
                                        {{ Form::select('otorisasi', [''=>'Select Penempanan Kertas'] + \App\ModelPlano::Otorisasi(), null,['class'=>'form-control j-select-otorisasi','required'=>true])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">1 Plano Jadi :</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control j-plano-jadi" name="plano_jadi" placeholder="Masukkan Jumlah 1 Plano Jadi" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Jumlah Lembar Plano :</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control j-jml-lembar-plano" name="jml_lembar_plano" placeholder="Masukkan Jumlah Lembar Plano"  required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Mesin :</label>
                                    <div class="col-md-4">
                                        {{ Form::select('id_mesin', [''=>'Select Mesin'], null,['class'=>'form-control j-select-mesin','required'=>true])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="control-label col-md-2">Model Montage :</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control j-model-montage" name="model_montage" placeholder="Masukkan Model Montage" required="">
                                        </div>
                                    </div>
                                <div class="form-group">
                                        <label class="control-label col-md-2">Master :</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control j-master" name="master" placeholder="Masukkan Master" required="">
                                        </div>
                                    </div>
                                <div class="form-group">
                                        <label class="control-label col-md-2">Keterangan :</label>
                                        <div class="col-md-4">
                                            <textarea class="form-control j-keterangan" name="keterangan"  required=""></textarea>
                                        </div>
                                    </div>
                                <div class="text-right col-sm-12 col-md-6"><button class="btn btn-primary j-save-pracetak-bahan" data-id="<?=$data->id_detail_order?>">Save</button></div>
                            
                            </div>
                        </form>
                        
                        <div class="row">
                        <div class="col-sm-12 table-responsive j-table-pracetak-bahan">
                            @include('page.order.table_pracetak_bahan',['model'=>$data])
                        </div>
                        </div>
                    </div>
                    <div id="cetak" class="tab-pane fade">
                       
                        <br/>
                        
                        <div class="row">
                        <div class="col-sm-12 table-responsive j-table-cetak">
                            @include('page.order.table_cetak',['model'=>$data])
                        </div>
                        </div>
                    </div>
                    <div id="finishing" class="tab-pane fade">
                        <br/>
                        <form action="" class="form-horizontal" id="form-save-finishing">
                            <input type="hidden" name="id_detail" value="<?=$data->id_detail_order?>">
                            <div class="">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Finishing :</label>
                                        <div class="col-md-4">
                                            {{ Form::select('id_finishing', [''=>'Select Finishing'] + App\ModelFinishing::dropdown(), null,['class'=>'form-control j-select-finishing','required'=>true])}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Keterangan :</label>
                                        <div class="col-md-4">
                                            <textarea type="text" class="form-control j-keterangan-finishing" name="ket" placeholder="Masukkan keterangan"   required=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="text-right col-sm-12 col-md-6"><button class="btn btn-primary j-save-finishing" data-id="<?=$data->id_detail_order?>">Save</button></div>
                                     
                            </div>
                        </form>
                        <div class="row">
                        <div class="col-sm-12 table-responsive j-table-finishing">
                            @include('page.order.table_finishing',['model'=>$data])
                        </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>


    </div>

</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        
        $("#form-save-detail").submit(function(e) {
            e.preventDefault();
            var parent = $(this);
            var data = $(parent).serialize();
            
            $.ajax({
                url: "<?=url('pemesanan/save_detail')?>",
                type: 'post',
                data: data,
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        $('.j-tab-pracetak').trigger('click');
                    }
                }
            });

        });
        
        $(document).delegate('.j-select-plano', 'change', function (e) {
            var id = $(this).val();
            var plano = $('.j-select-plano');
            var tujuan = $('.j-select-bahan');
            var panjang_plano = $('.j-panjang-plano');
            var lebar_plano = $('.j-lebar-plano');
            if(id == ''){
                $(tujuan).html('');
                $(tujuan).append('<option value="" selected="selected">Select Bahan</option>');
                $(panjang_plano).val(0);
                $(lebar_plano).val(0);
            }else{
                $.ajax({
                    url: "<?= url('pemesanan/select_bahan') ?>",
                    type: 'post',
                    data: {
                        id: id,

                    },
                    dataType: 'JSON',
                    success: function (result) {
                        if (result.success) {

                            $(panjang_plano).val(result.plano.panjang);
                            $(lebar_plano).val(result.plano.lebar);

                            $(tujuan).html('');
                            $(tujuan).append('<option value="" selected="selected">Select Bahan</option>');

                            $.each(result.item,function(i,v){
                                $(tujuan).append('<option value="'+i+'">'+v+'</option>');
                            });

                        }
                    }
                });
            }

        });
        $(document).delegate('.j-select-otorisasi', 'change', function (e) {
            var that = $(this);
            var id = $(this).val();
            
            var oplah = $('.j-oplah').val();
            var komponen = $('.j-select-komponen').val();
            var jumlah_halaman_isi = $('.j-jml-hal').val();
            var up = $('.j-up').val();
            
            var plano = $('.j-select-plano');
            var id_plano = $(plano).val();
            var panjang_plano = $('.j-panjang-plano').val();
            var lebar_plano = $('.j-lebar-plano').val();
            
            var panjang_nc = $('.j-panjang-nc').val();
            var lebar_nc = $('.j-lebar-nc').val();
            
            var plano_jadi = $('.j-plano-jadi');
            var jml_lembar_plano = $('.j-jml-lembar-plano');
            
            if(oplah == null || oplah == ''){
                alert('Mohon Isi Data Oplah Dahulu');
                $(that).val('');
                return false;
            }
            if(komponen == null || komponen == ''){
                $('.j-select-komponen').val('');
                alert('Mohon Pilih Data Komponen Dahulu');
                $(that).val('');
                return false;
            }
            if(up == null || up == ''){
                $('.j-up').val('');
                alert('Mohon Isi Data UP Dahulu');
                $(that).val('');
                return false;
            }
            
            if(id_plano == null || id_plano == ''){
                $(plano).val('');
                alert('Mohon Pilih Plano Dahulu');
                $(that).val('');
                return false;
            }
            
            if(panjang_plano == '0' || lebar_plano == '0'){
                $(plano).val('');
                alert('Mohon Pilih Plano Dahulu');
                $(that).val('');
                return false;
            }
            
            if(panjang_nc == null || lebar_nc == null || panjang_nc =='' || lebar_nc == ''){
                alert('Mohon Isi Panjang NC dan Lebar NC Dahulu');
                $(that).val('');
                return false;
            }
            
            if(komponen == 2){
                if(jumlah_halaman_isi == null || jumlah_halaman_isi == ''){
                    alert('Mohon Isi Data Jumlah Halaman Isi Dahulu');
                    $(that).val('');
                    return false;
                }
            }
            
            var plano_jadi_hasil = 0;
            
            //potrait
            if(id == 1){
                plano_jadi_hasil = Math.floor(panjang_plano / panjang_nc) * Math.floor(lebar_plano / lebar_nc);
                plano_jadi_hasil = Math.floor(plano_jadi_hasil);
            }
            
            //landscape
            if(id == 2){
                plano_jadi_hasil = Math.floor(panjang_plano / lebar_nc) * Math.floor(lebar_plano / panjang_nc);
                plano_jadi_hasil = Math.floor(plano_jadi_hasil);
            }
            
            plano_jadi_hasil = Math.floor(plano_jadi_hasil);
            
            var jml_lembar_plano_hasil = 0;
            if(plano_jadi_hasil > 0){
                jml_lembar_plano_hasil = oplah / up / plano_jadi_hasil;

                if(komponen == 2){
                    jml_lembar_plano_hasil = Math.floor(jumlah_halaman_isi * jml_lembar_plano_hasil / 2);
                }

                jml_lembar_plano_hasil = Math.floor(jml_lembar_plano_hasil);
            }
            
            $(plano_jadi).val(plano_jadi_hasil);
            $(jml_lembar_plano).val(jml_lembar_plano_hasil);
            
            var tujuan = $('.j-select-mesin');
            $.ajax({
                url: "<?= url('pemesanan/select_mesin') ?>",
                type: 'post',
                data: {
                    id_plano: id_plano,
                    id_komponen : komponen
                },
                dataType: 'JSON',
                success: function (result) {
                    if (result.success) {
                        $(tujuan).html('');
                        $(tujuan).append('<option value="" selected="selected">Select Mesin</option>');
                        
                        $.each(result.item,function(i,v){
                            $(tujuan).append('<option value="'+i+'">'+v+'</option>');
                        });
                        
                    }
                }
            });
            
        });
        
        $("#form-save-pracetak-bahan").submit(function(e) {
            e.preventDefault();
            var parent = $(this);
            var data = $(parent).serialize();
            $.ajax({
                url: "<?=url('pemesanan/save_pracetak_bahan')?>",
                type: 'post',
                data: data,
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        $('.j-table-pracetak-bahan').html(result.html);
                        $('.j-table-cetak').html(result.html2);
                        
                        reset_pracetak_bahan();
                    }
                }
            });

        });
        
        
        function reset_pracetak_bahan(){
            $('.j-select-komponen').val('');
            
            $('.j-up').val('');
            
            $('.j-select-plano').val('');
            $('.j-panjang-plano').val(0);
            $('.j-lebar-plano').val(0);
            
            $('.j-panjang-nc').val('');
            $('.j-lebar-nc').val('');
            $('.j-select-bahan').html('');
            $('.j-select-bahan').append('<option value="" selected="selected">Select Bahan</option>');
            $('.j-select-mesin').html('');
            $('.j-select-mesin').append('<option value="" selected="selected">Select Mesin</option>');
            
            $('.j-select-otorisasi').val('');
            
            $('.j-plano-jadi').val('');
            $('.j-jml-lembar-plano').val('');
            $('.j-model-motage').val('');
            $('.j-master').val('');
            $('.j-keterangan').val('');
        }
        
        
        
        
        
        $("#form-save-finishing").submit(function(e) {
            e.preventDefault();
            var parent = $(this);
            var data = $(parent).serialize();
            $.ajax({
                url: "<?=url('pemesanan/save_finishing_new')?>",
                type: 'post',
                data: data,
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        $('.j-table-finishing').html(result.html);
                        reset_finishing();
                    }
                }
            });

        });


        function reset_finishing(){
            $('.j-select-finishing').val('');
            $('.j-keterangan-finishing').val('');
            
        }
    });
</script>

@endsection
