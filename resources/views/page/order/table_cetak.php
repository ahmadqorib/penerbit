
<h4>Cetak</h4>
<table class="table table-striped">
    <tr>
        <th>Action</th>
        <th>Komponen</th>
        <th>Bahan</th>
        <th>UK NC Cetak</th>
        <th>Mesin</th>
        <th>Cetak</th>
        <th>Hal</th>
        <th>Tinta</th>
        <th>Jumlah Set</th>
        <th>Jumlah Cetak Per Set</th>
        <th>Insheet Tiap Set</th>
        <th>Total Druck Per Set</th>
    </tr>
    <?php foreach ($model->cetak as $item): ?>

        <tr>
            <td>
                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditCetak<?= $item->id_cetak ?>">
                    <span class="glyphicon glyphicon-edit"></span> edit
                </button>
            </td>
            <td><?= @$item->komponen->nama_komponen ?></td>
            <td><?= @$item->kebbahan->bahan->nama_bahan ?></td>
            <td><?= @$item->kebbahan->panjang ?> x <?= @$item->kebbahan->lebar ?></td>
            <td><?= @$item->pracetak->mesin->nama_mesin ?></td>
            <td><?= $item->ctk ?></td>
            <td><?= $item->hal ?></td>
            <td><?= @$item->tinta->warna ?></td>
            <td><?= $item->jml_set ?></td>
            <td><?= $item->jml_cetak_per_set ?></td>
            <td><?= $item->insheet_tiap_set ?></td>
            <td><?= $item->tot_druck_per_set ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php foreach ($model->cetak as $item): ?>

    <div class="modal fade" id="modalEditCetak<?= $item->id_cetak ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" class="form-horizontal form-save-cetak">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="text-danger">Edit Data</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="id_cetak" value="<?php echo $item->id_cetak; ?>">
                        <div class="">
                            <div class="row">    
                                <div class="form-group">
                                    <label class="control-label col-md-4">Komponen :</label>
                                    <div class="col-md-7">
                                        <?= $item->komponen->nama_komponen ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="form-group">
                                    <label class="control-label col-md-4">Bahan :</label>
                                    <div class="col-md-7">
                                        <?= $item->kebbahan->bahan->nama_bahan ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="form-group">
                                    <label class="control-label col-md-4">Cetak :</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control j-ctk" name="ctk" placeholder="Cetak" value="<?php echo $item->ctk; ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="form-group">
                                    <label class="control-label col-md-4">Hal :</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control j-hal" name="hal" placeholder="Masukkan hal" value="<?php echo $item->hal; ?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Tinta :</label>
                                    <div class="col-md-7">
                                        <?= Form::select('id_tinta', ['' => 'Select Tinta'] + App\ModelTinta::dropdown(), $item->id_tinta, ['class' => 'form-control j-select-tinta', 'required' => true]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jumlah Set :</label>
                                    <div class="col-md-7">
                                        <input type="number" class="form-control j-jml-set" name="jml_set" value="<?php echo $item->jml_set; ?>" placeholder="Masukkan jumlah set"  required="">
                                    </div>
                                </div>
                            </div>


                            <div class="text-right col-sm-12 col-md-6"></div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary j-save-cetak">Save</button>

                    </div>
                </form>
            </div>
        </div>

    </div>
<?php endforeach; ?>

<script type="text/javascript">
    $(document).ready(function () {
        $(".form-save-cetak").submit(function (e) {
            e.preventDefault();
            var parent = $(this);
            var data = $(parent).serialize();
            $.ajax({
                url: "<?= url('pemesanan/save_cetak_new') ?>",
                type: 'post',
                data: data,
                dataType: 'JSON',
                success: function (result) {
                    if (result.success) {
                        $('.j-table-cetak').html(result.html);
                        $('.modal.in').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                    }
                }
            });
            $('.modal.in').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        });
    });
</script>   

