<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <style>
        body{
            font-size : 12px;
        }
    </style>
</head>
<body onload="window.print();">

    <h6><b>SLIP ORDER KERJA INTERN</b></h6>
    <div class="row">
        <div class="col-xs-6">
            <label>No SOKI:</label>
            <label>{{ $row->no_soki }}</label>
        </div>
        <div class="col-xs-6">
            <label>No SP:</label>
            <label>{{ $row->no_sp }}</label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <table width="100%">
                <tr>
                    <td width="25%">Tgl. Dibuat</td>
                    <td width="5%">:</td>
                    <td>{{ $row->tgl_order }}</td>
                </tr>
                <tr>
                    <td>Pemesan</td>
                    <td>:</td>
                    <td>{{ $row->nama_pemesan }}</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td> {{ $row->alamat_pemesan }}</td>
                </tr>
                <tr>
                    <td>Nama pesanan</td>
                    <td>:</td>
                    <td>{{ $row->nama_order }}</td>
                </tr>
                <tr>
                    <td>Oplah</td>
                    <td>:</td>
                    <td>{{ $detail->oplah ." ". $detail->satuan }}</td>
                </tr>
            </table>
        </div>
        <div class="col-xs-6">
            <table width="100%">
                <tr>
                    <td width="25%">Pen. Pesanan</td>
                    <td>:</td>
                    <td>{{ $row->karyawan['nama_karyawan'] }}</td>
                </tr>
                <tr>
                    <td>No Telp Pemesan</td>
                    <td>:</td>
                    <td>{{ $row->no_telp_pemesan }}</td>
                </tr>
                <tr>
                    <td>Produk</td>
                    <td>:</td>
                    <td>{{ $detail->produk }}</td>
                </tr>
                <tr>
                    <td>Jenis Order</td>
                    <td>:</td>
                    <td>{{ $detail->jenis_order }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Produk</td>
                        <td>Jumlah</td>
                        <td>Satuan</td>
                        <td>Pjg</td>
                        <td>Lbr</td>
                        <td>Jml. Hal. Isi</td>
                        <td>Komponen</td>
                        <td>Jenis Bahan</td>
                        <td>Model Cetak</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $detail->produk }}</td>
                        <td>{{ $detail->oplah }}</td>
                        <td>{{ $detail->satuan }}</td>
                        <td>{{ $detail->panjang }}</td>
                        <td>{{ $detail->lebar }}</td>
                        <td>{{ $detail->jml_hlm_isi }}</td>
                        <td>
                            @foreach($detail->pracetak as $pra)
                                {{ $pra->komponen->nama_komponen }} <br/>
                            @endforeach
                        </td>
                        <td>
                            @foreach($detail->kebbahan as $bah)
                                {{ $bah->bahan->nama_bahan }} <br/>
                            @endforeach
                        </td>
                        <td>
                            @foreach($detail->cetak as $cet)
                                {{ $cet->model_cetak }} <br/>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-12">
            <h5>BAGIAN PRA CETAK</h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td width="1%">No.</td>
                        <td>Komponen</td>
                        <td>Mesin</td>
                        <td>Up</td>
                        <td>Model Montage</td>
                        <td>Master</td>
                        <td>Keterangan</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail->pracetak as $key => $pra)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $pra->komponen->nama_komponen }}</td>
                        <td>{{ $pra->mesin->no_mesin." : ".$pra->mesin->nama_mesin }}</td>
                        <td>{{ $pra->up }}</td>
                        <td>{{ $pra->model_montage }}</td>
                        <td>{{ $pra->master }}</td>
                        <td>{{ $pra->ket }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-12">
            <h5>BAGIAN KEBUTUHAN BAHAN</h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td width="1%">No.</td>
                        <td>Nama Bahan</td>
                        <td colspan="2">Ukuran NC</td>
                        <td colspan="2">Ukuran Plano</td>
                        <td>PL Jadi</td>
                        <td>Jumlah Lbr. P</td>
                        <td>Keterangan</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail->kebbahan as $key => $keb)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $keb->bahan->nama_bahan }}</td>
                        <td>{{ $keb->panjang }}</td>
                        <td>{{ $keb->lebar }}</td>
                        <td>{{ $keb->bahan->panjang }}</td>
                        <td>{{ $keb->bahan->lebar }}</td>
                        <td>{{ $keb->pl_jadi }}</td>
                        <td>{{ $keb->jml_lbr_p }}</td>
                        <td>{{ $keb->ket }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-12">
            <h5>BAGIAN CETAK</h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td width="1%">No.</td>
                        <td>Model Cetak</td>
                        <td>Hal. 1/2</td>
                        <td>Warna Tinta</td>
                        <td>Jml Set</td>
                        <td>Insh/Set</td>
                        <td>Tot Druck/Set Plate</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail->cetak as $key => $cet)
                    <tr>
                        <td width="1%">{{ $key+1 }}</td>
                        <td>{{ $cet->model_cetak }}</td>
                        <td>{{ $cet->hal }}</td>
                        <td>{{ $cet->tinta->warna }}</td>
                        <td>{{ $cet->jml_set }}</td>
                        <td>{{ $cet->insheet_tiap_set }}</td>
                        <td>{{ $cet->tot_druck_per_set }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 5px">
        <div class="col-md-12">
            <h5>BAGIAN FINISHING</h5>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td width="1%">No.</td>
                        <td>Finishing</td>
                        <td>Keterangan</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detail->finish as $key => $fin)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $fin->finishing->nama_finishing }}</td>
                        <td>{{ $fin->ket }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
    

