@extends('layout/master') 
@section('title', $title) @section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Add Pemesanan

    </div>
    <div class="panel-body">
        <form method="post" action="{{url('/pemesanan/save_pemesanan')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-sm-12">  
                    <h4>Data Pemesanan</h4>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Nama Pesanan* :</label>
                        <div class="col-md-4">
                            <input type="text" name="nama_pesanan" class="form-control input-sm" required="" placeholder="Masukkan nama pesanan...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Penerima* :</label>
                        <div class="col-md-4">
                            <?php $user = Auth::user();?>
                            <input type="text" name="penerima" class="form-control input-sm" value="<?=$user->nama_karyawan?>" readonly="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Nama Pemesan* :</label>
                        <div class="col-md-4">
                            <input type="text" name="nama_pemesan" class="form-control input-sm" required="" placeholder="Masukkan nama pemesan...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Alamat* :</label>
                        <div class="col-md-4">
                            <textarea name="alamat_pemesan" class="form-control input-sm" required="" placeholder="Masukkan alamat pemesan..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">No Telp. Pemesan* :</label>
                        <div class="col-md-4">
                            <input type="text" name="no_telp_pemesan" class="form-control input-sm" required=""placeholder="Masukkan no telepon...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Keterangan* :</label>
                        <div class="col-md-4">
                            <textarea name="ket" class="form-control input-sm" required="" placeholder="Masukkan keterangan..."></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Detail</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="30%">Nama Produk</th>
                                    <th width="30%">Jenis Percetakan</th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <tr>
                                    <td>
                                        <div class="btn btn-xs btn-warning j-remove-item"><i class="glyphicon glyphicon-remove"></i></div>
                                    </td>
                                   
                                    <td>
                                        <input type="text" class="form-control" name="nama_produk[]" placeholder="Masukkan Nama Produk..." required="">
                                    </td>
                                    <td>
                                        {{ Form::select('id_jenis_percetakan[]', [''=>'Select Nama Jenis Percetakan'] + $list_jenis_percetakan, null,['class'=>'form-control input-sm','required'=>true])}}
                                    </td>
                                   
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <div class="btn btn-primary j-add-item btn-sm pull-right" style="cursor: pointer">Add</div>  
                                    </td>
                                    
                                </tr>

                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

            <hr/>
            <div class="row-fluid">  
                <div class="pull-right">
                    <input type="submit" class="btn btn-success j-" value="Simpan">
                </div>
            </div>    
        </form>
    </div>
    <div class="hidden">
        <table>        
            <tbody class="item-push">
                <tr>
                    <td>
                        <div class="btn btn-xs btn-warning j-remove-item"><i class="glyphicon glyphicon-remove"></i></div>
                    </td>

                    <td>
                        <input type="text" class="form-control" name="nama_produk[]" placeholder="Masukkan Nama Produk..." required="">
                    </td>
                    <td>
                        {{ Form::select('id_jenis_percetakan[]', [''=>'Select Nama Jenis Percetakan'] + $list_jenis_percetakan, null,['class'=>'form-control input-sm','required'=>true])}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>    
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).delegate('.j-remove-item', 'click', function (e) {
            $(this).parents('tr').remove();

        });
        $(document).delegate('.j-add-item', 'click', function (e) {
            var item = $('.item-push').html();
            $('.item-push-content').append(item);

        });
        
        $(document).delegate('.j-save-button', 'click', function (e) {
            
        });
        
    });
</script>

@endsection
