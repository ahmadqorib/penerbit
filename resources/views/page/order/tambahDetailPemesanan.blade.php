<!-- modal tambah detail pemesanan -->
	<div id="modalAddDetail" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Produk</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/detailPemesanan/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<input type="hidden" name="id_order" id="idorder">
						<div class="form-group">
							<label class="control-label col-md-3">Produk* :</label>
							<div class="col-md-8">
								<input type="text" name="produk" class="form-control input-sm">
							</div>
						</div>
						<!-- <div class="form-group">
							<label class="control-label col-md-3">Oplah* :</label>
							<div class="col-md-8">
								<input type="text" name="oplah" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Satuan* :</label>
							<div class="col-md-8">
								<input type="text" name="satuan" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jenis Order* :</label>
							<div class="col-md-8">
								<input type="text" name="jenis_order" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">panjang* :</label>
							<div class="col-md-8">
								<input type="text" name="panjang" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Lebar* :</label>
							<div class="col-md-8">
								<input type="text" name="lebar" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jumlah Hal Isi* :</label>
							<div class="col-md-8">
								<input type="text" name="jml_hal" class="form-control input-sm">
							</div>
						</div> -->
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Simpan">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<script>
        $('#modalAddDetail').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var idorder = button.data('id') 
            var modal = $(this)
            modal.find('.modal-body #idorder').val(idorder)
        })
    </script>