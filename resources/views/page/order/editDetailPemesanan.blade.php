<!-- modal edit detail pemesanan -->
	<div id="modalEditDetail" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Detail Pemesanan</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/detailPemesanan/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<input type="hidden" name="id_detail_order" id="id_detail_order">
						<input type="hidden" name="id_order" id="id_order">
						<div class="form-group">
							<label class="control-label col-md-3">Produk* :</label>
							<div class="col-md-8">
								<input type="text" name="produk" class="form-control input-sm" id="produk">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Oplah* :</label>
							<div class="col-md-8">
								<input type="text" name="oplah" class="form-control input-sm" id="oplah">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Satuan* :</label>
							<div class="col-md-8">
								<input type="text" name="satuan" class="form-control input-sm" id="satuan">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jenis Order* :</label>
							<div class="col-md-8">
								<input type="text" name="jenis_order" class="form-control input-sm" id="jenis_order">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">panjang* :</label>
							<div class="col-md-8">
								<input type="text" name="panjang" class="form-control input-sm" id="panjang">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Lebar* :</label>
							<div class="col-md-8">
								<input type="text" name="lebar" class="form-control input-sm" id="lebar">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jumlah Hal Isi* :</label>
							<div class="col-md-8">
								<input type="text" name="jml_hal" class="form-control input-sm" id="jml_hal">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Edit">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<script type="text/javascript">
        function showEdit(id)
        {
            $.ajax({
                url : "{{ url('/detailPemesanan/showEdit') }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    $('#id_detail_order').val(data.id_detail_order);
                    $('#id_order').val(data.id_order);
                    $('#produk').val(data.produk);
                    $('#oplah').val(data.oplah);
                    $('#satuan').val(data.satuan);
                    $('#jenis_order').val(data.jenis_order);
                    $('#panjang').val(data.panjang);
                    $('#lebar').val(data.lebar);
                    $('#jml_hal').val(data.jml_hlm_isi);
                    $('#modalEditDetail').modal('show'); // show bootstrap modal when complete loaded
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
        }
    </script>