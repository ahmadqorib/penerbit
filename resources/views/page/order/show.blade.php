@extends('layout/master') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        ACC Pemesanan Detail

    </div>
    <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">  
                    <h4>Data Pemesanan</h4>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Nama Produk* :</label>
                        <div class="col-md-4">
                            {{$data->produk}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Jenis Percetakan* :</label>
                        <div class="col-md-4">
                            {{@$data->jenis_percetakan->nama}}
                        </div>
                    </div>
                    
                </div>
            </div>

            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Detail</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">Oplah</th>
                                    <th width="10%">Satuan</th>
                                    <th width="20%">Jenis Pesanan</th>
                                    <th width="10%">Panjang</th>
                                    <th width="10%">Lebar</th>
                                    <th width="10%">Jumlah Halaman Isi</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <?php if(!empty($data->oplah)):?>
                                    <tr>
                                        <td>
                                            <input type="number" class="form-control j-oplah" name="oplah" placeholder="Masukkan Oplah" value="{{$data->oplah}}" required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-satuan" name="satuan" placeholder="Masukkan Satuan" value="{{$data->satuan}}" required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-jenis" name="jenis_pesanan" placeholder="Masukkan Jenis Pesanan" value="{{$data->jenis_order}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-panjang" name="panjang" placeholder="Masukkan Panjang" value="{{$data->panjang}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-lebar" name="lebar" placeholder="Masukkan Lebar" value="{{$data->lebar}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-jml-hal" name="jml_hal" placeholder="Masukkan Jumlah Halaman Isi" value="{{$data->jml_hlm_isi}}" required="">
                                        </td>
                                        <td>
                                            <?php if($data->status == 0):?>
                                            <div class="btn btn-primary j-add-item btn-sm pull-right" data-id="<?=$data->id_detail_order?>" style="cursor: pointer">Save</div> 
                                            <?php endif;?>
                                        </td>

                                    </tr>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                                <?php if(empty($data->oplah)):?>
                                <tr>
                                    <td>
                                        <input type="number" class="form-control j-oplah" name="oplah" placeholder="Masukkan Oplah" required="">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control j-satuan" name="satuan" placeholder="Masukkan Satuan" required="">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control j-jenis" name="jenis_pesanan" placeholder="Masukkan Jenis Pesanan" required="">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control j-panjang" name="panjang" placeholder="Masukkan Panjang" required="">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control j-lebar" name="lebar" placeholder="Masukkan Lebar" required="">
                                    </td>
                                    <td>
                                        <input type="number" class="form-control j-jml-hal" name="jml_hal" placeholder="Masukkan Jumlah Halaman Isi" required="">
                                    </td>
                                    <td>
                                        <div class="btn btn-primary j-add-item btn-sm pull-right" data-id="<?=$data->id_detail_order?>" style="cursor: pointer">Add</div> 
                                    </td>
                                   
                                </tr>
                                <?php endif;?>

                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            
            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Pracetak</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">Komponen</th>
                                    <th width="10%">Mesin</th>
                                    <th width="10%">Up</th>
                                    <th width="10%">Model Montage</th>
                                    <th width="10%">Master</th>
                                    <th width="10%">Keterangan</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <?php if (count($data->pracetak) > 0):?>
                                    <?php foreach ($data->pracetak as $item):?>
                                    <tr>
                                        <td>
                                            {{ Form::select('id_komponen', [''=>'Select Komponen'] + App\ModelKomponen::dropdown(), $item->id_komponen,['class'=>'form-control j-komponen','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            {{ Form::select('id_mesin', [''=>'Select Mesin'] + App\ModelMesin::dropdown(), $item->id_mesin,['class'=>'form-control j-mesin','required'=>true])}}
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-up" name="up" placeholder="Masukkan Up" value="{{$item->up}}"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-model" name="model_montage" placeholder="Masukkan model" value="{{$item->model_montage}}"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-master" name="master" placeholder="Masukkan master" value="{{$item->master}}"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan" value="{{$item->ket}}"  required="">
                                        </td>
                                        <td>
                                            <?php if($item->status == 0):?>
                                            <div class="btn btn-primary j-add-item-pracetak btn-sm pull-right" data-id-detail="<?=$item->id_detail_order?>" data-id="<?=$item->id_pra_cetak?>"style="cursor: pointer">Save</div> 
                                            <?php endif;?>
                                        </td>

                                    </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                                <?php if($data->finish_pracetak == 0):?>
                                <tr>
                                    <td>
                                        {{ Form::select('id_komponen', [''=>'Select Komponen']+ App\ModelKomponen::dropdown(), null,['class'=>'form-control j-komponen','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            {{ Form::select('id_mesin', [''=>'Select Mesin'] + App\ModelMesin::dropdown(), null,['class'=>'form-control j-mesin','required'=>true])}}
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-up" name="up" placeholder="Masukkan Up"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-model" name="model_montage" placeholder="Masukkan model"   required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-master" name="master" placeholder="Masukkan master"   required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan"  required="">
                                        </td>
                                    <td>
                                        <div class="btn btn-primary j-add-item-pracetak btn-sm pull-right" data-id-detail="<?=$data->id_detail_order?>" data-id="0" style="cursor: pointer">Add</div> 
                                    </td>
                                   
                                </tr>
                                <?php endif;?>

                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Kebutuhan Bahan</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">Bahan</th>
                                    <th width="10%">Panjang</th>
                                    <th width="10%">Lebar</th>
                                    <th width="10%">PL Jadi</th>
                                    <th width="10%">Jumlah Lembar P</th>
                                    <th width="10%">Keterangan</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <?php if (count($data->kebbahan) > 0):?>
                                    <?php foreach ($data->kebbahan as $item):?>
                                    <tr>
                                        <td>
                                            {{ Form::select('id_bahan', [''=>'Select Bahan'] + App\ModelBahan::dropdown(), $item->id_bahan,['class'=>'form-control j-bahan','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-panjang" name="panjang" placeholder="Masukkan Panjang" value="{{$item->panjang}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-lebar" name="lebar" placeholder="Masukkan Lebar" value="{{$item->lebar}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-pl-jadi" name="pl_jadi" placeholder="Masukkan PL Jadi" value="{{$item->pl_jadi}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-jml-lembar-p" name="jml_lbr_p" placeholder="Masukkan Jumlah Lembar P" value="{{$item->jml_lbr_p}}"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan" value="{{$item->ket}}"  required="">
                                        </td>
                                        <td>
                                            <?php if($item->status == 0):?>
                                            <div class="btn btn-primary j-add-item-kebbahan btn-sm pull-right" data-id-detail="<?=$item->id_detail_order?>" data-id="<?=$item->id_keb_bahan?>"style="cursor: pointer">Save</div>
                                            <?php endif;?>
                                        </td>

                                    </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                                <?php if($data->finish_bahan == 0):?>
                                <tr>
                                    <td>
                                            {{ Form::select('id_bahan', [''=>'Select Bahan'] + App\ModelBahan::dropdown(), null,['class'=>'form-control j-bahan','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-panjang" name="panjang" placeholder="Masukkan Panjang"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-lebar" name="lebar" placeholder="Masukkan Lebar"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-pl-jadi" name="pl_jadi" placeholder="Masukkan PL Jadi"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-jml-lembar-p" name="jml_lembar_p" placeholder="Masukkan Jumlah Lembar P"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan"   required="">
                                        </td>
                                        <td>
                                            <div class="btn btn-primary j-add-item-kebbahan btn-sm pull-right" data-id-detail="<?=$data->id_detail_order?>" data-id="0"style="cursor: pointer">Add</div> 
                                        </td>
                                   
                                </tr>
                                <?php endif;?>
                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Cetak</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">Model Cetak</th>
                                    <th width="10%">Hal</th>
                                    <th width="10%">Tinta</th>
                                    <th width="10%">Jumlah Set</th>
                                    <th width="10%">Insheet Tiap Set</th>
                                    <th width="10%">Total Druck Per Set</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <?php if (count($data->cetak) > 0):?>
                                    <?php foreach ($data->cetak as $item):?>
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control j-model-cetak" name="model_cetak" placeholder="Masukkan model cetak" value="{{$item->model_cetak}}"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-hal" name="hal" placeholder="Masukkan hal" value="{{$item->hal}}"  required="">
                                        </td>
                                        <td>
                                            {{ Form::select('id_tinta', [''=>'Select Tinta'] + App\ModelTinta::dropdown(), $item->id_tinta,['class'=>'form-control j-tinta','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-jml-set" name="jml_set" placeholder="Masukkan jumlah set" value="{{$item->jml_set}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-insheet-tiap-set" name="insheet_tiap_set" placeholder="Masukkan insheet tiap set" value="{{$item->insheet_tiap_set}}"  required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-total-druck-per-set" name="total_druck_per_set" placeholder="Masukkan Total Druck Per Set" value="{{$item->tot_druck_per_set}}"  required="">
                                        </td>
                                       
                                        <td>
                                            <?php if($item->status == 0):?>
                                            <div class="btn btn-primary j-add-item-cetak btn-sm pull-right" data-id-detail="<?=$item->id_detail_order?>" data-id="<?=$item->id_cetak?>"style="cursor: pointer">Save</div> 
                                            <?php endif;?>
                                        </td>

                                    </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                                <?php if($data->finish_cetak == 0):?>
                                <tr>
                                    <td>
                                            <input type="text" class="form-control j-model-cetak" name="model_cetak" placeholder="Masukkan model cetak"  required="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-hal" name="hal" placeholder="Masukkan hal"  required="">
                                        </td>
                                        <td>
                                            {{ Form::select('id_tinta', [''=>'Select Tinta'] + App\ModelTinta::dropdown(), null,['class'=>'form-control j-tinta','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-jml-set" name="jml_set" placeholder="Masukkan jumlah set"   required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-insheet-tiap-set" name="insheet_tiap_set" placeholder="Masukkan insheet tiap set" required="">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control j-total-druck-per-set" name="total_druck_per_set" placeholder="Masukkan Total Druck Per Set"   required="">
                                        </td>
                                       
                                        <td>
                                            <div class="btn btn-primary j-add-item-cetak btn-sm pull-right" data-id-detail="<?=$data->id_detail_order?>" data-id="0"style="cursor: pointer">Add</div> 
                                        </td>
                                   
                                </tr>
                                <?php endif;?>
                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12"> 
                    <h4>Data Finish</h4>
                    <div class="j-detail-input-content">
                        <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">Nama Finshing</th>
                                    <th width="20%">Keterangan</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="item-push-content">
                                <?php if (count($data->finish) > 0):?>
                                    <?php foreach ($data->finish as $item):?>
                                    <tr>
                                        <td>
                                            {{ Form::select('id_finishing', [''=>'Select Finishing'] + App\ModelFinishing::dropdown(), $item->id_finishing,['class'=>'form-control j-finishing','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan" value="<?=$item->ket?>"  required="">
                                        </td>
                                       
                                        <td>
                                            <?php if($item->status == 0):?>
                                            <div class="btn btn-primary j-add-item-finish btn-sm pull-right" data-id-detail="<?=$item->id_detail_order?>" data-id="<?=$item->id_finish?>"style="cursor: pointer">Save</div> 
                                            <?php endif;?>
                                        </td>

                                    </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                            <tfoot>
                                <?php if($data->finish_finishing == 0):?>
                                <tr>
                                    
                                        <td>
                                            {{ Form::select('id_finishing', [''=>'Select Finishing'] + App\ModelFinishing::dropdown(), null,['class'=>'form-control j-finishing','required'=>true])}}
                                            
                                        </td>
                                        <td>
                                            <input type="text" class="form-control j-keterangan" name="ket" placeholder="Masukkan keterangan"   required="">
                                        </td>
                                        <td>
                                            <div class="btn btn-primary j-add-item-finish btn-sm pull-right" data-id-detail="<?=$data->id_detail_order?>" data-id="0"style="cursor: pointer">Add</div> 
                                        </td>
                                   
                                </tr>
                                <?php endif;?>
                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?php if($data->finish_pracetak > 0 || $data->finish_bahan > 0):?>
                    <h4>File</h4>
                    <div class="label label-default" style="padding: 10px;">{{ $data->file }}</div>
                    <?php else:?>
                    <h4>Upload File</h4>
                    <form method="post" action="{{ url('/pemesanan/upload_file') }}" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <div class="col-md-4"> 
                                <input type="hidden" name="id" value="{{ $data->id_detail_order }}">
                                <input type="file" name="file" class="form-control">
                            </div>
                            <div class="col-md-7">
                                @if(!empty($data->file))
                                    <div class="label label-default" style="padding: 10px;">{{ $data->file }}</div>
                                @endif
                            </div>
                            <div class="col-md-1">
                                <input type="submit" name="" class="btn btn-primary pull-right btn-sm" value="Add File">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5">
                                @if ($errors->has('file'))
                                    <span class="text-danger">
                                        {{ $errors->first('file') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                    <?php endif;?>
                </div>
            </div>
    </div>
     
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).delegate('.j-add-item', 'click', function (e) {
            var parent = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var oplah = $(parent).find('.j-oplah').val();
            var satuan = $(parent).find('.j-satuan').val();
            var jenis = $(parent).find('.j-jenis').val();
            var panjang = $(parent).find('.j-panjang').val();
            var lebar = $(parent).find('.j-lebar').val();
            var jml_hal = $(parent).find('.j-jml-hal').val();
            
            $.ajax({
                url: "<?=url('pemesanan/save_detail')?>",
                type: 'post',
                data: {
                    id: id,
            
                    oplah : oplah,
                    satuan : satuan,
                    jenis : jenis,
                    panjang : panjang,
                    lebar : lebar,
                    jml_hal : jml_hal,
                },
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        location.reload();
                    }
                }
            });

        });
        $(document).delegate('.j-add-item-pracetak', 'click', function (e) {
            var parent = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var id_detail = $(this).attr('data-id-detail');
            var komponen = $(parent).find('.j-komponen').val();
            var mesin = $(parent).find('.j-mesin').val();
            var up = $(parent).find('.j-up').val();
            var model = $(parent).find('.j-model').val();
            var master = $(parent).find('.j-master').val();
            var keterangan = $(parent).find('.j-keterangan').val();
            
            $.ajax({
                url: "<?=url('pemesanan/save_pracetak')?>",
                type: 'post',
                data: {
                    id: id,
                    id_detail : id_detail,
                    id_komponen : komponen,
                    id_mesin : mesin,
                    up : up,
                    model_montage : model,
                    master : master,
                    ket : keterangan,
                },
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        location.reload();
                    }
                }
            });

        });
        $(document).delegate('.j-add-item-kebbahan', 'click', function (e) {
            var parent = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var id_detail = $(this).attr('data-id-detail');
            var bahan = $(parent).find('.j-bahan').val();
            var panjang = $(parent).find('.j-panjang').val();
            var lebar = $(parent).find('.j-lebar').val();
            var pl_jadi = $(parent).find('.j-pl-jadi').val();
            var jml_lembar_p = $(parent).find('.j-jml-lembar-p').val();
            var keterangan = $(parent).find('.j-keterangan').val();
            
            $.ajax({
                url: "<?=url('pemesanan/save_kebbahan')?>",
                type: 'post',
                data: {
                    id: id,
                    id_detail : id_detail,
                    id_bahan : bahan,
                    panjang : panjang,
                    lebar : lebar,
                    pl_jadi : pl_jadi,
                    jml_lembar_p : jml_lembar_p,
                    ket : keterangan,
                },
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        location.reload();
                    }
                }
            });

        });
        $(document).delegate('.j-add-item-cetak', 'click', function (e) {
            var parent = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var id_detail = $(this).attr('data-id-detail');
            var model_cetak = $(parent).find('.j-model-cetak').val();
            var hal = $(parent).find('.j-hal').val();
            var id_tinta = $(parent).find('.j-tinta').val();
            var jml_set = $(parent).find('.j-jml-set').val();
            var insheet_tiap_set = $(parent).find('.j-insheet-tiap-set').val();
            var total_druck_per_set = $(parent).find('.j-total-druck-per-set').val();
            
            $.ajax({
                url: "<?=url('pemesanan/save_cetak')?>",
                type: 'post',
                data: {
                    id: id,
                    id_detail : id_detail,
                    model_cetak : model_cetak,
                    hal : hal,
                    id_tinta : id_tinta,
                    jml_set : jml_set,
                    insheet_tiap_set : insheet_tiap_set,
                    total_druck_per_set : total_druck_per_set,
                },
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        location.reload();
                    }
                }
            });

        });
        
        $(document).delegate('.j-add-item-finish', 'click', function (e) {
            var parent = $(this).parents('tr');
            var id = $(this).attr('data-id');
            var id_detail = $(this).attr('data-id-detail');
            var id_finishing = $(parent).find('.j-finishing').val();
            var ket = $(parent).find('.j-keterangan').val();
            
            $.ajax({
                url: "<?=url('pemesanan/save_finish')?>",
                type: 'post',
                data: {
                    id: id,
                    id_detail : id_detail,
                    id_finishing : id_finishing,
                    ket : ket
                },
                dataType: 'JSON',
                success: function (result) {
                    if(result.success){
                        location.reload();
                    }
                }
            });

        });
        
        
        
    });
</script>

@endsection
