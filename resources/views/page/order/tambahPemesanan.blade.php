<div id="modalAddPemesanan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah DataPemesanan</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'pemesanan/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
                    <div class="form-group">
                        <label class="control-label col-md-3">NO SOKI* :</label>
                        <div class="col-md-8">
                            <input type="text" name="no_soki" class="form-control input-sm" placeholder="Masukkan no soki...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">NO SP* :</label>
                        <div class="col-md-8">
                            <input type="text" name="no_sp" class="form-control input-sm" placeholder="Masukkan no sp...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Nama Pesanan* :</label>
                        <div class="col-md-8">
                            <input type="text" name="nama_pesanan" class="form-control input-sm" placeholder="Masukkan nama pesanan...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Penerima* :</label>
                        <div class="col-md-8">
                           <select name="id_penerima" class="form-control input-sm">
                               <option value="">Pilih Penerima</option>
                               @foreach($karyawan as $item)
                                <option value="{{ $item->id_karyawan }}">{{ $item->nama_karyawan }}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Nama Pemesan* :</label>
                        <div class="col-md-8">
                            <input type="text" name="nama_pemesan" class="form-control input-sm" placeholder="Masukkan nama pemesan...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Alamat* :</label>
                        <div class="col-md-8">
                            <textarea name="alamat_pemesan" class="form-control input-sm" placeholder="Masukkan alamat pemesan..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">No Telp. Pemesan* :</label>
                        <div class="col-md-8">
                            <input type="text" name="no_telp_pemesan" class="form-control input-sm" placeholder="Masukkan no telepon...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Keterangan* :</label>
                        <div class="col-md-8">
                            <textarea name="ket" class="form-control input-sm" placeholder="Masukkan keterangan..."></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
                <input type="reset" class="btn btn-danger btn-sm" value="Reset">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
