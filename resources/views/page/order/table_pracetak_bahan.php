
<h4>Pracetak</h4>
<table class="table table-striped">
    <tr>
        <th>Komponen</th>
        <th>Mesin</th>
        <th>UP</th>
        <th>Model Montage</th>
        <th>Master</th>
        <th>Keterangan</th>
    </tr>
    <?php foreach ($model->pracetak as $item):?>
    <tr>
        <td><?=$item->komponen->nama_komponen?></td>
        <td><?=$item->mesin->nama_mesin?></td>
        <td><?=$item->up?></td>
        <td><?=$item->model_montage?></td>
        <td><?=$item->master?></td>
        <td><?=$item->ket?></td>
    </tr>
    <?php endforeach; ?>
</table>

<h4>Kebutuhan Bahan</h4>

<table class="table table-striped">
    <tr>
        <th>Komponen</th>
        <th>Bahan</th>
        <th>Panjang NC</th>
        <th>Lebar NC</th>
        <th>Plano</th>
        <th>1 Plano Jadi</th>
        <th>Jumlah Lembar Plano</th>
        <th>Keterangan</th>
    </tr>
    <?php foreach ($model->kebbahan as $item):?>
    <tr>
        <td><?=$item->pracetak->komponen->nama_komponen?></td>
        <td><?=$item->bahan->nama_bahan?></td>
        <td><?=$item->panjang?></td>
        <td><?=$item->lebar?></td>
        <td><?=$item->bahan->plano->nama_ukuran()?></td>
        <td><?=$item->pl_jadi?></td>
        <td><?=$item->jml_lbr_p?></td>
        <td><?=$item->ket?></td>
    </tr>
    <?php endforeach; ?>
</table>

