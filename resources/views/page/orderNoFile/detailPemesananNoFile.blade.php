@extends('layout/master') 
@section('title', $title) 
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detail Pemesanan
            <div style="float: right">
                <a href="{{ route('pemesanan') }}" class="btn btn-xs btn-warning"> kembali</a>
            </div>
        </div>
        <div class="panel-body">
            
            <div class="row" style="padding: 5px;">
                <div class="col-md-6">
                    <label>No SOKI:</label>
                    <input type="text" disabled value="{{ $row->no_soki }}">
                </div>
                <div class="col-md-6">
                    <label>No SP:</label>
                    <input type="text" disabled value="{{ $row->no_sp }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Tgl. Dibuat</td>
                            <td>:</td>
                            <td>{{ $row->tgl_order }}</td>
                        </tr>
                        <tr>
                            <td>Pemesan</td>
                            <td>:</td>
                            <td>{{ $row->nama_pemesan }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>{{ $row->alamat_pemesan }}</td>
                        </tr>
                        <tr>
                            <td>Nama pesanan</td>
                            <td>:</td>
                            <td>{{ $row->nama_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Pen. Pesanan</td>
                            <td>:</td>
                            <td>{{ $row->karyawan['nama_karyawan'] }}</td>
                        </tr>
                        <tr>
                            <td>No Telp Pemesan</td>
                            <td>:</td>
                            <td>{{ $row->no_telp_pemesan }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $row->status_text() }}</td>
                        </tr>
                        <tr>
                            <td>Ket</td>
                            <td>:</td>
                            <td>{{ $row->ket }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                       &nbsp;
                        <div style="float: right">
                            <a href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddDetail" data-id="{{ $row->id_order }}">
                                <span class="glyphicon glyphicon-plus"></span> tambah
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%">No</td>
                                    <td>Produk</td>
                                    <td>Jenis Percetakan</td>
                                    <td>Status</td>
                                    <td width="18%">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($det as $value => $key)
                                <tr>
                                    <td width="3%">{{ $value + 1 }}</td>
                                    <td>{{ $key->produk }}</td>
                                    <td>{{ $key->jenis_percetakan->nama }}</td>
                                    <td>{{ $key->status_text() }}</td>
                                    <td>
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalUpload"  data-id="{{ $key->id_detail_order }}">
                                            <span class="glyphicon glyphicon-upload"></span> upload file
                                        </button>
                                        @if($key->funish_pracetak !=1 && $key->finish_bahan !=1)
                                            <a data-href="{{ URL('/detailPemesanan/delete/'.$key->id_detail_order) }}" class="btn btn-danger btn-xs" 
                                                data-toggle="modal" data-target="#modalHapus">
                                                <span class="glyphicon glyphicon-trash"></span> hapus</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('page.order.upload')
    @include('page.order.tambahDetailPemesanan')
@endsection
