@extends('layout/master') 
@section('title', $title) 
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detail Finish
            <div style="float: right">
                <a href="{{ route('finish') }}" class="btn btn-xs btn-warning"> kembali</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Produk</td>
                            <td>:</td>
                            <td>{{ $det->produk }}</td>
                        </tr>
                        <tr>
                            <td>Oplah</td>
                            <td>:</td>
                            <td>{{ $det->oplah }}</td>
                        </tr>
                        <tr>
                            <td>Satuan</td>
                            <td>:</td>
                            <td>{{ $det->satuan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Pesanan</td>
                            <td>:</td>
                            <td>{{ $det->jenis_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Panjang</td>
                            <td width="5%">:</td>
                            <td>{{ $det->panjang }}</td>
                        </tr>
                        <tr>
                            <td>Lebar</td>
                            <td>:</td>
                            <td>{{ $det->lebar }}</td>
                        </tr>
                        <tr>
                            <td>Jum. Hal. Isi</td>
                            <td>:</td>
                            <td>{{ $det->jml_hlm_isi }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $det->status_text() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%" rowspan="2">No</td>
                                    <td rowspan="2">Finishing</td>
                                    <td rowspan="2">Keterangan</td>
                                    <td width="7%" rowspan="2">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($detFinish as $value=>$row)
                               <tr>
                                    <td>{{ $value + 1 }}</td>
                                    <td>{{ $row->finishing->nama_finishing }}</td>
                                    <td>{{ $row->ket }}</td>
                                    <td>
                                        <?php if($row->status == 0):?>
                                        <a data-href="{{ URL('/finish/completed/'.$row->id_finish) }}" class="btn btn-success btn-xs" 
                                            data-toggle="modal" data-target="#modalCompleted"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-ok-circle"></span> completed
                                        </a>
                                        <?php endif;?>
<!--                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="showEdit({{ $row->id_cetak }})"  style="margin-top: 3px">
					 		                <span class="glyphicon glyphicon-edit"></span> edit
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
