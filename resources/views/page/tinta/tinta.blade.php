@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Tinta
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddTinta">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">NO</td>
                        <td>Warna</td>
                        <td width="15%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->warna }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditTinta" data-id="{{ $key->id_tinta }}" data-warna="{{ $key->warna }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/tinta/delete/'.$key->id_tinta) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.tinta.tambahTinta')
    @include('page.tinta.editTinta')
@endsection
