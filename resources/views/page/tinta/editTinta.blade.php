<!-- modal tambah data bahan -->
	<div id="modalEditTinta" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Tinta</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/tinta/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                        <input type="hidden" name="id_tinta" id="id_tinta">
						<div class="form-group">
							<label class="control-label col-md-3">Warna* :</label>
							<div class="col-md-8">
								<input type="text" name="warna" class="form-control input-sm" id="warna">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Edit">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
   
    <script>
        $('#modalEditTinta').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var idtinta = button.data('id')
            var warna = button.data('warna')
            var modal = $(this)
            modal.find('.modal-body #id_tinta').val(idtinta)
            modal.find('.modal-body #warna').val(warna)
        })
    </script>
