<!-- modal tambah data mesin -->
	<div id="modalAddTinta" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Tinta</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/tinta/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<!-- <div class="form-group">
							<label class="control-label col-md-3">ID Bahan*</label>
							<div class="col-md-8">
								<input type="text" name="id_bahan" class="form-control">
							</div>
						</div> -->
						<div class="form-group">
							<label class="control-label col-md-3">Warna* :</label>
							<div class="col-md-8">
								<input type="text" name="warna" class="form-control input-sm">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Simpan">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
