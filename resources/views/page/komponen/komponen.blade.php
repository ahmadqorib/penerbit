@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Komponen
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddKomponen">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">NO</td>
                        <td>Nama Komponen</td>
                        <td width="15%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->nama_komponen }}</td>
                        <td>
                            <?php if($key->id_komponen > 2):?>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditKomponen" data-id="{{ $key->id_komponen}}" data-namakomponen="{{ $key->nama_komponen }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/komponen/delete/'.$key->id_komponen) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            <?php endif;?>            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.komponen.tambahKomponen')
    @include('page.komponen.editKomponen')
@endsection
