@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Plano
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddPlano">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama" placeholder="Cari Nama Plano" value="<?= $nama ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Plano</td>
                            <td>Panjang</td>
                            <td>Lebar</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->panjang }}</td>
                            <td>{{ $item->lebar }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditPlano" data-id="{{ $item->id_plano }}" data-nama="{{ $item->nama }}" data-panjang="{{ $item->panjang }}" data-lebar="{{ $item->lebar }}">
                                    <span class="glyphicon glyphicon-edit"></span> edit</button>
                                <a data-href="{{ URL('/plano/delete/'.$item->id_plano) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama' => $nama,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data mesin -->
<div id="modalAddPlano" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Plano</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/plano/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Plano :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Panjang (cm) :</label>
                    <div class="col-md-8">
                        <input type="number" name="panjang" class="form-control input-sm" value="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Lebar (cm) :</label>
                    <div class="col-md-8">
                        <input type="number" name="lebar" class="form-control input-sm" value="1" min="1">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<!-- modal tambah data bahan -->
<div id="modalEditPlano" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Data Plano</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/plano/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                <input type="hidden" name="id_plano" id="id_plano">
                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Plano :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm" id="nama">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Panjang (cm) :</label>
                    <div class="col-md-8">
                        <input type="number" name="panjang" class="form-control input-sm" value="1" min="1" id="panjang">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Lebar (cm) :</label>
                    <div class="col-md-8">
                        <input type="number" name="lebar" class="form-control input-sm" value="1" min="1" id="lebar">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Edit">
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>




@endsection

@section('script')
<script>
    $('#modalEditPlano').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id_plano = button.data('id');
        var nama = button.data('nama');
        var panjang = button.data('panjang');
        var lebar = button.data('lebar');
        var modal = $(this);
        modal.find('.modal-body #id_plano').val(id_plano);
        modal.find('.modal-body #nama').val(nama);
        modal.find('.modal-body #panjang').val(panjang);
        modal.find('.modal-body #lebar').val(lebar);
    })
</script>

@endsection    
