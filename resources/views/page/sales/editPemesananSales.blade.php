@extends('layout/master') @section('title', $title) @section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Edit Data Pemesanan
    </div>
    <div class="panel-body">
        {{ Form::open(['url' => 'data_pemesanan/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
        <input type="hidden" name="id_order" value="{{ $record->id_order }}">
        <div class="form-group">
            <label class="control-label col-md-2">NO SOKI* :</label>
            <div class="col-md-5">
                <input type="text" name="no_soki" class="form-control input-sm" placeholder="Masukkan no soki..." value="{{ $record->no_soki }}" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">NO SP* :</label>
            <div class="col-md-5">
                <input type="text" name="no_sp" class="form-control input-sm" placeholder="Masukkan no sp..." value="{{ $record->no_sp }}" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Nama Pesanan* :</label>
            <div class="col-md-5">
                <input type="text" name="nama_pesanan" class="form-control input-sm" placeholder="Masukkan nama pesanan..." value="{{ $record->nama_order }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Penerima* :</label>
            <div class="col-md-5">
                <input type="text" name="id_penerima" class="form-control input-sm" value="{{ $record->karyawan->nama_karyawan }}" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Nama Pemesan* :</label>
            <div class="col-md-5">
                <input type="text" name="nama_pemesan" class="form-control input-sm" placeholder="Masukkan nama pemesan..." value="{{ $record->nama_pemesan }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Alamat* :</label>
            <div class="col-md-5">
                <textarea name="alamat_pemesan" class="form-control input-sm" placeholder="Masukkan alamat pemesan...">{{ $record->alamat_pemesan }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">No Telp. Pemesan* :</label>
            <div class="col-md-5">
                <input type="text" name="no_telp_pemesan" class="form-control input-sm" placeholder="Masukkan no telepon..." value="{{ $record->no_telp_pemesan }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Keterangan* :</label>
            <div class="col-md-5">
                <textarea name="ket" class="form-control input-sm" placeholder="Masukkan keterangan...">{{ $record->ket }}</textarea>
            </div>
        </div>
        <div class="col-md-2 col-md-offset-2">
            <input type="submit" class="btn btn-primary btn-sm" value="Edit">
            <input type="reset" class="btn btn-danger btn-sm" value="Reset">
        </div>
    </div>
</div>

@endsection
