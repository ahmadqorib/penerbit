@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Step Komponen Jenis Percetakan
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddStepKomponenJenisPercetakan">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-1 pull-right hidden">
                        <button  class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_step_percetakan" placeholder="Cari Nama Step" value="<?= $nama_step_percetakan ?>">

                    </div>
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama_komponen_jenis_percetakan" placeholder="Cari Nama KomponenJenisPercetakan" value="<?= $nama_komponen_jenis_percetakan ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Komponen Jenis Percetakan</td>
                            <td>Nama Step</td>
                            <td>Urutan</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ @$item->komponen_jenis_percetakan->nama_komponen_jenis_percetakan() }}</td>
                            <td>{{ @$item->step_percetakan->nama }}</td>
                            <td>{{ @$item->urutan }}</td>
                            <td>
                                
                                <a data-href="{{ URL('/step_komponen_jenis_percetakan/delete/'.$item->id_step_komponen_jenis_percetakan) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama_komponen_jenis_percetakan' => $nama_komponen_jenis_percetakan,
                        'nama_step_percetakan' => $nama_step_percetakan,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data step_percetakan -->
<div id="modalAddStepKomponenJenisPercetakan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Step Komponen Jenis Percetakan</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/step_komponen_jenis_percetakan/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Komponen Jenis Percetakan :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_komponen_jenis_percetakan', [''=>'Select Nama Komponen Jenis Percetakan'] + $list_komponen_jenis_percetakan, null,['class'=>'form-control input-sm','required'=>true])}}
                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-4">*Nama Step :</label>
                    <div class="col-md-7">
                        {{ Form::select('id_step_percetakan', [''=>'Select Nama Step Percetakan'] + $list_step_percetakan, null,['class'=>'form-control input-sm','required'=>true])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Urutan :</label>
                    <div class="col-md-8">
                        <input type="number" name="urutan" class="form-control input-sm" min="1">
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>






@endsection

@section('script')
@endsection    
