@extends('layout/master')

@section('title', $title)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Data Finishing
            <div style="float: right">
                <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddFinishing">
                    <span class="glyphicon glyphicon-plus"></span> tambah
                </button>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <td width="3%">NO</td>
                        <td>Nama Finishing</td>
                        <td width="15%">Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $value => $key)
                    <tr>
                        <td>{{ $value + 1 }}</td>
                        <td>{{ $key->nama_finishing }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditFinishing" data-id="{{ $key->id_finishing}}" data-namafinishing="{{ $key->nama_finishing }}">
                                        <span class="glyphicon glyphicon-edit"></span> edit</button>
                            <a data-href="{{ URL('/finishing/delete/'.$key->id_finishing) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                        <span class="glyphicon glyphicon-trash"></span> hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('page.finishing.tambahFinishing')
    @include('page.finishing.editFinishing')
@endsection
