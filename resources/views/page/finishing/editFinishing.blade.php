<!-- modal tambah data bahan -->
	<div id="modalEditFinishing" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Finishing</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/finishing/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                        <input type="hidden" name="id_finishing" id="id_finishing">
						<div class="form-group">
							<label class="control-label col-md-3">Nama Finishing* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_finishing" class="form-control input-sm" id="nama_finishing">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary" value="Edit">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>

    <script>
        $('#modalEditFinishing').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var idfinishing = button.data('id')
            var namafinishing = button.data('namafinishing')
            var modal = $(this)
            modal.find('.modal-body #id_finishing').val(idfinishing)
            modal.find('.modal-body #nama_finishing').val(namafinishing)
        })
    </script>
