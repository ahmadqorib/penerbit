<!-- modal tambah data mesin -->
	<div id="modalAddFinishing" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Tambah Data Finishing</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/finishing/create', 'class' => 'form-horizontal', 'id' => 'form']) }}
					<div class="form-group">
							<label class="control-label col-md-3">Nama Finishing* :</label>
							<div class="col-md-8">
								<input type="text" name="nama_finishing" class="form-control input-sm">
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary" value="Simpan">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
