<!-- modal edit detail pemesanan -->
	<div id="modalEditCetak" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title">Edit Data Cetak</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(['url' => '/cetak/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
						<input type="hidden" name="id_cetak" id="id_cetak">
						<input type="hidden" name="id_detail_order" value="{{ $det->id_detail_order }}">
						<div class="form-group">
							<label class="control-label col-md-3">Komponen & Bahan :</label>
							<div class="col-md-8">
								<input type="text" name="kb" class="form-control input-sm" id="kb" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Ukuran Nk Ctk :</label>
							<div class="col-md-4">
								<input type="text" name="pjg" class="form-control input-sm" id="pjg" disabled>
							</div>
							<div class="col-md-4">
							    <input type="text" name="lbr" class="form-control input-sm" id="lbr" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Mesin :</label>
							<div class="col-md-8">
								<input type="text" name="mesin" class="form-control input-sm" id="mesin" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Ctk :</label>
							<div class="col-md-8">
								<input type="text" name="ctk" class="form-control input-sm" id="ctk">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Hal :</label>
							<div class="col-md-8">
								<input type="text" name="hal" class="form-control input-sm" id="hal">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Warna Tinta* :</label>
							<div class="col-md-8">
								<select name="tinta" class="form-control input-sm" >
                                    <option id="tinta"></option>
								    <option value="">Pilih Warna Tinta</option>
								    @foreach($warna as $item)
								    <option value="{{ $item->id_tinta }}">{{ $item->warna }}</option>
								    @endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jumlah Set :</label>
							<div class="col-md-8">
								<input type="text" name="jml_set" class="form-control input-sm" id="jml_set">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Jumlah Cetak/ Set Plate :</label>
							<div class="col-md-8">
								<input type="text" name="jml_ctk" class="form-control input-sm" id="jml_ctk" value="{{ $det->oplah }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Insh/ Set :</label>
							<div class="col-md-7">
								<input type="text" name="insh" class="form-control input-sm" id="insh"> 
							</div>
							<label class="col-md-1">%</label>
						</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-primary btn-sm" value="Edit">
					<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<script type="text/javascript">
        function showEdit(id)
        {
            $.ajax({
                url : "{{ url('/Cetak/showEdit') }}/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    $('#id_cetak').val(id);
                    $('#kb').val(data.kombahan);
                    $('#lbr').val(data.lbr);
                    $('#pjg').val(data.pjg);
                    $('#mesin').val(data.mesin);
                    $("#tinta").attr("value", data.id_tinta);
                    $("#tinta").html(data.warna);
                    $('#ctk').val(data.deCetak.model_cetak);
                    $('#hal').val(data.deCetak.hal);
                    $('#jml_set').val(data.deCetak.jml_set);
                    $('#insh').val(data.deCetak.insheet_tiap_set);
                    $('#tot_druck').val(data.deCetak.tot_druck_per_set);
                    $('#modalEditCetak').modal('show'); // show bootstrap modal when complete loaded
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error get data from ajax');
                }
            });
        }
    </script>