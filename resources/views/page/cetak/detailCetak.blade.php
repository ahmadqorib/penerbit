@extends('layout/master') 
@section('title', $title) 
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detail Cetak
            <div style="float: right">
                <a href="{{ route('cetak') }}" class="btn btn-xs btn-warning"> kembali</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Produk</td>
                            <td>:</td>
                            <td>{{ $det->produk }}</td>
                        </tr>
                        <tr>
                            <td>Oplah</td>
                            <td>:</td>
                            <td>{{ $det->oplah }}</td>
                        </tr>
                        <tr>
                            <td>Satuan</td>
                            <td>:</td>
                            <td>{{ $det->satuan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Pesanan</td>
                            <td>:</td>
                            <td>{{ $det->jenis_order }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table width="100%" class="tableDetail">
                        <tr>
                            <td width="25%">Panjang</td>
                            <td width="5%">:</td>
                            <td>{{ $det->panjang }}</td>
                        </tr>
                        <tr>
                            <td>Lebar</td>
                            <td>:</td>
                            <td>{{ $det->lebar }}</td>
                        </tr>
                        <tr>
                            <td>Jum. Hal. Isi</td>
                            <td>:</td>
                            <td>{{ $det->jml_hlm_isi }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $det->status_text() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default" style="margin-top: 5px; border-radius: 0px;">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <td width="3%">No</td>
                                    <td>Model Cetak</td>
                                    <td>Hal 1/2</td>
                                    <td>Warna Tinta</td>
                                    <td>Jml. Set</td>
                                    <td>Insh/ Set</td>
                                    <td>Tot. druck/ Set Plate</td>
                                    <td>File</td>
                                    <td width="7%" rowspan="2">Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($detCetak as $value=>$row)
                               <tr>
                                    <td>{{ $value + 1 }}</td>
                                    <td>{{ $row->model_cetak }}</td>
                                    <td>{{ $row->hal }}</td>
                                    <td>{{ $row->tinta['warna'] }}</td>
                                    <td>{{ $row->jml_set }}</td>
                                    <td>{{ $row->insheet_tiap_set }} %</td>
                                    <td>{{ $row->tot_druck_per_set }}</td>
                                    <td><a href="{{ asset('file/'.$row->detailorder->file ) }}" class="btn btn-xs btn-danger">{{ $row->detailorder->file }} <span class="glyphicon glyphicon-download"></span></a></td>
                                    <td>
                                        <?php if($row->status == 0):?>
                                        <a data-href="{{ URL('/cetak/completed/'.$row->id_cetak) }}" class="btn btn-success btn-xs" 
                                            data-toggle="modal" data-target="#modalCompleted"  style="margin-top: 3px">
                                            <span class="glyphicon glyphicon-ok-circle"></span> completed
                                        </a>
                                        <?php endif;?>
<!--                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="showEdit({{ $row->id_cetak }})"  style="margin-top: 3px">
					 		                <span class="glyphicon glyphicon-edit"></span> edit
                                        </a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('page.cetak.editCetak')
@endsection
