@extends('layout/master')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Data Distributor
        <div style="float: right">
            <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddDistributor">
                <span class="glyphicon glyphicon-plus"></span> tambah
            </button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <form action="">
                    
                    <div class="col-sm-3 pull-right">
                        <input type="text" class="form-control" name="nama" placeholder="Cari Nama Distributor" value="<?= $nama ?>">

                    </div>
                </form>
            </div>    
        </div>
        <br/>
        <div class="row-fluid">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <td width="3%">NO</td>
                            <td>Nama Distributor</td>
                            <td>Alamat</td>
                            <td>No Telp</td>
                            <td width="15%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->no_telp }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEditDistributor" data-id="{{ $item->id_distributor }}" data-nama="{{ $item->nama }}" data-alamat="{{ $item->alamat }}" data-no_telp="{{ $item->no_telp }}">
                                    <span class="glyphicon glyphicon-edit"></span> edit</button>
                                <a data-href="{{ URL('/distributor/delete/'.$item->id_distributor) }}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalHapus">
                                    <span class="glyphicon glyphicon-trash"></span> hapus</a>
                            </td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="pull-right">
                    <?php
                    echo $data->appends([
                        'nama' => $nama,
                    ])->links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah data mesin -->
<div id="modalAddDistributor" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data Distributor</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/distributor/store', 'class' => 'form-horizontal', 'id' => 'form']) }}

                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Distributor :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Alamat :</label>
                    <div class="col-md-8">
                        <input type="text" name="alamat" class="form-control input-sm" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*No Telp :</label>
                    <div class="col-md-8">
                        <input type="text" name="no_telp" class="form-control input-sm" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<!-- modal tambah data bahan -->
<div id="modalEditDistributor" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Data Distributor</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => '/distributor/update', 'class' => 'form-horizontal', 'id' => 'form']) }}
                <input type="hidden" name="id_distributor" id="id_distributor">
                <div class="form-group">
                    <label class="control-label col-md-3">*Nama Distributor :</label>
                    <div class="col-md-8">
                        <input type="text" name="nama" class="form-control input-sm" id="nama">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*Alamat :</label>
                    <div class="col-md-8">
                        <input type="text" name="alamat" class="form-control input-sm" id="alamat">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">*No Telp :</label>
                    <div class="col-md-8">
                        <input type="text" name="no_telp" class="form-control input-sm"  id="no_telp">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary btn-sm" value="Edit">
            </div>
            {{ Form::close() }}
        </div>

    </div>
</div>




@endsection

@section('script')
<script>
    $('#modalEditDistributor').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id_distributor = button.data('id');
        var nama = button.data('nama');
        var alamat = button.data('alamat');
        var no_telp = button.data('no_telp');
        var modal = $(this);
        modal.find('.modal-body #id_distributor').val(id_distributor);
        modal.find('.modal-body #nama').val(nama);
        modal.find('.modal-body #alamat').val(alamat);
        modal.find('.modal-body #no_telp').val(no_telp);
    })
</script>

@endsection    
