
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>
	<div class="container">
    	<div class="row">
    		<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
    			<h3 align="center">LOGIN!!!</h3>
    			{{ Form::open(['url' => '/login']) }}
    				<hr class="colorgraph">
    				<div class="form-group">
    					<div class="input-group">
	    					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	    					<input type="text" class="form-control" name="username" placeholder="Masukkan Username ... ">
  						</div>
    				</div>
    				<div class="form-group">
    					<div class="input-group">
	    					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
	    					<input type="password" class="form-control" name="password" placeholder="Masukkan Password ... ">
  						</div>
    				</div>
    				<div class="form-group group-but">
    					<input type="submit" name="" class="btn btn-primary btn-block" value="Login">
    				</div>
    			{{ Form::close() }}
    		</div>
    	</div>
	</div>
</body>
</html>
